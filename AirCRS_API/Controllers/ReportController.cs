﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AirCRS_API.SERVICE.ReportServices;
using AirCRS_API.Models.UniversalModel;
using Microsoft.AspNet.Identity;
using AirCRS_API.SERVICE.ReportServices.Dtos;

namespace AirCRS_API.Controllers
{
    public class ReportController : ApiController
    {
        private readonly IReportServices _reportServices;
        public ReportController(IReportServices reportService)
        {
            _reportServices = reportService;
        }
        [Route("GetPnrList")]
        [HttpPost]
        public HttpResponseMessage GetPnrList()
        {
            var formatter = Models.RequestFormat.JsonFormaterString();
            try
            {
                var loggedUser = User.Identity.GetUserId();
                var response = _reportServices.GetPNRList(loggedUser);
                return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = true, Message = "PNR Booking List", Payload = response }, formatter);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = false, Message = "Data Not Found", Payload = null }, formatter);
            }
        }
        [Route("GetPnrReportList")]
        [HttpPost]
        public HttpResponseMessage GetPnrReportList(PnrWiseReportVm input)
        {
            var formatter = Models.RequestFormat.JsonFormaterString();
            try
            {
                var loggedUser = User.Identity.GetUserId();
                var response = _reportServices.GetPnrReportList(input.TripId,loggedUser);
                return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = true, Message = "PNR Booking List", Payload = response }, formatter);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = false, Message = "Data Not Found", Payload = null }, formatter);
            }
        }

    }
}
