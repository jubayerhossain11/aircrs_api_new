﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web;
using System.Web.Http;
using AirCRS_API.DAL.DBContext;
using AirCRS_API.DAL.Entities;
using AirCRS_API.Models;
using AirCRS_API.Models.UniversalModel;
using Microsoft.AspNet.Identity;

namespace AirCRS_API.Controllers
{
    //[Authorize]
    public class UserRoleController : ApiController
    {
        [HttpPost]
        [Route("GetAccess")]
        public HttpResponseMessage GetAccess()
        {
            var formatter = RequestFormat.JsonFormaterString();
            try
            {
                List<Accesslist> AccesList = new List<Accesslist>();
                using (AirCRSDBContext unit = new AirCRSDBContext())
                {
                    ClaimsIdentity claimsIdentity = HttpContext.Current.User.Identity as ClaimsIdentity;

                    string userId = claimsIdentity.GetUserId();
                    int GroupID = GetGroupID();
                    if (GroupID != 0)
                    {
                        var access = unit.Accesslists.ToList();
                        var accessMapper = unit.AccessMappers.Where(x=>x.PmsGroupGroupID == GroupID).FirstOrDefault();

                        string s = accessMapper.AccessList;
                        string[] values = s.Split(',');
                        foreach (var item2 in values)
                        {
                            Accesslist ac = unit.Accesslists.Find(Convert.ToInt32(item2));
                            AccesList.Add(ac);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = true, Message = "Data Found", Payload = AccesList }, formatter);
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = false, Message = "400" }, formatter);
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = false, Message = "400" }, formatter);

            }
        }



        public int GetGroupID()
        {
            string UserID = User.Identity.GetUserId();
            int GID = 0;
            using (AirCRSDBContext unit = new AirCRSDBContext())
            {
                var aspNetUser = unit.Users.Find(UserID);
                if (aspNetUser != null)
                {
                    GID = aspNetUser.PmsGroupID;
                    return GID;
                }
                return GID;
            }

        }
    }
}
