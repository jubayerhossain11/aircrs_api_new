﻿using AirCRS_API.Models;
using AirCRS_API.Models.UniversalModel;
using AirCRS_API.SERVICE.BookingServices;
using AirCRS_API.SERVICE.BookingServices.Dtos;
using AirCRS_API.SERVICE.TripServices;
using AirCRS_API.SERVICE.TripServices.Dtos;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace AirCRS_API.Controllers
{
    [System.Web.Http.Authorize]
    public class BookingController : ApiController
    {
        private readonly IBookingService _bookingService;

        public BookingController(IBookingService bookingService)
        {
            _bookingService = bookingService;
        }
        // GET: Booking

        [System.Web.Http.Route("GetPNRWithAvaibleTickets")]
        [System.Web.Http.HttpPost]
        public HttpResponseMessage GetPNRWithAvaibleTickets(SearchInputModel input)
        {
            var formatter = RequestFormat.JsonFormaterString();
            try
            {
                var output = _bookingService.GetPNRWithAvaibleTickets(User.Identity.GetUserId());

                return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = true, Message = "Data Found", Payload = output }, formatter);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = false, Message = "Data Not Found", Payload = null }, formatter);
            }
        }


        [System.Web.Http.Route("GetPriceBreakDownByTripId")]
        [System.Web.Http.HttpPost]
        public HttpResponseMessage GetPriceBreakDownByTripId(BookingInputDto input)
        {
            var kkk = ConfigurationManager.AppSettings["BaseCurrency"];
            var formatter = RequestFormat.JsonFormaterString();
            try
            {
                var output = _bookingService.GetPriceBreakDownByTripId(input.TripId);

                return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = true, Message = "Data Found", Payload = output }, formatter);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = false, Message = "Data Not Found", Payload = null }, formatter);
            }
        }
        
        [System.Web.Http.Route("GetWheelChairByTripId")]
        [System.Web.Http.HttpPost]
        public HttpResponseMessage GetWheelChairByTripId(WheelChairDto input)
        {
            var formatter = RequestFormat.JsonFormaterString();
            try
            {
                var output = _bookingService.GetWheelChairByTripId(input.TripId);

                return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = true, Message = "Data Found", Payload = output ?? new WheelChairDto() }, formatter);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = false, Message = "Data Not Found", Payload = null }, formatter);
            }
        }

        [System.Web.Http.Route("GetMealByTripId")]
        [System.Web.Http.HttpPost]
        public HttpResponseMessage GetMealByTripId(BookingInputDto input)
        {
            var formatter = RequestFormat.JsonFormaterString();
            try
            {
                var output = _bookingService.GetMealListByTripId(input.TripId);

                return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = true, Message = "Data Found", Payload = output }, formatter);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = false, Message = "Data Not Found", Payload = null }, formatter);
            }
        }

        [System.Web.Http.Route("GetBaggageByTripId")]
        [System.Web.Http.HttpPost]
        public HttpResponseMessage GetBaggageByTripId(BookingInputDto input)
        {
            var formatter = RequestFormat.JsonFormaterString();
            try
            {
                var output = _bookingService.GetBaggageByTripId(input.TripId);

                return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = true, Message = "Data Found", Payload = output }, formatter);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = false, Message = "Data Not Found", Payload = null }, formatter);
            }
        }

        [System.Web.Http.Route("CreatePassengerForBooking")]
        [System.Web.Http.HttpPost]
        public HttpResponseMessage CreatePassengerForBooking(List<CreateOrUpdatePassengerInfo> input)
        {
            var formatter = RequestFormat.JsonFormaterString();
            try
            {
                var output = _bookingService.CreatePassengerForBooking(input, User.Identity.GetUserId());

                return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = true, Message = "Data Found", Payload = output }, formatter);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = false, Message = "Data Not Found", Payload = null }, formatter);
            }
        }

    }
}