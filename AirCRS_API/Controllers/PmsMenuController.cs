﻿using AirCRS_API.DAL.DBContext;
using AirCRS_API.DAL.Entities;
using AirCRS_API.Models;
using AirCRS_API.Models.Menu;
using AirCRS_API.Models.UniversalModel;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace AirCRS_API.Controllers
{
    
        [System.Web.Http.Authorize]
        public class PmsMenueController : ApiController//return menue list of Logged in user
        {
            #region Garbage

            //[System.Web.Http.HttpPost]
            //[System.Web.Http.Route("PmsMenueList")]
            //public HttpResponseMessage GetPmsMenueList()
            //{
            //    var formatter = RequestFormat.JsonFormaterString();
            //    Helper.CustomAuthorizationAttribute customAuthorizationHelper = new Helper.CustomAuthorizationAttribute();
            //    List<Models.Accesslist> accesslist = new List<Models.Accesslist>();
            //    List<int> AccesList = new List<int>();
            //    using (Models.DAL.UnitOfWork unit = new Models.DAL.UnitOfWork())
            //    {
            //        int GroupID = customAuthorizationHelper.GetGroupID(User.Identity.GetUserId());
            //        if (GroupID != 0)
            //        {
            //            var accessMapper = unit.AccessMapperRepository.GetByID(GroupID);

            //            string s = accessMapper.AccessList;
            //            string[] values = s.Split(',');
            //            foreach (var value in values)
            //            {
            //                AccesList.Add(Convert.ToInt32(value));
            //            }

            //            foreach (var item in AccesList)
            //            {
            //                var access = unit.AccesslistRepository.GetByID(item);

            //                accesslist.Add(access);
            //            }
            //            return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = true, Message = "200", Payload = accesslist }, formatter);

            //        }
            //        return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = false, Message = "400" }, formatter);

            //    }

            //}

            #endregion

            [System.Web.Http.HttpPost]
            [System.Web.Http.Route("PmsRootMenuList")]
            public HttpResponseMessage GetPmsRootMenuList()
            {
                try
                {
                    Menu menu = new Menu();
                    //Helper.CustomAuthorizationAttribute customAuthorizationHelper = new Helper.CustomAuthorizationAttribute();
                    List<CustomerAccessList> AllAccesslist = new List<CustomerAccessList>();

                    List<MenueViewModel> menueList = new List<MenueViewModel>();

                    List<int> AccesList = new List<int>();
                    using (AirCRSDBContext unit = new AirCRSDBContext())
                    {
                        //int GroupID = customAuthorizationHelper.GetGroupID(User.Identity.GetUserId()); //1
                        //string UserName = customAuthorizationHelper.GetUserName(User.Identity.GetUserId());

                        string LoggedInUser = User.Identity.GetUserId();

                        int GroupID = unit.Users.ToList().Where(x => x.Id == LoggedInUser).Select(x => x.PmsGroupID).FirstOrDefault();


                        var UserName = unit.Users.Find(LoggedInUser).UserName;
                        var PmsGroupId = unit.Users.Find(LoggedInUser);
                        string GroupName = unit.PmsGroups.Find(PmsGroupId.PmsGroupID).GroupName;


                        if (GroupID != 0)
                        {
                        var accessMapper = unit.AccessMappers.Where(x => x.PmsGroupGroupID == GroupID).FirstOrDefault();
                            string s = accessMapper.AccessList;
                            string[] values = s.Split(',');
                            foreach (var value in values)
                            {
                                AccesList.Add(Convert.ToInt32(value));
                            }
                            foreach (var item in AccesList)
                            {
                                try
                                {
                                    var access = unit.Accesslists.Find(item);

                                    CustomerAccessList custom = new CustomerAccessList();
                                    custom.AccessID = access.AccessID;
                                    custom.AccessStatus = access.AccessStatus;
                                    custom.ActionName = access.ActionName;
                                    custom.BaseModule = access.BaseModule;
                                    custom.ControllerName = access.ControllerName;
                                    custom.IconClass = access.IconClass;
                                    custom.Mask = access.Mask;
                                    custom.BaseModuleIndex = access.BaseModuleIndex;

                                    if (access != null && access.IsRemoved == 0 && access.IsVisible == 0)
                                    {
                                        AllAccesslist.Add(custom);
                                    }
                                }
                                catch (Exception ex)
                                {


                                }

                            }

                            bool IsCrsGroup = GroupID == 2 ? true : false || GroupID != 1 ? true : false;

                            var ModuleList = unit.Modules.Where(x => IsCrsGroup == true ? x.IsCrsUser == true : x.IsCrsUser == false).OrderBy(a => a.MenueOrder).ToList();
                            foreach (var item in ModuleList)
                            {
                                MenueViewModel menue = new MenueViewModel();
                                menue.ModuleID = item.ModuleID;
                                menue.ModuleName = item.ModuleName;
                                menue.ModuleIcon = item.ModuleIcon;
                                var module = unit.Modules.Find(item.ModuleID);

                                if (module.IsVisible == 0)
                                {
                                    menue.AccessList = AllAccesslist.Where(a => a.BaseModule == item.ModuleID).OrderBy(a => a.BaseModuleIndex).ToList();
                                    menueList.Add(menue);

                                }


                            }
                        }

                        CrsUser crsUser;

                        int groupID = unit.Users.Where(x => x.Id == LoggedInUser).Select(x => x.PmsGroupID).FirstOrDefault();

                        int? crsUserId = unit.PmsGroups.Where(x => x.GroupID == groupID).Select(x => x.CrsUserId).FirstOrDefault();

                        if (crsUserId == null && groupID == 2)
                        {
                            crsUser = unit.CrsUsers.Where(x => x.IdentityUserID == LoggedInUser).FirstOrDefault();
                            menu.BannerName = crsUser.BannerName;
                            menu.LogoImageUrl = crsUser.LogoImageUrl;
                        }

                        else if (crsUserId != null)
                        {
                            crsUser = unit.CrsUsers.Where(x => x.Id == crsUserId).FirstOrDefault();

                            menu.BannerName = crsUser.BannerName;
                            menu.LogoImageUrl = crsUser.LogoImageUrl;
                        }




                        menu.MenuList = menueList;
                        menu.UserName = UserName;
                        menu.GroupName = GroupName;

                        var formatter = RequestFormat.JsonFormaterString();
                        return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = true, Message = "200", Payload = menu }, formatter);

                        //var formatter1 = RequestFormat.JsonFormaterString();
                        //return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = false, Message = "400" }, formatter1);

                    }
                }
                catch (Exception ex)
                {
                    var formatter = RequestFormat.JsonFormaterString();
                    return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = false, Message = "200", Payload = null }, formatter);


                }

            }

            //[System.Web.Http.HttpPost]
            //[System.Web.Http.Route("GetNotification")]
            //public HttpResponseMessage GetNotification()
            //{
            //    var formatter = RequestFormat.JsonFormaterString();
            //    try
            //    {
            //        using (var unit = new Models.DAL.UnitOfWork())
            //        {
            //            Models.ViewModel.Menue.AdminNotificationVM adminNotificationVM = new Models.ViewModel.Menue.AdminNotificationVM();


            //            var deshboardNotification = unit.DeshboardNotificationRepository.Get().FirstOrDefault();

            //            adminNotificationVM.FlightBooking = deshboardNotification.FlightBooking;
            //            adminNotificationVM.BookingFaieldFlight = deshboardNotification.BookingFaieldFlight;
            //            adminNotificationVM.BookingConfirmedFlight = deshboardNotification.BookingConfirmedFlight;
            //            adminNotificationVM.PriceChecked = deshboardNotification.PriceChecked;
            //            adminNotificationVM.ETicketProcessed = deshboardNotification.ETicketProcessed;
            //            adminNotificationVM.ConfirmedFlightTicketing = deshboardNotification.ConfirmedFlightTicketing;
            //            adminNotificationVM.OrderETicketProcess = deshboardNotification.OrderETicketProcess;

            //            adminNotificationVM.HotelBooking = deshboardNotification.HotelBooking;
            //            adminNotificationVM.HotelBookingConfirmed = deshboardNotification.HotelBookingConfirmed;
            //            adminNotificationVM.HotelBookingFaield = deshboardNotification.HotelBookingFaield;
            //            adminNotificationVM.HotelBookingInProcess = deshboardNotification.HotelBookingInProcess;
            //            adminNotificationVM.HotelBookingCanceled = deshboardNotification.HotelBookingCanceled;
            //            adminNotificationVM.HotelBookingCanceledFailed = deshboardNotification.HotelBookingCanceledFailed;

            //            adminNotificationVM.ETicketRequest = deshboardNotification.ETicketRequest;
            //            adminNotificationVM.ProcessedETicketRequest = deshboardNotification.ProcessedETicketRequest;


            //            adminNotificationVM.AgentRegistrationRequest = deshboardNotification.AgentRegistrationRequest;
            //            adminNotificationVM.AgentDepositRequest = deshboardNotification.AgentDepositRequest;
            //            adminNotificationVM.AgentSupportTocken = 0;
            //            adminNotificationVM.BlockedAgent = deshboardNotification.BlockedAgent;

            //            return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = true, Message = "200", Payload = adminNotificationVM }, formatter);
            //        }
            //    }
            //    catch (Exception ex)
            //    {
            //        return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = false, Message = "200", Payload = null }, formatter);
            //    }

            //}

            //[System.Web.Http.HttpPost]
            //[System.Web.Http.Route("GetBookingCalendarData")]
            //public HttpResponseMessage GetBookingCalendarData(SearchCalendarSearch searchCalendarSearch)
            //{
            //    var formatter = RequestFormat.JsonFormaterString();
            //    try
            //    {
            //        using (var unit = new Models.DAL.UnitOfWork())
            //        {
            //            List<FareCalendarResult> fareCalendarResult = new List<FareCalendarResult>();

            //            //var end = Convert.ToDateTime(searchCalendarSearch.EndDate).ToString("yyyy-MM-dd");
            //            //var start = Convert.ToDateTime(searchCalendarSearch.StartDate).ToString("yyyy-MM-dd");

            //            var airApiResponse = unit.AirTicketingRepository.Get().ToList();
            //            var hotelApiResponse = unit.HotelApiBookingCanReqReRepository.Get(a => a.Status == "CONFIRMED").ToList();

            //            foreach (var air in airApiResponse)
            //            {
            //                FareCalendarResult fareCalendar = new FareCalendarResult();
            //                if ((air.TicketingDate >= Convert.ToDateTime(searchCalendarSearch.StartDate)) && (air.TicketingDate <= Convert.ToDateTime(searchCalendarSearch.EndDate)))
            //                {
            //                    fareCalendar.BookingNumber = air.TicketNo;
            //                    fareCalendar.Link = "Air";
            //                    fareCalendar.Date = air.TicketingDate.ToString();

            //                    fareCalendarResult.Add(fareCalendar);
            //                }

            //            }

            //            foreach (var hotel in hotelApiResponse)
            //            {
            //                FareCalendarResult fareCalendar = new FareCalendarResult();
            //                if ((hotel.CreateDate >= Convert.ToDateTime(searchCalendarSearch.StartDate)) && (hotel.CreateDate <= Convert.ToDateTime(searchCalendarSearch.EndDate)))
            //                {
            //                    fareCalendar.BookingNumber = hotel.BookingId;
            //                    fareCalendar.Link = "Hotel";
            //                    fareCalendar.Date = hotel.CreateDate.ToString();

            //                    fareCalendarResult.Add(fareCalendar);
            //                }
            //            }

            //            return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = true, Message = "200", Payload = fareCalendarResult }, formatter);
            //        }
            //    }
            //    catch (Exception ex)
            //    {
            //        return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = false, Message = "200", Payload = null }, formatter);
            //    }

            //}

            //[System.Web.Http.HttpPost]
            //[System.Web.Http.Route("MonthWiseBookingData")]
            //public HttpResponseMessage MonthWiseBookingData()
            //{
            //    try
            //    {
            //        using (var unit = new UnitOfWork())
            //        {
            //            var monthBookingData = unit.MonthWiseBookingDataRepository.GetAll();

            //            var formatter = RequestFormat.JsonFormaterString();
            //            return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = true, Message = "200", Payload = monthBookingData }, formatter);
            //        }
            //    }
            //    catch (Exception)
            //    {
            //        var formatter = RequestFormat.JsonFormaterString();
            //        return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = false, Message = "200", Payload = null }, formatter);
            //    }
            //}

            //[System.Web.Http.HttpPost]
            //[System.Web.Http.Route("NotificationBar")]
            //public HttpResponseMessage NotificationBar()
            //{
            //    var formatter = RequestFormat.JsonFormaterString();
            //    try
            //    {
            //        using (var unit = new Models.DAL.UnitOfWork())
            //        {
            //            Models.ViewModel.Menue.NotificationBarVM notificationBarVM = new Models.ViewModel.Menue.NotificationBarVM();

            //            notificationBarVM.RegisteredAgentCount = unit.AgentRegistrationRequestRepository.Get(a => a.IsSeen == null || a.IsSeen == false).Count();

            //            notificationBarVM.ETicketCount = unit.ETicketRequestRepository.Get(a => a.IsSeen == null || a.IsSeen == false).Count();

            //            notificationBarVM.DepositRequestCount = unit.AgentDepositRequestRepository.Get(a => a.IsSeen == null || a.IsSeen == false).Count();

            //            notificationBarVM.SupportTicketCount = 0;

            //            return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = true, Message = "200", Payload = notificationBarVM }, formatter);
            //        }
            //    }
            //    catch (Exception ex)
            //    {
            //        return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = false, Message = "200", Payload = null }, formatter);
            //    }

            //}


            //[System.Web.Http.HttpPost]
            //[System.Web.Http.Route("RegisteredAgentNotification")]
            //public HttpResponseMessage RegisteredAgentNotification()
            //{
            //    var formatter = RequestFormat.JsonFormaterString();
            //    try
            //    {
            //        using (var unit = new Models.DAL.UnitOfWork())
            //        {
            //            List<Models.ViewModel.Menue.RegisteredAgent> registeredAgentList = new List<Models.ViewModel.Menue.RegisteredAgent>();

            //            var RegisteredAgentDB = unit.AgentRegistrationRequestRepository.Get(a => a.IsSeen == null || a.IsSeen == false).Select(a => new { a.id, a.Name, a.RequestDate }).ToList().Take(5);

            //            foreach (var item in RegisteredAgentDB)
            //            {
            //                var AgentRegRequest = unit.AgentRegistrationRequestRepository.GetByID(item.id);
            //                AgentRegRequest.IsSeen = true;
            //                unit.Save();

            //                RegisteredAgent registeredAgent = new RegisteredAgent();

            //                registeredAgent.AgentID = item.id;
            //                registeredAgent.AgentName = item.Name;
            //                DateTime? dt = (DateTime?)item.RequestDate;

            //                registeredAgent.RegistrationDate = string.Format("{0:d}", dt);
            //                registeredAgentList.Add(registeredAgent);
            //            }

            //            return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = true, Message = "200", Payload = registeredAgentList }, formatter);
            //        }
            //    }
            //    catch (Exception ex)
            //    {
            //        return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = false, Message = "200", Payload = null }, formatter);
            //    }

            //}


            //[System.Web.Http.HttpPost]
            //[System.Web.Http.Route("EticketNotification")]
            //public HttpResponseMessage EticketNotification()
            //{
            //    var formatter = RequestFormat.JsonFormaterString();
            //    try
            //    {
            //        using (var unit = new Models.DAL.UnitOfWork())
            //        {
            //            List<Models.ViewModel.Menue.ETicket> ETicletList = new List<Models.ViewModel.Menue.ETicket>();

            //            var ETicketDB = unit.ETicketRequestRepository.Get(a => a.IsSeen == null || a.IsSeen == false, includeProperties: "ApiUser").Select(a => new { a.EticketrequestID, a.ApiUser.id, a.ApiUser.UserName, a.IGXRefNo, a.RequestDate }).ToList().Take(5);

            //            foreach (var item in ETicketDB)
            //            {
            //                var Eticket = unit.ETicketRequestRepository.GetByID(item.EticketrequestID);
            //                Eticket.IsSeen = true;
            //                unit.Save();
            //                Models.ViewModel.Menue.ETicket eTicket = new ETicket();
            //                eTicket.AgentID = item.id;
            //                eTicket.AgentName = item.UserName;
            //                eTicket.ETicketReqID = item.EticketrequestID;
            //                eTicket.IGXRefNo = item.IGXRefNo;
            //                eTicket.RequestDate = item.RequestDate.ToShortDateString();
            //                ETicletList.Add(eTicket);

            //            }

            //            return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = true, Message = "200", Payload = ETicletList }, formatter);
            //        }
            //    }
            //    catch (Exception ex)
            //    {
            //        return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = false, Message = "200", Payload = null }, formatter);
            //    }

            //}




        }
    
}