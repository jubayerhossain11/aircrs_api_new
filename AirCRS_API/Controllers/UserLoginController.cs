﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using Microsoft.AspNet.Identity.Owin;
using System.Threading.Tasks;
using AirCRS_API;
using AirCRS_API.DAL.DBContext;
using AirCRS_API.Models;
using AirCRS_API.Models.UniversalModel;

namespace AirCRS_API.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class UserLoginController : ApiController
    {

        private AirCRSDBContext db = new AirCRSDBContext();

        private ApplicationUserManager _userManager;

        public UserLoginController()
        {
        }

        public UserLoginController(ApplicationUserManager userManager,
            ISecureDataFormat<Microsoft.Owin.Security.AuthenticationTicket> accessTokenFormat)
        {
            UserManager = userManager;
            //AccessTokenFormat = accessTokenFormat;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        //GET: test
       [System.Web.Http.Route("user/authenticate")]
       [System.Web.Http.HttpPost]
        public async Task<HttpResponseMessage> Login(UserLoginModel userLoginModel)
        {
            UserLoginResponseModel responseModel = new UserLoginResponseModel();
            try
            {
                string jsonString = GetToken(userLoginModel);
                jsonString = jsonString.Replace(".expires", "expires");
                jsonString = jsonString.Replace(".issued", "issued");
                var token = JsonConvert.DeserializeObject<Token>(jsonString);
                responseModel.access_token = token.access_token;
                DateTime dt;
                try
                {
                    dt = DateTime.ParseExact(token.expires, "ddd, dd MMM yyyy HH:mm:ss 'GMT'", System.Globalization.CultureInfo.InvariantCulture);
                }
                catch (Exception)
                {
                    dt = DateTime.Now.AddHours(24);
                }
                responseModel.expires_in = dt.ToString("dd/MM/yyyy HH:mm:ss");
                responseModel.token_type = token.token_type;
                responseModel.userName = userLoginModel.UserName;

                string json = JsonConvert.SerializeObject(responseModel);
                var resp = new HttpResponseMessage()
                {
                    Content = new StringContent(json)
                };
                resp.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                return resp;
            }
            catch (Exception ex)
            {
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Unauthorized);
                return response;
            }
        }

        static string GetToken(UserLoginModel userLoginModel)
        {
            string host = HttpContext.Current.Request.Url.ToString().Replace("user/authenticate", "");


            var pairs = new List<KeyValuePair<string, string>>
                    {
                        new KeyValuePair<string, string>( "grant_type", "password" ),
                        new KeyValuePair<string, string>( "username", userLoginModel.UserName ),
                        new KeyValuePair<string, string> ( "Password", userLoginModel.Password )
                    };
            var content = new FormUrlEncodedContent(pairs);
            ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;
            using (var client = new HttpClient())
            {

                var response = client.PostAsync(host + "Token", content).Result;
                var jsonString = response.Content.ReadAsStringAsync();
                jsonString.Wait();
                return jsonString.Result;
            }
        }
        
        [System.Web.Http.Route("CheckToken")]
        [System.Web.Http.HttpPost]
        public HttpResponseMessage CheckToken()
        {
            try
            {
                var user = User.Identity.GetUserId();

                if (user!=null)
                {
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                    return response;
                }
                else
                {
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Unauthorized);
                    return response;
                }
            }
            catch (Exception ex)
            {
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Unauthorized);
                return response;
            }

        }

    }
}