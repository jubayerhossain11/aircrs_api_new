﻿using AirCRS_API.SERVICE.AirPriceServices;
using AirCRS_API.SERVICE.AirPriceServices.Dtos;
using AirCRS_API.SERVICE.AirSearchApiServices.ApiRequestDots;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;

namespace AirCRS_API.Controllers
{
    [System.Web.Http.RoutePrefix("AirPrice")]
    public class AirPriceController : ApiController
    {

        private string EncryptionKey = "2@3G#kja$#kmm";
        private IAirPriceService _airPriceServices;
        public AirPriceController(IAirPriceService airPriceServices)
        {
            _airPriceServices = airPriceServices;
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("AirPriceRequest")]
        public HttpResponseMessage AirPriceRequest(HttpRequestMessage request)//GetAirPriceCheckResponseData
        {
            try
            {
                string jsonContent = request.Content.ReadAsStringAsync().Result;
                AirPriceCheckRequestInput airSearchRequest = JsonConvert.DeserializeObject<AirPriceCheckRequestInput>(jsonContent);

                //request validation
                if (string.IsNullOrEmpty(airSearchRequest.AirPricingSolution_Key) || airSearchRequest.AirPricingSolution_Key.Length != 12)
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Your AirPricingSolution_Key Expaired Or Not Correct !!");

                AirPriceCheckRequestDto AirPriceCheckRequest = new AirPriceCheckRequestDto();
                AirPriceCheckDto airPriceCheckResponse = new AirPriceCheckDto();

                var response = _airPriceServices.GetAirPriceRequest(airSearchRequest);
                if (response == null)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Your AirPricingSolution_Key Expaired Or Not Correct !!");
                }
                else
                {
                    return JsonSerialize(response);
                }

            }
            catch(Exception er)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, er.Message);
            }
        }

        private HttpResponseMessage JsonSerialize<T>(T obj)
        {
            string json = JsonConvert.SerializeObject(obj);

            var resp = new HttpResponseMessage()
            {
                Content = new StringContent(json)
            };
            resp.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            return resp;
        }

    }
}
