﻿using AirCRS_API.DAL.Entities;
using AirCRS_API.Models;
using AirCRS_API.Models.UniversalModel;
using AirCRS_API.SERVICE.TripServices;
using AirCRS_API.SERVICE.TripServices.Dtos;
using Microsoft.AspNet.Identity;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FlamingoWebApi.Controllers
{
    [System.Web.Http.Authorize]
    public class TripReportController : ApiController
    {
        private readonly ITripService _tripService;
        public TripReportController(ITripService tripService)
        {
            _tripService = tripService;
        }

        [Route("GetTripReportList")]
        [HttpPost]
        public HttpResponseMessage GetTripReportList(TripSearchInputDto input)
        {
            var formatter = RequestFormat.JsonFormaterString();
            try
            {
                input.CreatedBy = User.Identity.GetUserId();
                var response = _tripService.GetTripReportList(input);
                return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = true, Message = "PNR Booking List", Payload = response }, formatter);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = false, Message = "Data Not Found", Payload = null }, formatter);
            }
        }

        [Route("GetTicketInfo")]
        [HttpPost]
        public HttpResponseMessage GetTicketInfo(PnrLog input)
        {
            var formatter = RequestFormat.JsonFormaterString();
            try
            {
                var response = _tripService.GetTicketInfo(input);
                return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = true, Message = "Tiket Information", Payload = response }, formatter);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = false, Message = "Data Not Found", Payload = null }, formatter);
            }
        }
    }
}