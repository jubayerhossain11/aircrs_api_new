﻿using AirCRS_API.DAL.DBContext;
using AirCRS_API.DAL.Entities;
using AirCRS_API.Models;
using AirCRS_API.Models.UniversalModel;
using AirCRS_API.SERVICE.SegmentServices;
using AirCRS_API.SERVICE.TripServices;
using AirCRS_API.SERVICE.TripServices.Dtos;
using AirCRS_API.Utils;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;
using System.Web.Http;

namespace AirCRS_API.Controllers
{
    [System.Web.Http.Authorize]
    public class TripController : ApiController
    {

        private readonly ITripService _tripService;
        private readonly ISegmentService _segmentService;
        public TripController(ITripService tripService,
            ISegmentService segmentService)
        {
            _tripService = tripService;
            _segmentService = segmentService;
        }

        [Route("GetAllTrip")]
        [HttpPost]
        public HttpResponseMessage GetTripList(TripSearchInputDto input)
        {
            var formatter = RequestFormat.JsonFormaterString();
            try
            {
                using (AirCRSDBContext unit = new AirCRSDBContext())
                {
                    var loggedinUserId = User.Identity.GetUserId();
                    var pmsGroupId = unit.Users.Find(loggedinUserId).PmsGroupID;
                    var isAdmin = User.IsInRole("Admin");

                    DateTime? startDate = null;
                    DateTime? endDate = null;

                    if (input.StartDate.HasValue && input.EndDate.HasValue)
                    {
                        endDate = new DateTime(input.EndDate.Value.Year, input.EndDate.Value.Month, input.EndDate.Value.Day, 23, 59, 59, 999);
                        startDate = new DateTime(input.StartDate.Value.Year, input.StartDate.Value.Month, input.StartDate.Value.Day, 0, 0, 0, 0);
                    }
                    var tripList = _tripService.GetTripList(pmsGroupId, loggedinUserId, input.IdentityUserID, isAdmin, startDate, endDate, input.MyTripOnly);

                    var response = new TripResponseDto()
                    {
                        TripList = tripList,
                        IsSysAdmin = isAdmin
                    };
                    return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = true, Message = "Trip Created", Payload = response }, formatter);
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = false, Message = "Data Not Found", Payload = null }, formatter);
            }
        }

        [System.Web.Http.Route("TripCreateOrUpdate")]
        [System.Web.Http.HttpPost]
        public HttpResponseMessage InsertOrUpdateTrip(TripVM tripInput)
        {
            var AirportTimes = UtilsGetAirLines.GetAllAirLinesData();

            var formatter = RequestFormat.JsonFormaterString();
            try
            {
                tripInput.CreatedBy = User.Identity.GetUserId();
                var response = _tripService.CreateOrUpdateTrip(tripInput, AirportTimes);

                return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = response, Message = response ? "" : "Error Occourred", Payload = null }, formatter);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = false, Message = ex.Message, Payload = null }, formatter);
            }


        }

        [Route("GetSegmentByTrip")]
        [HttpPost]
        public HttpResponseMessage GetSegmentByTrip(Segment input)
        {
            var formatter = RequestFormat.JsonFormaterString();
            try
            {
                var segmentData = _segmentService.GetSegmentByTrip(input);

                return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = true, Message = "Data Found", Payload = segmentData }, formatter);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = false, Message = "Data Not Found", Payload = null }, formatter);
            }
        }


        [Route("GetTripDetails")]
        [HttpPost]
        public HttpResponseMessage GetTripDetails(TripVM input)
        {
            var formatter = RequestFormat.JsonFormaterString();
            try
            {
                var tripInfo = _tripService.GetTripDetails(input);

                return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = true, Message = "Data Found", Payload = tripInfo }, formatter);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = false, Message = "Data Not Found", Payload = null }, formatter);
            }
        }

        [Route("ChangeTripStatus")]
        [HttpPost]
        public HttpResponseMessage ChangeTripStatus(Trip tripInput)
        {
            var formatter = RequestFormat.JsonFormaterString();
            try
            {
                var res = _tripService.ChangeTripStatus(tripInput);
                return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = true, Message = "Trip Status Updated", Payload = null }, formatter);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = false, Message = "Data Not Found", Payload = null }, formatter);
            }
        }

        [Route("GetPriceByTrip")]
        [HttpPost]
        public HttpResponseMessage GetPriceByTrip(Trip tripInput)
        {
            var formatter = RequestFormat.JsonFormaterString();
            try
            {
                var response = _tripService.GetTripPriceForUpdate(tripInput.TripId, User.Identity.GetUserId());
                return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = true, Message = "Trip Price Updated", Payload = response }, formatter);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = false, Message = "Data Not Found", Payload = null }, formatter);
            }
        }


        [Route("GetTicketByTrip")]
        [HttpPost]
        public HttpResponseMessage GetTicketByTrip(Trip tripInput)
        {
            var formatter = RequestFormat.JsonFormaterString();
            try
            {
                var output = _tripService.GetTicketByTrip(tripInput);
                return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = true, Message = "Trip Price Updated", Payload = output }, formatter);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = false, Message = "Data Not Found", Payload = null }, formatter);
            }
        }

        [Route("ChangeTripActiveStatus")]
        [HttpPost]
        public HttpResponseMessage ChangeTripActiveStatus(Trip tripInput)
        {
            var formatter = RequestFormat.JsonFormaterString();
            try
            {
                var res = _tripService.ChangeTripActiveStatus(tripInput);
                return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = res, Message = "Trip Active Status Updated", Payload = null }, formatter);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = false, Message = "Data Not Found", Payload = null }, formatter);
            }
        }

        [Route("UpdateTripPrice")]
        [HttpPost]
        public HttpResponseMessage UpdateTripPrice(List<PriceBreakdown> prices)
        {
            var modifiedBy = User.Identity.GetUserId();
            var formatter = RequestFormat.JsonFormaterString();
            try
            {
                var res = _tripService.UpdateTripPriceBreakDown(prices, modifiedBy);
                return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = true, Message = "Trip Active Status Updated", Payload = null }, formatter);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = false, Message = "Data Not Found", Payload = null }, formatter);
            }
        }

        [Route("UpdateTripTickets")]
        [HttpPost]
        public HttpResponseMessage UpdateTripTickets(TicketUpdateVM input)
        {
            var formatter = RequestFormat.JsonFormaterString();
            try
            {
                input.CreatedBy = User.Identity.GetUserId();
                var res = _tripService.UpdateTripTickets(input);
                return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = res, Message = "Trip Tickets Updated", Payload = null }, formatter);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = false, Message = "Data Not Found", Payload = null }, formatter);
            }
        }

        [Route("GetCrsAdminDashboardSummary")]
        [HttpPost]
        public HttpResponseMessage GetCrsAdminDashboardSummary()
        {
            var formatter = RequestFormat.JsonFormaterString();
            try
            {
                var output = _tripService.GetCrsAdminDashboardSummary(User.Identity.GetUserId());
                return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = true, Message = "CRS Admin Dashboard", Payload = output }, formatter);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = false, Message = "Data Not Found", Payload = null }, formatter);
            }

        }

        [Route("GetAllBookingList")]
        [HttpPost]
        public HttpResponseMessage GetAllBookingList(SearchInputModel input)
        {
            var formatter = RequestFormat.JsonFormaterString();
            try
            {
                input.LogedInUserId = User.Identity.GetUserId();
                var output = _tripService.GetAllBookingList(input);
                return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = true, Message = "PNR Booking List", Payload = output }, formatter);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = false, Message = "Data Not Found", Payload = null }, formatter);
            }

        }

        [Route("GetNewBookingNotification")]
        [HttpPost]
        public HttpResponseMessage GetNewBookingNotification()
        {
            var formatter = RequestFormat.JsonFormaterString();
            try
            {
                var output = _tripService.GetNewBookingNotification();
                return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = true, Message = "PNR New Booking", Payload = output }, formatter);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = false, Message = "Data Not Found", Payload = null }, formatter);
            }
        }

        [Route("ValidateTripTicketNumber")]
        [HttpPost]
        public HttpResponseMessage ValidateTripTicketNumber(List<string> tickets)
        {
            var formatter = RequestFormat.JsonFormaterString();
            try
            {
                var hasTickets = _tripService.ValidateTripTicketNumber(tickets);
                if (!hasTickets.Any())
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = true, Message = "Duplicate Tickets not Found", Payload = null });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = false, Message = "Duplicate Tickets Found", Payload = hasTickets });
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = false, Message = "Data Not Found", Payload = null }, formatter);
            }
        }

        [Route("GetTicketByPnr")]
        [HttpPost]
        public HttpResponseMessage GetTicketByPnr(Trip tripInput)
        {
            var formatter = RequestFormat.JsonFormaterString();
            try
            {
                var output = _tripService.GetTicketByPnr(tripInput.TripId);
                output.LoggedInUserId = output !=  null ? output.LoggedInUserId = User.Identity.GetUserId() : "";
                return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = true, Message = "Trip Price Updated", Payload = output }, formatter);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = false, Message = "Data Not Found", Payload = null }, formatter);
            }
        }

        [Route("GetTripExtraService")]
        [HttpPost]
        public HttpResponseMessage GetTripExtraService(Trip tripInput)
        {
            var formatter = RequestFormat.JsonFormaterString();
            try
            {
                var output = _tripService.GetTripExtraService(tripInput.TripId);
                return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = true, Message = "", Payload = output }, formatter);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = false, Message = "Data Not Found", Payload = null }, formatter);
            }


        } 
        
        [Route("GetTripBaggageForUpdate")]
        [HttpPost]
        public HttpResponseMessage GetTripBaggageForUpdate(BaggageDto input)
        {
            var formatter = RequestFormat.JsonFormaterString();
            try
            {
                var output = _tripService.GetBaggageForUpdate(input.Id);
                return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = true, Message = "", Payload = output }, formatter);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = false, Message = "Data Not Found", Payload = null }, formatter);
            }


        }
        
        [Route("InsertUpdateTripBaggage")]
        [HttpPost]
        public HttpResponseMessage InsertUpdateTripBaggage(BaggageDto input)
        {
            var formatter = RequestFormat.JsonFormaterString();
            try
            {
                input.CreatedBy = User.Identity.GetUserId();
                input.ModifiedBy = User.Identity.GetUserId();

                var output = _tripService.CreateOrUpdateBaggage(input);
                return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = true, Message = "", Payload = output }, formatter);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = false, Message = "Data Not Found", Payload = null }, formatter);
            }
        }
        
        [Route("GetTripMealForUpdate")]
        [HttpPost]
        public HttpResponseMessage GetTripMealForUpdate(MealsDto input)
        {
            var formatter = RequestFormat.JsonFormaterString();
            try
            {
                var output = _tripService.GetMealForUpdate(input.Id);
                return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = true, Message = "", Payload = output }, formatter);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = false, Message = "Data Not Found", Payload = null }, formatter);
            }
        }
        
        [Route("InsertUpdateTripMeal")]
        [HttpPost]
        public HttpResponseMessage InsertUpdateTripMeal(MealsDto input)
        {
            var formatter = RequestFormat.JsonFormaterString();
            try
            {
                input.CreatedBy = User.Identity.GetUserId();
                input.ModifiedBy = User.Identity.GetUserId();

                var output = _tripService.CreateOrUpdateMeal(input);
                return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = true, Message = "", Payload = output }, formatter);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = false, Message = "Data Not Found", Payload = null }, formatter);
            }
        }
        
        [Route("InsertUpdateTripWheelChair")]
        [HttpPost]
        public HttpResponseMessage InsertUpdateTripWheelChair(WheelChairDto input)
        {
            var formatter = RequestFormat.JsonFormaterString();
            try
            {
                input.CreatedBy = User.Identity.GetUserId();
                input.ModifiedBy = User.Identity.GetUserId();

                var output = _tripService.CreateOrUpdateWheelChair(input);
                return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = true, Message = "", Payload = output }, formatter);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = false, Message = "Data Not Found", Payload = null }, formatter);
            }
        }
        
        [Route("CheckGroupUniqueNumber")]
        [HttpPost]
        public HttpResponseMessage CheckGroupUniqueNumber(Trip input)
        {
            var formatter = RequestFormat.JsonFormaterString();
         
            try
            {
                input.CreatedBy = User.Identity.GetUserId();
            
                var output = _tripService.CheckGroupUniqueNumber(input.TripUniqueNo, input.CreatedBy);
                return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = output, Message = "", Payload = output }, formatter);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = false, Message = "Data Not Found", Payload = null }, formatter);
            }
        }


        [Route("GetPriceBreakDownForView")]
        [HttpPost]
        public HttpResponseMessage GetPriceBreakDownForView(Trip input)
        {
            var formatter = RequestFormat.JsonFormaterString();
            try
            {
                var output = _tripService.GetPriceBreakDownForViewPartial(input.TripId);
                return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = true, Message = "", Payload = output }, formatter);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = false, Message = "Data Not Found", Payload = null }, formatter);
            }
        }

    }
}