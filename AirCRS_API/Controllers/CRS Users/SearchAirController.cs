﻿using AirCRS_API.DAL.DBContext;
using AirCRS_API.DAL.Entities;
using AirCRS_API.Models;
using AirCRS_API.Models.UniversalModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace FlamingoWebApi.Controllers.CRS_Users
{
    public class SearchAirController : ApiController
    {
        [System.Web.Http.Route("GetLocation")]
        [System.Web.Http.HttpPost]
        public HttpResponseMessage GetLocationList(AirportSearchCode vm)
        {
            using (AirCRSDBContext unit = new AirCRSDBContext())
            {
                var list = unit.AirportSearchCodes.Where(x => x.AirportCode.ToLower().Contains(vm.AirportCode.ToLower())).Select(x => new LocationOutputVM() { Label = x.SearchString, Value = x.AirportCode }).ToList();
                var formatter = RequestFormat.JsonFormaterString();
                return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = true, Message = "Data Found", Payload = list }, formatter);

            }
        }

        [System.Web.Http.Route("GetAirLines")]
        [System.Web.Http.HttpPost]
        public HttpResponseMessage GetAirLinesList(Airline input)
        {
            using (AirCRSDBContext unit = new AirCRSDBContext())
            {
                var list = unit.Airlines.Where(x => x.AriLineName.ToLower().Contains(input.AriLineName.ToLower())).Select(x => new AirLineOutputVM() { Label = x.AriLineName, Value = x.Id }).ToList();
                var formatter = RequestFormat.JsonFormaterString();
                return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = true, Message = "Data Found", Payload = list }, formatter);
            }
        }

    }
}