﻿
using AirCRS_API;
using AirCRS_API.DAL.DBContext;
using AirCRS_API.DAL.Entities;
using AirCRS_API.Helper;
using AirCRS_API.Models;
using AirCRS_API.Models.UniversalModel;
using AirCRS_API.SERVICE.CrsUserServices;
using AirCRS_API.SERVICE.CrsUserServices.Dtos;
using AirCRS_API.SERVICE.TripServices.Dtos;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Transactions;
using System.Web;
using System.Web.Http;

namespace FlamingoWebApi.Controllers.CRS_Users
{
    [System.Web.Http.Authorize]
    public class CrsUserController : ApiController
    {
        private readonly ICrsUserServic _crsUserService;
        private ApplicationUserManager _userManager;

        public CrsUserController(ICrsUserServic crsUserService)
        {
            _crsUserService = crsUserService;

        }

        public CrsUserController(ApplicationUserManager userManager,
            ISecureDataFormat<AuthenticationTicket> accessTokenFormat)
        {
            UserManager = userManager;
            // AccessTokenFormat = accessTokenFormat;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }


        [System.Web.Http.Route("GetCrsUserById")]
        [System.Web.Http.HttpPost]
        public HttpResponseMessage GetCrsUserById(CrsUserOutputDto ReqApi)
        {

            try
            {
                var objOfViewModel = _crsUserService.GetCrsUserById(ReqApi);

                var formatter = RequestFormat.JsonFormaterString();
                return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = true, Message = "Data Found", Payload = objOfViewModel }, formatter);

            }
            catch (Exception ex)
            {
                var formatter = RequestFormat.JsonFormaterString();
                return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = false, Message = "Data Not Found", Payload = null }, formatter);
            }
        }

        [System.Web.Http.Route("InsertOrUpdateCrsUser")]
        [System.Web.Http.HttpPost]
        public async System.Threading.Tasks.Task<HttpResponseMessage> InsertOrUpdateAsync(CrsUser crsUser)
        {
            CrsUserOutputDto objOfViewModel = new CrsUserOutputDto();
            var formatter = RequestFormat.JsonFormaterString();
            try
            {
                    using (AirCRSDBContext unit = new AirCRSDBContext())
                    {

                        CrsUser crs = unit.CrsUsers.Find(crsUser.Id);

                        if (crs == null)
                        {
                            var user = new AirCRS_API.Models.ApplicationUser() { PmsGroupID = 2, UserName = crsUser.LoginID, Email = crsUser.UserEmail, LockoutEnabled = true };

                            try
                            {
                                IdentityResult result =
                                    await UserManager.CreateAsync(user, crsUser.UserPassword); //code for adding identity user for authentication
                                var user2 = unit.Users.Find(user.Id);

                                if (!result.Succeeded)
                                {
                                    return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = false, Message = "Error Creating Crs User. User Name or Email are Duplicate !!!!" }, formatter);
                                }

                                if (result.Succeeded)
                                {

                                    //var resultRole = await UserManager.AddToRolesAsync(user.Id, "CrsUser");

                                    string removeFile = "";
                                    try
                                    {
                                        #region CRS user Save 

                                        using (AirCRSDBContext unit2 = new AirCRSDBContext())
                                        {
                                            crsUser.UserTypeID = 5;// Type ID 1 is for B2B User
                                            crsUser.CreateDate = DateTime.Now;
                                            crsUser.CreatedBy = User.Identity.GetUserId();
                                            crsUser.CreateTime = DateTime.Now;
                                            crsUser.IsRemoved = false;
                                            crsUser.BannerName = crsUser.BannerName;
                                            crsUser.CrsName = crsUser.CrsName;
                                            crsUser.CrsLogoName = crsUser.CrsLogoName;
                                            crsUser.LogoImageUrl = crsUser.LogoImageUrl;
                                            crsUser.Address = crsUser.Address;

                                            crsUser.IdentityUserID = user.Id; //result.Result.ToString();

                                            unit2.CrsUsers.Add(crsUser);
                                            unit2.SaveChanges();
                                        }

                                        #endregion

                                    }
                                    catch (System.Transactions.TransactionException ex)
                                    {
                                        IdentityResult result2 =
                                    await UserManager.DeleteAsync(user);

                                        return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = false, Message = " Error Creating New Agent  !!!!" }, formatter);

                                    }

                                }

                                return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = true, Message = " New Agent Created  !!!!" }, formatter);
                            }
                            catch (Exception ex)
                            {
                                IdentityResult result2 =
                                    await UserManager.DeleteAsync(user); // code for delete identity user which created by the new api user 

                                return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = false, Message = "Error Creating Crs User  !!!!" }, formatter);
                            }
                        }
                        if (crs != null)
                        {
                            //crsUser.CreateDate = DateTime.Now;
                            //crsUser.CreateTime = DateTime.Now;
                            //crsUser.CreatedBy = User.Identity.GetUserId();
                            crsUser.Id = crs.Id;
                            crsUser.IdentityUserID = crs.IdentityUserID;
                            crsUser.IsRemoved = false;
                            crsUser.LoginID = crs.LoginID;
                            //apiUser.UserMobileNo = api.UserMobileNo;
                            crsUser.UserTypeID = crs.UserTypeID;
                            crsUser.Address = crs.Address;

                            if (crsUser.LogoImageUrl == null || crsUser.LogoImageUrl == "")
                            {
                                crsUser.LogoImageUrl = crs.LogoImageUrl;
                                crsUser.CrsLogoName = crs.CrsLogoName;
                            }

                            if (crs.UserPassword != crsUser.UserPassword)
                            {

                                UserManager.ChangePassword(crs.IdentityUserID, crs.UserPassword, crsUser.UserPassword);
                                //UserManager.Update(new ApplicationUser() {Id = crs.IdentityUserID, PmsGroupID = 3028, UserName = crsUser.LoginID, Email = crsUser.UserEmail, LockoutEnabled = true });
                            }
                            using (AirCRSDBContext unit2 = new AirCRSDBContext())
                            {
                                try
                                {
                                    unit2.Entry(crsUser).State = EntityState.Modified;
                                    unit2.SaveChanges();

                                    return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = true, Message = crsUser.UserTypeID.ToString() }, formatter);
                                }
                                catch (Exception ex)
                                {
                                    return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = false, Message = "Error Updating Agent  !!!!" }, formatter);
                                }
                            }
                        }

                        return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = true, Message = "Agent Created", Payload = null }, formatter);
                    }
               


            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = false, Message = "Data Not Found", Payload = objOfViewModel }, formatter);
            }
        }

        [System.Web.Http.Route("GetCrsUserList")]
        [System.Web.Http.HttpPost]
        public HttpResponseMessage getCrsUserlist()
        {
            using (AirCRSDBContext unit2 = new AirCRSDBContext())
            {
                List<CrsUser> crsUserList = unit2.CrsUsers.Where(x => x.UserTypeID == 5).ToList();
                var formatter = RequestFormat.JsonFormaterString();
                return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = false, Message = "Data Not Found", Payload = crsUserList }, formatter);
            }
        }

        [System.Web.Http.Route("GetCrsUserlistByPmsGroupId")]
        [System.Web.Http.HttpPost]
        public HttpResponseMessage GetCrsUserlistByPmsGroupId()
        {
            using (AirCRSDBContext unit = new AirCRSDBContext())
            {
                List<CrsUser> crsUserList = UserManager.Users.Where(x => x.PmsGroupID == 2).Select(x => new CrsUser()
                {
                    CrsName = x.UserName,
                    IdentityUserID = x.Id
                }).ToList();

                var response = new TripResponseDto()
                {
                    CrsUsers = crsUserList,
                    IsSysAdmin = User.IsInRole("Admin")
                };

                var formatter = RequestFormat.JsonFormaterString();
                return Request.CreateResponse(HttpStatusCode.OK, new PayloadResponse { Success = false, Message = "Data Not Found", Payload = response }, formatter);
            }
        }

        [System.Web.Http.Route("GetCrsInfoById")]
        [System.Web.Http.HttpPost]
        public HttpResponseMessage GetCrsUserInfoById(int crsId)
        {
            try
            {
                using (AirCRSDBContext unit = new AirCRSDBContext())
                {
                    var crsUserInfo = unit.CrsUsers.Find(crsId);

                    CrsUser crsUser = new CrsUser();
                    crsUser.CrsLogoName = crsUserInfo.CrsLogoName;
                    crsUser.BannerName = crsUserInfo.BannerName;

                    crsUser.LogoImageUrl = AirCRS_API.Properties.Settings.Default.ApplicatioUrl + crsUserInfo.LogoImageUrl;

                    return JsonHelper.JsonSerialize(new PayloadResponse { Success = true, Message = "Data Found", Payload = crsUser });
                }

            }
            catch (Exception)
            {
                return JsonHelper.JsonSerialize(new PayloadResponse { Success = false, Message = "Data Not Found", Payload = null });
            }
        }

    }
}