﻿using AirCRS_API.DAL.DBContext;
using AirCRS_API.DAL.Entities;
using AirCRS_API.DAL.Enums;
using AirCRS_API.SERVICE.AirPriceServices.Dtos;
using AirCRS_API.SERVICE.AirSearchApiServices;
using AirCRS_API.SERVICE.AirSearchApiServices.ApiRequestDots;
using AirCRS_API.SERVICE.AirSearchApiServices.Dtos;
using AirModels;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Transactions;
using System.Web.Http;

namespace FlamingoWebApi.Controllers
{
    [Authorize]
    public class AirSearchController : ApiController
    {
        private string EncryptionKey = "2@3G#kja$#kmm";
        private readonly IAirSearchApiService _airSearchService;
        public AirSearchController(IAirSearchApiService airSearchService)
        {
            _airSearchService = airSearchService;
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("GetAirSearchData")]
        public HttpResponseMessage GetAirSearchData(HttpRequestMessage request)
        {
            List<AirSearchResponse> airSearchResponse = new List<AirSearchResponse>();

            string jsonContent = request.Content.ReadAsStringAsync().Result;
            AirSearchRequestInput airSearchRequest = JsonConvert.DeserializeObject<AirSearchRequestInput>(jsonContent);

            if (string.IsNullOrEmpty(airSearchRequest.DepartureDate) || string.IsNullOrEmpty(airSearchRequest.Origin) || string.IsNullOrEmpty(airSearchRequest.Origin) || airSearchRequest.JourneyType < 0)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Invalid Search Parameter !!");
            }

            DateTime FormatedDepartureDate = Convert.ToDateTime(airSearchRequest.DepartureDate); //DateTime.Parse(airSearchRequest.DepartureDate);

            if (FormatedDepartureDate < DateTime.Now)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Departure Date Should be gartter then current date !!");
            }
            var loggedinUserId = User.Identity.GetUserId();
            var totalRequestedTickets = airSearchRequest.NoOfAdult + airSearchRequest.NoOfChildren + airSearchRequest.NoOfInfant;

            airSearchResponse = _airSearchService.GerAirSearchData(airSearchRequest, FormatedDepartureDate, loggedinUserId, totalRequestedTickets);

            return JsonSerialize(airSearchResponse);
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("AirPriceRequest")]
        public HttpResponseMessage AirPriceRequest(HttpRequestMessage request)//GetAirPriceCheckResponseData
        {
            AirPriceCheckRequest AirPriceCheckRequest = new AirPriceCheckRequest();
            AirPriceCheckResponse airPriceCheckResponse = new AirPriceCheckResponse();

            string jsonContent = request.Content.ReadAsStringAsync().Result;
            AirPriceCheckRequestInput airPriceCheckRequest = JsonConvert.DeserializeObject<AirPriceCheckRequestInput>(jsonContent);

            //request validation
            if (string.IsNullOrEmpty(airPriceCheckRequest.AirPricingSolution_Key))
                return Request.CreateErrorResponse(HttpStatusCode.OK, "Your AirPricingSolution_Key Expaired Or Not Correct !!");

            // AirPricingSolution_Key validation

            var decriptCode = DecryptString(airPriceCheckRequest.AirPricingSolution_Key, EncryptionKey).Split('|');

            var getRequestLogDataByLogId = _airSearchService.GetApiRequestLog(Convert.ToInt32(decriptCode[0]));
            var priceBreakDownId = Convert.ToInt32(decriptCode[1]);

            if (getRequestLogDataByLogId == null)
            {
                return Request.CreateErrorResponse(HttpStatusCode.OK, "Your AirPricingSolution_Key Expaired Or Not Correct !!");
            }
            else
            {
                airPriceCheckResponse = _airSearchService.GetAirPriceData(airPriceCheckRequest, getRequestLogDataByLogId, priceBreakDownId);
            }

            airPriceCheckResponse.ApiOriginalPrice = new ApiOriginalPrice();
            airPriceCheckResponse.ApiOriginalPrice.TotalPrice = airPriceCheckResponse.TotalPrice;
            airPriceCheckResponse.ApiOriginalPrice.BasePrice = airPriceCheckResponse.BasePrice;
            airPriceCheckResponse.ApiOriginalPrice.TotalTax = airPriceCheckResponse.TotalTax;

            return JsonSerialize(airPriceCheckResponse);
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("GetAirBookRequestData")]
        public HttpResponseMessage AirBookRequest(HttpRequestMessage request)
        {
            //var unit = new AirCRSDBContext();
            AirBookRequest airBookRequest = new AirBookRequest();
            AirBookResponse airBookResponse = new AirBookResponse();

            AirCRSBookingResponse airCrsBookingResponse = new AirCRSBookingResponse();
            string jsonContent = request.Content.ReadAsStringAsync().Result;
            AirBookRequestInput airBookDataFromRequest = JsonConvert.DeserializeObject<AirBookRequestInput>(jsonContent);

            //request validation
            if (string.IsNullOrEmpty(airBookDataFromRequest.AirPricingSolution_Key) || airBookDataFromRequest.PassengerInformations.Count < 0)
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Your AirPricingSolution_Key Is Empty Or Passenger Informations Did not match with Search Data !!");

            // AirPricingSolution_Key validation

            var decriptCode = DecryptString(airBookDataFromRequest.AirPricingSolution_Key, EncryptionKey).Split('|');

            var apiRequestlogData = _airSearchService.GetApiRequestLog(Convert.ToInt32(decriptCode[0]));
            var priceBreakDownId = Convert.ToInt32(decriptCode[1]);

            if (apiRequestlogData == null)
            {
                return Request.CreateErrorResponse(HttpStatusCode.OK, "Your AirPricingSolution_Key Expair !!");
            }
            else
            {
                try
                {
                    airCrsBookingResponse = _airSearchService.ApiBookingRequest(apiRequestlogData, airBookDataFromRequest, priceBreakDownId, User.Identity.GetUserId());
                }
                catch (Exception ex)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.OK, ex.Message);
                }
            }
            return JsonSerialize(airCrsBookingResponse);
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("GetTripExtraServices")]
        public HttpResponseMessage GetTripExtraServices(HttpRequestMessage request)
        {
            AirCRSExtraServiceResponse airCrsExtraServiceResponse = new AirCRSExtraServiceResponse();

            string jsonContent = request.Content.ReadAsStringAsync().Result;
            AirExtraServicesRequestInput airExtraServiceRequest = JsonConvert.DeserializeObject<AirExtraServicesRequestInput>(jsonContent);

            //request validation
            if (string.IsNullOrEmpty(airExtraServiceRequest.AirPricingSolution_Key))
                return Request.CreateErrorResponse(HttpStatusCode.OK, "Your AirPricingSolution_Key Expaired Or Not Correct !!");

            // AirPricingSolution_Key validation

            var decriptCode = DecryptString(airExtraServiceRequest.AirPricingSolution_Key, EncryptionKey).Split('|');

            var getRequestLogDataByLogId = _airSearchService.GetApiRequestLog(Convert.ToInt32(decriptCode[0]));
            var priceBreakDownId = Convert.ToInt32(decriptCode[1]);

            if (getRequestLogDataByLogId == null)
            {
                return Request.CreateErrorResponse(HttpStatusCode.OK, "Your AirPricingSolution_Key Expaired Or Not Correct !!");
            }
            else
            {
                airCrsExtraServiceResponse = _airSearchService.GetAirExtraService(getRequestLogDataByLogId, User.Identity.GetUserId());
            }

            return JsonSerialize(airCrsExtraServiceResponse);
        }


        //[System.Web.Http.HttpPost]
        //[System.Web.Http.Route("GetAirTicketRequestData")]
        //public HttpResponseMessage AirTicketRequest(HttpRequestMessage request)
        //{
        //    var unit = new AirCRSDBContext();

        //    AirTicketRequest airTicketRequest = new AirTicketRequest();
        //    AirTicketResponse airTicketResponse = new AirTicketResponse();

        //    string jsonContent = request.Content.ReadAsStringAsync().Result;
        //    AirTicketRequestInput airTicketDataByApiRequest = JsonConvert.DeserializeObject<AirTicketRequestInput>(jsonContent);

        //    //request validation
        //    if (string.IsNullOrEmpty(airTicketDataByApiRequest.AirPricingSolution_Key) || string.IsNullOrEmpty(airTicketDataByApiRequest.ReservationNumber) || airTicketDataByApiRequest.ConfirmationNumbers.Count < 0)
        //        return new HttpResponseMessage(HttpStatusCode.BadRequest);

        //    //AirPricingSolution_Key validation
        //    var requestLogId = Convert.ToInt32(DecryptString(airTicketDataByApiRequest.AirPricingSolution_Key, EncryptionKey));
        //    var apiRequestlogData = unit.ApiRequestLogs.Where(x => x.ApiRequestLogId == requestLogId && (x.Status == (int)ApiRequestLogStatus.Booking || x.Status == (int)ApiRequestLogStatus.Success)).FirstOrDefault();

        //    if (apiRequestlogData == null)
        //    {
        //        return new HttpResponseMessage(HttpStatusCode.BadRequest);
        //    }
        //    else
        //    {
        //        List<string> confirmationNumbers = new List<string>();
        //        List<TicketInfo> ticketInfos = new List<TicketInfo>();

        //        List<int> ticketIdsForRollback = new List<int>();
        //        List<int> pnrLogIdsForRollback = new List<int>();

        //        var tripData = unit.Trips.Where(x => x.TripId == apiRequestlogData.TripId).FirstOrDefault();
        //        var numberOfTicketsRequire = apiRequestlogData.NumberOfAdult + apiRequestlogData.NumberOfChild + apiRequestlogData.NumberOfInfrant;

        //        //if (numberOfTicketsRequire > tripData.NoOfTickets)
        //        //{
        //        //    //update Api Request log 
        //        //    apiRequestlogData.Status = (int)ApiRequestLogStatus.TicketingCancel;
        //        //    unit.ApiRequestLogRepository.Update(apiRequestlogData);
        //        //    unit.Save();
        //        //    return new HttpResponseMessage(HttpStatusCode.BadRequest);
        //        //}
        //        //else
        //        {
        //            foreach (var item in airTicketDataByApiRequest.ConfirmationNumbers)
        //            {
        //                var decryptedPnrLogId = Convert.ToInt32(DecryptString(item, EncryptionKey));

        //                var pnrLogData = unit.PnrLogs.Where(x => x.PnrLogId == decryptedPnrLogId).FirstOrDefault();

        //                if (pnrLogData == null)
        //                {
        //                    //rollback to the previous state
        //                    if (ticketIdsForRollback.Count > 0)
        //                    {
        //                        var tickets = unit.Tickets.Where(x => ticketIdsForRollback.Contains(x.TicketId)).ToList();

        //                        foreach (var tItem in tickets)
        //                        {
        //                            tItem.TStatus = (int)TicketStatus.Free;
        //                            unit.Entry(tItem).State = EntityState.Modified;
        //                            unit.SaveChanges();
        //                        }
        //                    }

        //                    //rollback to the previous state
        //                    if (pnrLogIdsForRollback.Count > 0)
        //                    {
        //                        var pnrLogs = unit.PnrLogs.Where(x => pnrLogIdsForRollback.Contains(x.PnrLogId)).ToList();
        //                        foreach (var pnrLogItem in pnrLogs)
        //                        {
        //                            // pnrLogItem.UsesFlag = (int)PnrUsesType.Hold;
        //                            pnrLogItem.UsesFlag = (int)PnrUsesType.Booked;
        //                            pnrLogItem.TicketNumber = "";
        //                            //unit.PnrLogRepository.Update(pnrLogItem);

        //                            unit.Entry(pnrLogItem).State = EntityState.Modified;
        //                            unit.SaveChanges();
        //                        }
        //                    }

        //                    //update Api Request log 
        //                    apiRequestlogData.Status = (int)ApiRequestLogStatus.TicketingCancel;
        //                    //unit.ApiRequestLogRepository.Update(apiRequestlogData);

        //                    unit.Entry(apiRequestlogData).State = EntityState.Modified;
        //                    unit.SaveChanges();

        //                    return new HttpResponseMessage(HttpStatusCode.BadRequest);
        //                }

        //                var passenger = unit.Passengers.Where(x => x.PassengerId == pnrLogData.PassengerId).FirstOrDefault();
        //                var pnr = unit.Pnrs.Where(x => x.PnrId == pnrLogData.PnrId).FirstOrDefault();

        //                List<string> ticketNumber = new List<string>();
        //                var oldTicket = unit.Tickets.Where(x => x.TripId == pnr.TripId && x.ConfirmationNumber == item).FirstOrDefault();
        //                if (oldTicket != null)
        //                {
        //                    ticketNumber = new List<string>()
        //                    {
        //                        oldTicket.TicketNumber,
        //                    };
        //                }
        //                else
        //                {
        //                    var freeTicket = unit.Tickets.Where(x => x.TripId == pnr.TripId && x.TStatus == (int)TicketStatus.Free).FirstOrDefault();
        //                    if (freeTicket == null)
        //                    {
        //                        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Ticket is processing, Please try again later !!");
        //                    }
        //                    ticketNumber = new List<string>()
        //                    {
        //                            freeTicket.TicketNumber,
        //                    };

        //                    //update Ticket Status
        //                    freeTicket.TStatus = (int)TicketStatus.Used;
        //                    freeTicket.ConfirmationNumber = item;
        //                    // unit.TicketRepository.Update(freeTicket);
        //                    unit.Entry(freeTicket).State = EntityState.Modified;
        //                    unit.SaveChanges();

        //                    ticketIdsForRollback.Add(freeTicket.TicketId);

        //                    //update PnrLog
        //                    //pnrLogData.UsesFlag = (int)PnrUsesType.Success;
        //                    pnrLogData.UsesFlag = (int)PnrUsesType.Ticketed;
        //                    pnrLogData.TicketNumber = freeTicket.TicketNumber;

        //                    unit.Entry(pnrLogData).State = EntityState.Modified;
        //                    unit.SaveChanges();

        //                    pnrLogIdsForRollback.Add(pnrLogData.PassengerId);
        //                }

        //                TicketInfo ticket = new TicketInfo()
        //                {
        //                    PassengerName = passenger.Name,
        //                    ReservationNumber = pnr.PnrValue,
        //                    TicketNumbers = ticketNumber
        //                };

        //                ticketInfos.Add(ticket);
        //            }
        //            airTicketResponse.TicketInfoes = ticketInfos;
        //            airTicketResponse.ConfirmationNumbers = airTicketDataByApiRequest.ConfirmationNumbers;
        //        }

        //    }

        //    //update request log
        //    //update Api Request log 
        //    apiRequestlogData.Status = (int)ApiRequestLogStatus.Success;

        //    unit.Entry(apiRequestlogData).State = EntityState.Modified;
        //    unit.SaveChanges();

        //    AirCRSBookingResponse airCrsBookingResponse = new AirCRSBookingResponse();
        //    airCrsBookingResponse.AirTicketResponse = airTicketResponse;
        //    airCrsBookingResponse.IsSuccess = true;
        //    airCrsBookingResponse.Message = "Ticket Retrieved Successfully";

        //    return JsonSerialize(airCrsBookingResponse);
        //}

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("GetSyncTicketData")]
        public HttpResponseMessage SyncTicketData(HttpRequestMessage request)
        {
            var unit = new AirCRSDBContext();

            AirModels.CRSTicketDataSyncResponse airTicketResponse = new AirModels.CRSTicketDataSyncResponse()
            {
                Errors = new List<AirModels.AirErrorResponse>(),
                TicketInfos = new List<TicketInfo>(),
            };

            string jsonContent = request.Content.ReadAsStringAsync().Result;
            AirModels.CRSTicketDataSyncRequest airTicketRequest = JsonConvert.DeserializeObject<AirModels.CRSTicketDataSyncRequest>(jsonContent);
            airTicketRequest.SegmentCode = DecryptString(airTicketRequest.SegmentCode);

            SegmentCodeBreakdown segmentCode = new SegmentCodeBreakdown().GetSegmentCodeBreakdown(airTicketRequest.SegmentCode);
            //request validation
            if (string.IsNullOrEmpty(segmentCode.AirPriceSolutionsKey) || string.IsNullOrEmpty(airTicketRequest.ReservationNumber) || airTicketRequest.Passengers.Count <= 0)
                return new HttpResponseMessage(HttpStatusCode.BadRequest);

            //AirPricingSolution_Key validation
           // var requestLogId = Convert.ToInt32(DecryptString(segmentCode.AirPriceSolutionsKey, EncryptionKey));

            var decriptCode = DecryptString(segmentCode.AirPriceSolutionsKey, EncryptionKey).Split('|');

            var getRequestLogDataByLogId = Convert.ToInt32(decriptCode[0]);

            var apiRequestlogData = unit.ApiRequestLogs.Where(x => x.ApiRequestLogId == getRequestLogDataByLogId && (x.Status == (int)ApiRequestLogStatus.Booking || x.Status == (int)ApiRequestLogStatus.Success)).FirstOrDefault();

            if (apiRequestlogData == null)
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
            else
            {

                Pnr pnrObj = unit.Pnrs.Where(x => x.PnrValue == airTicketRequest.ReservationNumber).FirstOrDefault();
                if (pnrObj == null)
                {
                    AirModels.AirErrorResponse error = new AirModels.AirErrorResponse()
                    {
                        ErrorCode = "301",
                        ErrorMessage = "PNR is invalid!",
                    };
                    return JsonSerialize(error);
                }
                else
                {
                    foreach (var paxItem in airTicketRequest.Passengers)
                    {


                        DateTime dob = Convert.ToDateTime(paxItem.DOB);
                        Passenger pax = unit.Passengers.Where(x => x.FirstName == paxItem.FirstName && x.LastName == paxItem.LastName && (x.PassportNumber == paxItem.PassengerPassport || (x.DOB.HasValue && x.DOB == dob))).FirstOrDefault();
                        if (pax == null)
                        {
                            airTicketResponse.Errors.Add(new AirModels.AirErrorResponse()
                            {
                                ErrorCode = "302",
                                ErrorMessage = "Passenger data not matched!",
                                Referance = paxItem.FirstName + " " + paxItem.LastName + " PNR :" + pnrObj.PnrValue + " DOB : " + paxItem.DOB
                            }); ;
                        }
                        else
                        {
                            PnrLog pnrLog = unit.PnrLogs.Where(x => x.PnrId == pnrObj.PnrId && x.PassengerId == pax.PassengerId).FirstOrDefault();
                            if (pnrLog == null)
                            {
                                airTicketResponse.Errors.Add(new AirModels.AirErrorResponse()
                                {
                                    ErrorCode = "303",
                                    ErrorMessage = "Booking record not found!",
                                    Referance = $"{pax.FirstName} {pax.LastName}" + " PNR :" + pnrObj.PnrValue + " DOB : " + paxItem.DOB
                                }); ;
                            }
                            else
                            {
                                airTicketResponse.TicketInfos.Add(new TicketInfo()
                                {
                                    PassengerName = $"{pax.Title} {pax.FirstName} {pax.LastName}",
                                    ReservationNumber = pnrObj.PnrValue,
                                    TicketNumbers = new List<string>() { pnrLog.TicketNumber }
                                });

                            }

                        }
                    }
                }
                airTicketResponse.ReservationNumber = airTicketRequest.ReservationNumber;
                airTicketResponse.SegmentCode = airTicketRequest.SegmentCode;
                airTicketResponse.AirItineraryCode = airTicketRequest.AirItineraryCode;
                airTicketResponse.IGXKey = airTicketRequest.IGXKey;
            }

            return JsonSerialize(airTicketResponse);
        }


        public class SegmentCodeBreakdown
        {
            public string JsonFileName { get; set; }
            public string AirPriceSolutionsKey { get; set; }
            public string ApiRef { get; set; }
            public SegmentCodeBreakdown GetSegmentCodeBreakdown(string SegmentCode)
            {
                SegmentCodeBreakdown segmentCode = new SegmentCodeBreakdown();
                if (!string.IsNullOrEmpty(SegmentCode))
                {
                    string[] segmentBreakdown = SegmentCode.Split('|');
                    if (!string.IsNullOrEmpty(segmentBreakdown[0]))
                        segmentCode.JsonFileName = segmentBreakdown[0];
                    if (!string.IsNullOrEmpty(segmentBreakdown[1]))
                        segmentCode.AirPriceSolutionsKey = segmentBreakdown[1];
                    if (!string.IsNullOrEmpty(segmentBreakdown[2]))
                        segmentCode.ApiRef = segmentBreakdown[2];
                }

                return segmentCode;
            }
        }

        #region Encrypt Or Decrypt

        private string EncryptString(string Message, string Passphrase)
        {
            byte[] Results;
            System.Text.UTF8Encoding UTF8 = new System.Text.UTF8Encoding();
            MD5CryptoServiceProvider HashProvider = new MD5CryptoServiceProvider();
            byte[] TDESKey = HashProvider.ComputeHash(UTF8.GetBytes(Passphrase));
            TripleDESCryptoServiceProvider TDESAlgorithm = new TripleDESCryptoServiceProvider();
            TDESAlgorithm.Key = TDESKey;
            TDESAlgorithm.Mode = CipherMode.ECB;
            TDESAlgorithm.Padding = PaddingMode.PKCS7;
            byte[] DataToEncrypt = UTF8.GetBytes(Message);
            try
            {
                ICryptoTransform Encryptor = TDESAlgorithm.CreateEncryptor();
                Results = Encryptor.TransformFinalBlock(DataToEncrypt, 0, DataToEncrypt.Length);
            }
            finally
            {
                TDESAlgorithm.Clear();
                HashProvider.Clear();
            }
            return Convert.ToBase64String(Results);
        }

        public static string DecryptString(string encrString)
        {
            byte[] b;
            string decrypted;
            try
            {
                b = Convert.FromBase64String(encrString);
                decrypted = System.Text.ASCIIEncoding.ASCII.GetString(b);
            }
            catch (FormatException fe)
            {
                decrypted = "";
            }
            return decrypted;
        }

        private string DecryptString(string Message, string Passphrase)
        {
            byte[] Results;
            System.Text.UTF8Encoding UTF8 = new System.Text.UTF8Encoding();
            MD5CryptoServiceProvider HashProvider = new MD5CryptoServiceProvider();
            byte[] TDESKey = HashProvider.ComputeHash(UTF8.GetBytes(Passphrase));
            TripleDESCryptoServiceProvider TDESAlgorithm = new TripleDESCryptoServiceProvider();
            TDESAlgorithm.Key = TDESKey;
            TDESAlgorithm.Mode = CipherMode.ECB;
            TDESAlgorithm.Padding = PaddingMode.PKCS7;
            byte[] DataToDecrypt = Convert.FromBase64String(Message);
            try
            {
                ICryptoTransform Decryptor = TDESAlgorithm.CreateDecryptor();
                Results = Decryptor.TransformFinalBlock(DataToDecrypt, 0, DataToDecrypt.Length);
            }
            finally
            {
                TDESAlgorithm.Clear();
                HashProvider.Clear();
            }
            return UTF8.GetString(Results);
        }

        private HttpResponseMessage JsonSerialize<T>(T obj)
        {
            string json = JsonConvert.SerializeObject(obj);

            var resp = new HttpResponseMessage()
            {
                Content = new StringContent(json)
            };
            resp.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            return resp;
        }

        #endregion



    }

}
