[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(AirCRS_API.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(AirCRS_API.App_Start.NinjectWebCommon), "Stop")]

namespace AirCRS_API.App_Start
{
    using System;
    using System.Web;
    using AirCRS_API.DAL.UnitOfWorks;
    using AirCRS_API.SERVICE.AirPriceServices;
    using AirCRS_API.SERVICE.AirSearchApiServices;
    using AirCRS_API.SERVICE.AspNetUserServices;
    using AirCRS_API.SERVICE.BookingServices;
    using AirCRS_API.SERVICE.CrsUserServices;
    using AirCRS_API.SERVICE.SegmentServices;
    using AirCRS_API.SERVICE.TripServices;
    using BackOffice_API.Services.UserPermissions;
    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    using Ninject;
    using Ninject.Web.Common;

    public static class NinjectWebCommon 
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start() 
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }
        
        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }

        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        //private static IKernel CreateKernel()
        //{
        //    var kernel = new StandardKernel();
        //    try
        //    {
        //        kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
        //        kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

        //        RegisterServices(kernel);
        //        return kernel;
        //    }
        //    catch
        //    {
        //        kernel.Dispose();
        //        throw;
        //    }
        //}

        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
            kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();
            System.Web.Http.GlobalConfiguration.Configuration.DependencyResolver = new Ninject.WebApi.DependencyResolver.NinjectDependencyResolver(kernel);
            RegisterServices(kernel);
            return kernel;
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind<IUoW>().To<UoW>();
            kernel.Bind<ITripService>().To<TripService>();
            kernel.Bind<ISegmentService>().To<SegmentService>();
            kernel.Bind<IUserPermissionService>().To<UserPermissionService>();
            kernel.Bind<ICrsUserServic>().To<CrsUserServic>();
            kernel.Bind<IAirPriceService>().To<AirPriceService>();
            kernel.Bind<IBookingService>().To<BookingService>();
            kernel.Bind<IAirSearchApiService>().To<AirSearchApiService>();
            kernel.Bind<SERVICE.ReportServices.IReportServices>().To<SERVICE.ReportServices.ReportServices>();
        }        
    }
}
