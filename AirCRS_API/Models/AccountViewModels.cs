﻿using System;
using System.Collections.Generic;

namespace AirCRS_API.Models
{
    // Models returned by AccountController actions.

    public class ExternalLoginViewModel
    {
        public string Name { get; set; }

        public string Url { get; set; }

        public string State { get; set; }
    }

    public class ManageInfoViewModel
    {
        public string LocalLoginProvider { get; set; }

        public string Email { get; set; }

        public IEnumerable<UserLoginInfoViewModel> Logins { get; set; }

        public IEnumerable<ExternalLoginViewModel> ExternalLoginProviders { get; set; }
    }

    public class UserInfoViewModel
    {
        public string Email { get; set; }

        public bool HasRegistered { get; set; }

        public string LoginProvider { get; set; }
    }

    public class UserLoginInfoViewModel
    {
        public string LoginProvider { get; set; }

        public string ProviderKey { get; set; }
    }

    public class UserLoginModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
    public class UserLoginResponseModel
    {
        public string access_token { get; set; }
        public string token_type { get; set; }
        public string expires_in { get; set; }
        public string userName { get; set; }
        //public decimal balance { get; set; }
        //public string currency { get; set; }
        public decimal valuePerBDT { get; set; }
        public string AgentName { get; set; }
        public string user_type { get; set; }
    }
    //public class ExternalLoginViewModel
    //{
    //    public string Name { get; set; }

    //    public string Url { get; set; }

    //    public string State { get; set; }
    //}

    //public class ManageInfoViewModel
    //{
    //    public string LocalLoginProvider { get; set; }

    //    public string Email { get; set; }

    //    public IEnumerable<UserLoginInfoViewModel> Logins { get; set; }

    //    public IEnumerable<ExternalLoginViewModel> ExternalLoginProviders { get; set; }
    //}

    //public class UserInfoViewModel
    //{
    //    public string Email { get; set; }

    //    public bool HasRegistered { get; set; }

    //    public string LoginProvider { get; set; }
    //}

    //public class UserLoginInfoViewModel
    //{
    //    public string LoginProvider { get; set; }

    //    public string ProviderKey { get; set; }
    //}
}
