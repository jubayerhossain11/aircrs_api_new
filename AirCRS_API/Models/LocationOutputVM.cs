﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AirCRS_API.Models
{
    public class LocationOutputVM
    {
        public string Label { get; set; }
        public string Value { get; set; }
    }
}