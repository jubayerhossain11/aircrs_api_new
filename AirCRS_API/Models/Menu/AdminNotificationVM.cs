﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AirCRS_API.Models.Menu
{
    public class AdminNotificationVM
    {
        public int FlightBooking { get; set; }
        public int? BookingFaieldFlight { get; set; }
        public int? BookingConfirmedFlight { get; set; }
        public int? ConfirmedFlightTicketing { get; set; }
        public int? ETicketProcessed { get; set; }
        public int? OrderETicketProcess { get; set; }
        public int? PriceChecked { get; set; }

        public int? TicketingFaieldFlight { get; set; }
        public int? CanceledFLight { get; set; }
        public int? AgentETicketRequest { get; set; }
        public int? FlightInProcess { get; set; }
               
        public int? HotelBooking { get; set; }
        public int? HotelBookingConfirmed { get; set; }
        public int? HotelBookingFaield { get; set; }
        public int? HotelBookingCanceled { get; set; }
        public int? AgentEbookingRequest { get; set; }
        public int? HotelBookingInProcess { get; set; }
        public int? HotelBookingCanceledFailed { get; set; }
               
        public int? AgentRegistrationRequest { get; set; }
        public int? AgentDepositRequest { get; set; }
        public int? AgentSupportTocken { get; set; }
        public int? BlockedAgent { get; set; }

        public int? ETicketRequest { get; set; }
        public int? ProcessedETicketRequest { get; set; }
        public int? EBookingRequest { get; set; }
        public int? UnsuccessfullRequest { get; set; }
        public int? UnsuccessfulResponse { get; set; }



    }

    public class SearchCalendarSearch
    {
        public string StartDate { get; set; }
        public string EndDate { get; set; }
    }

    public class FareCalendarResult
    {
        public string BookingNumber { get; set; }
        public string Link { get; set; }
        public string Date { get; set; }
    }

    public class MonthBookingModel
    {
        public int Hotel { get; set; }
        public int Air { get; set; }
        public int HotelMonth { get; set; }
        public int AirMonth { get; set; }
    }

    public class NotificationBarVM
    {
        public int SupportTicketCount { get; set; }
        public int RegisteredAgentCount { get; set; }
        public int ETicketCount { get; set; }
        public int DepositRequestCount { get; set; }
    }

    public class SupportTicket
    {
        public int AgentID { get; set; }
        public string AgentName { get; set; }
        public string ETicketNo { get; set; }


    }

    public class RegisteredAgent
    {
        public int AgentID { get; set; }
        public string AgentName { get; set; }
        public string RegistrationDate { get; set; }
    }

    public class ETicket
    {
        public int AgentID { get; set; }
        public string AgentName { get; set; }
        public int ETicketReqID { get; set; }
        public string IGXRefNo { get; set; }
        public string RequestDate { get; set; }
    }
    public class DepositRequest
    {
        public int RequestID { get; set; }
        public int AgentID { get; set; }
        public string AgentName { get; set; }
        public decimal RequestedAmount { get; set; }
        public string CurrencyCode { get; set; }
        public string RequestDate { get; set; }
    }
}