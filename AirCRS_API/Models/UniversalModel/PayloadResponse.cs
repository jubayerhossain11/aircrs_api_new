﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AirCRS_API.Models.UniversalModel
{
    public class PayloadResponse
    {
        public PayloadResponse()
        {
            this.RequestTime = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
        }
        public string RequestTime { get; set; }
        public string ResponseTime { get; set; }
        public string RequestURL { get; set; }
        public bool Success { get; set; }
        public string Message { get; set; }
        public dynamic Payload { get; set; }
        public string PayloadType { get; set; }
    }

    public class Token
    {
        public string access_token { get; set; }
        public string token_type { get; set; }
        public string expires_in { get; set; }
        public string issued { get; set; }
        public string expires { get; set; }
    }
}