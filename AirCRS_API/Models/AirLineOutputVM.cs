﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AirCRS_API.Models
{
    public class AirLineOutputVM
    {
        public string Label { get; set; }
        public int Value { get; set; }
    }
}