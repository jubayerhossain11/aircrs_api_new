﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AirCRS_API.Helper
{
    static public class GenerateOTP
    {
        private static Random random = new Random();
        public static string GetOTP(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

    }

    static public class GenerateREFKEY
    {
        private static Random random = new Random();
        public static string GetREFKEY(int length)
        {
            const string chars = "0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

    }
}