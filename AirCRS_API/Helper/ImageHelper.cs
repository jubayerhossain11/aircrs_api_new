﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace AirCRS_API.Helper
{
    abstract public class ImageHelper
    {
        public static string Base64ToImage(string ImageName, string ImageFIle, string FilePath)
        {
            try
            {
                #region Save Image
                string Filename = ImageFIle.Replace(" ", "_");
                Filename = "[" + Filename + "]" + ImageName;

                byte[] imageBytes = Convert.FromBase64String(ImageFIle);
                MemoryStream ms = new MemoryStream(imageBytes, 0, imageBytes.Length);
                ms.Write(imageBytes, 0, imageBytes.Length);
                System.Drawing.Image image = System.Drawing.Image.FromStream(ms, true);




                string path = System.Web.HttpContext.Current.Server.MapPath("~/" + FilePath);
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                var fileSavePath = Path.Combine(path, ImageName);

                if (!string.IsNullOrEmpty(fileSavePath))
                {
                    if (File.Exists(fileSavePath))
                    {
                        // deletevprevious image
                        File.Delete(fileSavePath);
                    }
                }

                bool checkFileSave = false;
                try
                {
                    image.Save(fileSavePath);
                    checkFileSave = true;
                }
                catch (Exception ex)
                {
                    checkFileSave = false;
                }
                ImageName = Filename;
                //packageInsertVM.PackageBannerImageFile = "/App_Data/Package/" + packageInsertVM.PackageBannerImageName;
                string SaveFilePath = "/Package/" + Filename;
                #endregion



                return "";
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public static string Base64ToImageGlobal(string ImageName, string ImageFIle, string FilePath)
        {
            try
            {
                #region Save Image
                string Filename = ImageFIle.Replace(" ", "_");
                Filename = "[" + Filename + "]" + ImageName;

                byte[] imageBytes = Convert.FromBase64String(ImageFIle);
                MemoryStream ms = new MemoryStream(imageBytes, 0, imageBytes.Length);
                ms.Write(imageBytes, 0, imageBytes.Length);
                System.Drawing.Image image = System.Drawing.Image.FromStream(ms, true);




                string path = System.Web.HttpContext.Current.Server.MapPath("~/" + FilePath);
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                var fileSavePath = Path.Combine(path, ImageName);

                if (!string.IsNullOrEmpty(fileSavePath))
                {
                    if (File.Exists(fileSavePath))
                    {
                        // deletevprevious image
                        File.Delete(fileSavePath);
                    }
                }

                bool checkFileSave = false;
                try
                {
                    image.Save(fileSavePath);
                    checkFileSave = true;
                }
                catch (Exception ex)
                {
                    checkFileSave = false;
                }
                //packageInsertVM.PackageBannerImageFile = "/App_Data/Package/" + packageInsertVM.PackageBannerImageName;
                string SaveFilePath = FilePath +"/"+ ImageName;
                #endregion



                return SaveFilePath;
            }
            catch (Exception ex)
            {
                return "";
            }
        }
    }
}