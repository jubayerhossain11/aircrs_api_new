﻿using System;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace AirCRS_API.Helper
{
    [Authorize]
    public class AccountHelper : IDisposable
    {

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

    }
}