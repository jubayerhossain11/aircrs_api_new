﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;

namespace AirCRS_API.Helper
{
    public class SmsHelper
    {
        public bool SendSms(string contacts, string body)
        {
            try
            {
                ////Zaman it Sms Gateway End

                //string sms_url = System.Configuration.ConfigurationManager.AppSettings["Sms_Url"];
                //string api_key = System.Configuration.ConfigurationManager.AppSettings["Sms_Api_Key"];
                //string senderId = System.Configuration.ConfigurationManager.AppSettings["Sms_Senderid"];

                //var client = new HttpClient { BaseAddress = new Uri(sms_url) };
                //var uri = new Uri(sms_url + "?api_key=" + api_key + "&type=text" + "&contacts=" + contacts + "&senderid=" + senderId + "&msg=" + body);

                ////End

                //MobiReach it Sms Gateway End

                string sms_url = System.Configuration.ConfigurationManager.AppSettings["Sms_Url"];
                string api_userName = System.Configuration.ConfigurationManager.AppSettings["Sms_User_Name"];
                string api_password = System.Configuration.ConfigurationManager.AppSettings["Sms_Password"];
                string sms_from = System.Configuration.ConfigurationManager.AppSettings["Sms_From"];

                body = System.Web.HttpUtility.UrlEncode(body);

                var client = new HttpClient { BaseAddress = new Uri(sms_url) };
                var uri = new Uri(sms_url + "?Username=" + api_userName + "&Password=" + api_password + "&From=" + sms_from + "&To=" + contacts + "&Message=" + body);
                
                //End
                
                HttpResponseMessage result = client.GetAsync(uri.ToString()).Result;

                var jsonString = result.Content.ReadAsStringAsync();
                jsonString.Wait();
                try
                {
                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }

            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}