﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;

namespace AirCRS_API.Helper.FIle_Convertion
{
    public class FileConverter: IDisposable
    {
        public Image Base64ToImage(string Bas64String)
        {
            byte[] bytes = Convert.FromBase64String(Bas64String);

            Image image;
            using (MemoryStream ms = new MemoryStream(bytes))
            {
                image = Image.FromStream(ms);
            }

            return image;

        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}