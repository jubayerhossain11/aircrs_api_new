﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Net.Mail;
//using System.Net;
//using FlamingoWebApi.Models.DAL;

//namespace FlamingoWebApi.Helper
//{
//    static public class EmailHelper
//    {

//        public static bool UserRegEmail(int RequestedAgentID,string otp)
//        {
//            try
//            {
//                using (var unit = new Models.DAL.UnitOfWork())
//                {
//                    var email = unit.EmailRepository.Get(a => a.EmailCode == "REG101").FirstOrDefault();
//                    var apiuser = unit.AgentRegistrationRequestRepository.GetByID(RequestedAgentID);

//                    var receiverList = unit.EmailEmailReceiverMapperRepository.Get(a => a.RefEmailID == email.EmailID).ToList();
//                    List<string> receiverID = new List<string>();
//                    receiverID.Add(apiuser.Email);
//                    foreach (var item in receiverList)
//                    {
//                        receiverID.Add(unit.EmailReceiverRepository.Get(a => a.ReceiverID == item.RefReceiverID).FirstOrDefault().EmailID);
//                    }

//                    string mailBody = email.EmailBody.Replace("{{UserName}}", apiuser.Name);
//                    mailBody = mailBody.Replace("{{otp}}", otp);

//                    if (SendEmailAsync(email.SenderID, email.EmailID, receiverID, email.EmailSubject, mailBody))
//                    {
//                        return true;
//                    }
//                    else
//                    {
//                        return false;
//                    }
//                }
//            }
//            catch (Exception ex)
//            {
//                return false;
//            }
//        }

//        //Error
//        public static bool UserRegConfirmationEmail(string UserEmail,string UserName ,string LoginID, string Password)
//        {
//            try
//            {
//                using (var unit = new Models.DAL.UnitOfWork())
//                {
//                    var email = unit.EmailRepository.Get(a => a.EmailCode == "REG102").FirstOrDefault();
//                    //var apiuser = unit.ApiUserRepository.GetByID(ApiUserID);

//                    var receiverList = unit.EmailEmailReceiverMapperRepository.Get(a => a.RefEmailID == email.EmailID).ToList();
//                    List<string> receiverID = new List<string>();
//                    receiverID.Add(UserEmail);
//                    foreach (var item in receiverList)
//                    {
//                        receiverID.Add(unit.EmailReceiverRepository.Get(a => a.ReceiverID == item.RefReceiverID).FirstOrDefault().EmailID);
//                    }

//                    string mailBody = email.EmailBody.Replace("{{UserName}}", UserName);
//                    mailBody = mailBody.Replace("{{LoginID}}", LoginID);
//                    mailBody = mailBody.Replace("{{Password}}", Password);
//                    if (SendEmailAsync(email.SenderID, email.EmailID, receiverID, email.EmailSubject, mailBody))
//                    {
//                        return true;
//                    }
//                    else
//                    {
//                        return false;
//                    }
//                }
//            }
//            catch (Exception ex)
//            {
//                return false;
//            }
//        }

//        public static bool DepositRequest(int ApiUserID, decimal DepositAmount, string DepositDate)
//        {
//            try
//            {
//                using (var unit = new Models.DAL.UnitOfWork())
//                {
//                    var email = unit.EmailRepository.Get(a => a.EmailCode == "DEPOSIT103").FirstOrDefault();
//                    var apiuser = unit.ApiUserRepository.GetByID(ApiUserID);

//                    var receiverList = unit.EmailEmailReceiverMapperRepository.Get(a => a.RefEmailID == email.EmailID).ToList();
//                    List<string> receiverID = new List<string>();
//                    receiverID.Add(apiuser.UserEmail);
//                    foreach (var item in receiverList)
//                    {
//                        receiverID.Add(unit.EmailReceiverRepository.Get(a => a.ReceiverID == item.RefReceiverID).FirstOrDefault().EmailID);
//                    }

//                    string mailBody = email.EmailBody.Replace("{{UserName}}", apiuser.UserName);
//                    mailBody = mailBody.Replace("{{amount}}", DepositAmount.ToString());
//                    mailBody = mailBody.Replace("{{UserCurrencyCode}}", unit.CrossCurrencyRepository.Get(a=>a.CurrencyID==apiuser.ApiUserBaseCurrency).FirstOrDefault().CurrencyCode ?? "");
//                    mailBody = mailBody.Replace("{{DepositDate}}", DepositDate);
//                    if (SendEmailAsync(email.SenderID, email.EmailID, receiverID, email.EmailSubject, mailBody))
//                    {
//                        return true;
//                    }
//                    else
//                    {
//                        return false;
//                    }
//                }
//            }
//            catch (Exception ex)
//            {
//                return false;
//            }
//        }

//        public static bool DepositApprove(int ApiUserID, decimal DepositAmount, string DepositDate)
//        {
//            try
//            {
//                using (var unit = new Models.DAL.UnitOfWork())
//                {
//                    var email = unit.EmailRepository.Get(a => a.EmailCode == "DEPOSIT101").FirstOrDefault();
//                    var apiuser = unit.ApiUserRepository.GetByID(ApiUserID);

//                    var receiverList = unit.EmailEmailReceiverMapperRepository.Get(a => a.RefEmailID == email.EmailID).ToList();
//                    List<string> receiverID = new List<string>();
//                    receiverID.Add(apiuser.UserEmail);
//                    foreach (var item in receiverList)
//                    {
//                        receiverID.Add(unit.EmailReceiverRepository.Get(a => a.ReceiverID == item.RefReceiverID).FirstOrDefault().EmailID);
//                    }

//                    string mailBody = email.EmailBody.Replace("{{UserName}}", apiuser.UserName);
//                    mailBody = mailBody.Replace("{{amount}}", DepositAmount.ToString());
//                    mailBody = mailBody.Replace("{{UserCurrencyCode}}", unit.CrossCurrencyRepository.Get(a => a.CurrencyID == apiuser.ApiUserBaseCurrency).FirstOrDefault().CurrencyCode ?? "");
//                    mailBody = mailBody.Replace("{{DepositDate}}", DepositDate);
//                    if (SendEmailAsync(email.SenderID, email.EmailID, receiverID, email.EmailSubject, mailBody))
//                    {
//                        return true;
//                    }
//                    else
//                    {
//                        return false;
//                    }
//                }
//            }
//            catch (Exception ex)
//            {
//                return false;
//            }
//        }

//        public static bool DepositReject(int ApiUserID, decimal DepositAmount, string DepositDate, string RejectPurpose)
//        {
//            try
//            {
//                using (var unit = new Models.DAL.UnitOfWork())
//                {
//                    var email = unit.EmailRepository.Get(a => a.EmailCode == "DEPOSIT102").FirstOrDefault();
//                    var apiuser = unit.ApiUserRepository.GetByID(ApiUserID);

//                    var receiverList = unit.EmailEmailReceiverMapperRepository.Get(a => a.RefEmailID == email.EmailID).ToList();
//                    List<string> receiverID = new List<string>();
//                    receiverID.Add(apiuser.UserEmail);
//                    foreach (var item in receiverList)
//                    {
//                        receiverID.Add(unit.EmailReceiverRepository.Get(a => a.ReceiverID == item.RefReceiverID).FirstOrDefault().EmailID);
//                    }

//                    string mailBody = email.EmailBody.Replace("{{UserName}}", apiuser.UserName);
//                    mailBody = mailBody.Replace("{{amount}}", DepositAmount.ToString());
//                    mailBody = mailBody.Replace("{{UserCurrencyCode}}", unit.CrossCurrencyRepository.Get(a => a.CurrencyID == apiuser.ApiUserBaseCurrency).FirstOrDefault().CurrencyCode ?? "");
//                    mailBody = mailBody.Replace("{{DepositDate}}", DepositDate);
//                    mailBody = mailBody.Replace("{{RejectPurpose}}", RejectPurpose);
//                    if (SendEmailAsync(email.SenderID, email.EmailID, receiverID, email.EmailSubject, mailBody))
//                    {
//                        return true;
//                    }
//                    else
//                    {
//                        return false;
//                    }
//                }
//            }
//            catch (Exception ex)
//            {
//                return false;
//            }
//        }


//        //tested upto here


//        public static bool UserBlock(int ApiUserID,string Purpose)
//        {
//            try
//            {
//                using (var unit = new Models.DAL.UnitOfWork())
//                {
//                    var email = unit.EmailRepository.Get(a => a.EmailCode == "REG102").FirstOrDefault();
//                    var apiuser = unit.ApiUserRepository.GetByID(ApiUserID);

//                    var receiverList = unit.EmailEmailReceiverMapperRepository.Get(a => a.RefEmailID == email.EmailID).ToList();
//                    List<string> receiverID = new List<string>();
//                    receiverID.Add(apiuser.UserEmail);
//                    foreach (var item in receiverList)
//                    {
//                        receiverID.Add(unit.EmailReceiverRepository.Get(a => a.ReceiverID == item.RefReceiverID).FirstOrDefault().EmailID);
//                    }

//                    string mailBody = email.EmailBody.Replace("{{UserName}}", apiuser.UserName);
//                    mailBody = email.EmailBody.Replace("{{amount}}", Purpose);
//                    if (SendEmailAsync(email.SenderID, email.EmailID, receiverID, email.EmailSubject, mailBody))
//                    {
//                        return true;
//                    }
//                    else
//                    {
//                        return false;
//                    }
//                }
//            }
//            catch (Exception ex)
//            {
//                return false;
//            }
//        }

//        public static bool UserUnblock(int ApiUserID, string Purpose)
//        {
//            try
//            {
//                using (var unit = new Models.DAL.UnitOfWork())
//                {
//                    var email = unit.EmailRepository.Get(a => a.EmailCode == "REG102").FirstOrDefault();
//                    var apiuser = unit.ApiUserRepository.GetByID(ApiUserID);

//                    var receiverList = unit.EmailEmailReceiverMapperRepository.Get(a => a.RefEmailID == email.EmailID).ToList();
//                    List<string> receiverID = new List<string>();
//                    receiverID.Add(apiuser.UserEmail);
//                    foreach (var item in receiverList)
//                    {
//                        receiverID.Add(unit.EmailReceiverRepository.Get(a => a.ReceiverID == item.RefReceiverID).FirstOrDefault().EmailID);
//                    }

//                    string mailBody = email.EmailBody.Replace("{{UserName}}", apiuser.UserName);
//                    mailBody = email.EmailBody.Replace("{{amount}}", Purpose);
//                    if (SendEmailAsync(email.SenderID, email.EmailID, receiverID, email.EmailSubject, mailBody))
//                    {
//                        return true;
//                    }
//                    else
//                    {
//                        return false;
//                    }
//                }
//            }
//            catch (Exception ex)
//            {
//                return false;
//            }
//        }

//        public static bool AgentPasswordReset(int ApiUserID, string Link)
//        {
//            try
//            {
//                using (var unit = new Models.DAL.UnitOfWork())
//                {
//                    var email = unit.EmailRepository.Get(a => a.EmailCode == "PWD101").FirstOrDefault();
//                    var apiuser = unit.ApiUserRepository.GetByID(ApiUserID);

//                    var receiverList = unit.EmailEmailReceiverMapperRepository.Get(a => a.RefEmailID == email.EmailID).ToList();
//                    List<string> receiverID = new List<string>();
//                    receiverID.Add(apiuser.UserEmail);
//                    foreach (var item in receiverList)
//                    {
//                        receiverID.Add(unit.EmailReceiverRepository.Get(a => a.ReceiverID == item.RefReceiverID).FirstOrDefault().EmailID);
//                    }

//                    string mailBody = email.EmailBody.Replace("{{UserName}}", apiuser.UserName);
//                    mailBody = mailBody.Replace("{{Link}}", Link);
//                    if (SendEmailAsync(email.SenderID, email.EmailID, receiverID, email.EmailSubject, mailBody))
//                    {
//                        return true;
//                    }
//                    else
//                    {
//                        return false;
//                    }
//                }
//            }
//            catch (Exception ex)
//            {
//                return false;
//            }
//        }

//        public static bool AgentChangePasswordConfirmation()
//        {

//            return false;
//        }

//        public static bool AgentMinimumBalanceNotification(int ApiUserID,decimal CurrentBalance)
//        {
//            try
//            {
//                using (var unit = new Models.DAL.UnitOfWork())
//                {
//                    var email = unit.EmailRepository.Get(a => a.EmailCode == "REG102").FirstOrDefault();
//                    var apiuser = unit.ApiUserRepository.GetByID(ApiUserID);

//                    var receiverList = unit.EmailEmailReceiverMapperRepository.Get(a => a.RefEmailID == email.EmailID).ToList();
//                    List<string> receiverID = new List<string>();
//                    receiverID.Add(apiuser.UserEmail);
//                    foreach (var item in receiverList)
//                    {
//                        receiverID.Add(unit.EmailReceiverRepository.Get(a => a.ReceiverID == item.RefReceiverID).FirstOrDefault().EmailID);
//                    }

//                    string mailBody = email.EmailBody.Replace("{{UserName}}", apiuser.UserName);
//                    mailBody = email.EmailBody.Replace("{{CurrentBalance}}", CurrentBalance.ToString());
//                    if (SendEmailAsync(email.SenderID, email.EmailID, receiverID, email.EmailSubject, mailBody))
//                    {
//                        return true;
//                    }
//                    else
//                    {
//                        return false;
//                    }
//                }
//            }
//            catch (Exception ex)
//            {
//                return false;
//            }
//        }

//        public static bool AgentLookToBook(int ApiUserID)
//        {
//            try
//            {
//                using (var unit = new Models.DAL.UnitOfWork())
//                {
//                    var email = unit.EmailRepository.Get(a => a.EmailCode == "REG102").FirstOrDefault();
//                    var apiuser = unit.ApiUserRepository.GetByID(ApiUserID);

//                    var receiverList = unit.EmailEmailReceiverMapperRepository.Get(a => a.RefEmailID == email.EmailID).ToList();
//                    List<string> receiverID = new List<string>();
//                    receiverID.Add(apiuser.UserEmail);
//                    foreach (var item in receiverList)
//                    {
//                        receiverID.Add(unit.EmailReceiverRepository.Get(a => a.ReceiverID == item.RefReceiverID).FirstOrDefault().EmailID);
//                    }

//                    string mailBody = email.EmailBody.Replace("{{UserName}}", apiuser.UserName);
//                    if (SendEmailAsync(email.SenderID, email.EmailID, receiverID, email.EmailSubject, mailBody))
//                    {
//                        return true;
//                    }
//                    else
//                    {
//                        return false;
//                    }
//                }
//            }
//            catch (Exception ex)
//            {
//                return false;
//            }
//        }

//        public static bool AgentBalanceDeduction(int ApiUserID, decimal Amount, string Purpose)
//        {
//            try
//            {
//                using (var unit = new Models.DAL.UnitOfWork())
//                {
//                    var email = unit.EmailRepository.Get(a => a.EmailCode == "REG102").FirstOrDefault();
//                    var apiuser = unit.ApiUserRepository.GetByID(ApiUserID);

//                    var receiverList = unit.EmailEmailReceiverMapperRepository.Get(a => a.RefEmailID == email.EmailID).ToList();
//                    List<string> receiverID = new List<string>();
//                    receiverID.Add(apiuser.UserEmail);
//                    foreach (var item in receiverList)
//                    {
//                        receiverID.Add(unit.EmailReceiverRepository.Get(a => a.ReceiverID == item.RefReceiverID).FirstOrDefault().EmailID);
//                    }

//                    string mailBody = email.EmailBody.Replace("{{UserName}}", apiuser.UserName);
//                    mailBody = mailBody.Replace("{{Amount}}", Amount.ToString());
//                    mailBody = email.EmailBody.Replace("{{Purpose}}", Purpose);
//                    if (SendEmailAsync(email.SenderID, email.EmailID, receiverID, email.EmailSubject, mailBody))
//                    {
//                        return true;
//                    }
//                    else
//                    {
//                        return false;
//                    }
//                }
//            }
//            catch (Exception ex)
//            {
//                return false;
//            }
//        }

//        public static bool AirBookingConfirmation(int ApiUserid, string LeadPax, string AirlinePnr, string IgxRefNo, decimal TotalFare)
//        {
//            try
//            {
//                using (var unit = new Models.DAL.UnitOfWork())
//                {
//                    var email = unit.EmailRepository.Get(a => a.EmailCode == "AIR101").FirstOrDefault();
//                    var apiuser = unit.ApiUserRepository.GetByID(ApiUserid);

//                    var receiverList = unit.EmailEmailReceiverMapperRepository.Get(a => a.RefEmailID == email.EmailID).ToList();
//                    List<string> receiverID = new List<string>();
//                    receiverID.Add(apiuser.UserEmail);
//                    foreach (var item in receiverList)
//                    {
//                        receiverID.Add(unit.EmailReceiverRepository.Get(a => a.ReceiverID == item.RefReceiverID).FirstOrDefault().EmailID);
//                    }

//                    string mailBody = email.EmailBody.Replace("{{UserName}}", apiuser.UserName);
//                    mailBody = mailBody.Replace("{{PaxName}}", LeadPax);
//                    mailBody = mailBody.Replace("{{PNR}}", AirlinePnr);
//                    mailBody = mailBody.Replace("{{IgxRefNo}}", IgxRefNo);
//                    mailBody = mailBody.Replace("{{TotalFare}}", TotalFare.ToString());
//                    mailBody = mailBody.Replace("{{UserCurrencyCode}}", unit.CrossCurrencyRepository.Get(a => a.CurrencyID == apiuser.ApiUserBaseCurrency).FirstOrDefault().CurrencyCode ?? "");


//                    if (SendEmailAsync(email.SenderID, email.EmailID, receiverID, email.EmailSubject, mailBody))
//                    {
//                        return true;
//                    }
//                    else
//                    {
//                        return false;
//                    }
//                }

//            }
//            catch (Exception)
//            {
//                return false;
//            }
//        }

//        public static bool AirTicketingConfirmation(int ApiUserid, string LeadPax, string AirlinePnr, string TicketNo, string IgxRefNo, decimal TotalPrice)
//        {
//            try
//            {
//                using (var unit = new Models.DAL.UnitOfWork())
//                {
//                    var email = unit.EmailRepository.Get(a => a.EmailCode == "AIR102").FirstOrDefault();
//                    var apiuser = unit.ApiUserRepository.GetByID(ApiUserid);

//                    var receiverList = unit.EmailEmailReceiverMapperRepository.Get(a => a.RefEmailID == email.EmailID).ToList();
//                    List<string> receiverID = new List<string>();
//                    receiverID.Add(apiuser.UserEmail);
//                    foreach (var item in receiverList)
//                    {
//                        receiverID.Add(unit.EmailReceiverRepository.Get(a => a.ReceiverID == item.RefReceiverID).FirstOrDefault().EmailID);
//                    }

//                    string mailBody = email.EmailBody.Replace("{{UserName}}", apiuser.UserName);
//                    mailBody = mailBody.Replace("{{PaxName}}", LeadPax);
//                    mailBody = mailBody.Replace("{{PNR}}", AirlinePnr);
//                    mailBody = mailBody.Replace("{{TicketNo}}", TicketNo);
//                    mailBody = mailBody.Replace("{{IgxRefNo}}", IgxRefNo);


//                    if (SendEmailAsync(email.SenderID, email.EmailID, receiverID, email.EmailSubject, mailBody))
//                    {
//                        return true;
//                    }
//                    else
//                    {
//                        return false;
//                    }
//                }

//            }
//            catch (Exception ex)
//            {
//                return false;
//            }
//        }

//        public static bool HotelBookingConfirmation(int ApiUserID, string BookingCreateDate, string ChechkInDate, string ChechkOutDate, string CityName, string HotelName, string LeadPax, string BookingNo, string IgxRefNo)
//        {
//            try
//            {
//                using (var unit = new Models.DAL.UnitOfWork())
//                {
//                    var email = unit.EmailRepository.Get(a => a.EmailCode == "HOTEL101").FirstOrDefault();
//                    var apiuser = unit.ApiUserRepository.GetByID(ApiUserID);

//                    var receiverList = unit.EmailEmailReceiverMapperRepository.Get(a => a.RefEmailID == email.EmailID).ToList();
//                    List<string> receiverID = new List<string>();
//                    receiverID.Add(apiuser.UserEmail);
//                    foreach (var item in receiverList)
//                    {
//                        receiverID.Add(unit.EmailReceiverRepository.Get(a => a.ReceiverID == item.RefReceiverID).FirstOrDefault().EmailID);
//                    }

//                    string mailBody = email.EmailBody.Replace("{{UserName}}", apiuser.UserName);
//                    mailBody = mailBody.Replace("{{BookingCreateDate}}", BookingCreateDate);
//                    mailBody = mailBody.Replace("{{CheckInDate}}", ChechkInDate);
//                    mailBody = mailBody.Replace("{{CheckOutDate}}", ChechkOutDate);
//                    mailBody = mailBody.Replace("{{LeadPax}}", LeadPax);
//                    mailBody = mailBody.Replace("{{City}}", CityName);
//                    mailBody = mailBody.Replace("{{HotelName}}", HotelName);
//                    mailBody = mailBody.Replace("{{BookingID}}", BookingNo);
//                    mailBody = mailBody.Replace("{{IGXRefNo}}", IgxRefNo);

//                    if (SendEmailAsync(email.SenderID, email.EmailID, receiverID, email.EmailSubject, mailBody))
//                    {
//                        return true;
//                    }
//                    else
//                    {
//                        return false;
//                    }
//                }

//            }
//            catch (Exception ex)
//            {
//                return false;
//            }
//        }

//        public static bool ETicketOrdered()
//        {

//            return false;
//        }

//        public static bool EticketConfirmation()
//        {
//            return false;
//        }

//        public static bool AgentEmail(int id, string subject, string body)
//        {
//            try
//            {
//                using (var unit = new Models.DAL.UnitOfWork())
//                {
//                    var email = unit.EmailRepository.Get(a => a.EmailCode == "CUSTOM101").FirstOrDefault();
//                    var apiuser = unit.ApiUserRepository.GetByID(id);
//                    if (apiuser != null)
//                    {
//                        var receiverList = unit.EmailEmailReceiverMapperRepository.Get(a => a.RefEmailID == email.EmailID).ToList();
//                        List<string> receiverID = new List<string>();
//                        receiverID.Add(apiuser.UserEmail);
//                        foreach (var item in receiverList)
//                        {
//                            receiverID.Add(unit.EmailReceiverRepository.Get(a => a.ReceiverID == item.RefReceiverID).FirstOrDefault().EmailID);
//                        }
//                        string mailSubject = email.EmailSubject.Replace(email.EmailSubject, subject);
//                        string mailBody = email.EmailBody.Replace("{{UserName}}", apiuser.UserName);
//                        mailBody = mailBody.Replace("{{Body}}", body);
//                        if (SendEmailAsync(email.SenderID, email.EmailID, receiverID, mailSubject, mailBody))
//                        {
//                            return true;
//                        }
//                        else
//                        {
//                            return false;
//                        }
//                    }
//                    else
//                    {
//                        var receiverList = unit.EmailEmailReceiverMapperRepository.Get(a => a.RefEmailID == email.EmailID).ToList();
//                        List<string> receiverID = new List<string>();
//                        var apiUserList = unit.ApiUserRepository.Get().ToList();
//                        foreach (var apiUser in apiUserList)
//                        {
//                            receiverID.Add(apiUser.UserEmail);
//                        }
//                        //receiverID.Add(apiuser.UserEmail);
//                        foreach (var item in receiverList)
//                        {
//                            receiverID.Add(unit.EmailReceiverRepository.Get(a => a.ReceiverID == item.RefReceiverID).FirstOrDefault().EmailID);
//                        }
//                        string mailSubject = subject;
//                        string mailBody = body;
//                        //mailBody = email.EmailBody.Replace("{{Body}}", body);
//                        if (SendEmailAsync(email.SenderID, email.EmailID, receiverID, mailSubject, mailBody))
//                        {
//                            return true;
//                        }
//                        else
//                        {
//                            return false;
//                        }
//                    }
//                }
//            }
//            catch (Exception ex)
//            {
//                return false;
//            }
//        }







//        public static bool SendEmailAsync(int SenderID, int EmailID, List<string> ReceiverID, string subject, string body)
//        {
//            Models.EmailSenderId emailSenderId = new Models.EmailSenderId();
//            using (var unit = new UnitOfWork())
//            {
//                emailSenderId = unit.EmailSenderIdRepository.GetByID(SenderID);
//            }
//            if (emailSenderId != null)
//            {
//                //EmailViewModel email = new EmailViewModel();
//                //email.SenderEmail = Properties.Settings.Default.EmailSenderID;
//                //email.RecevierName = "Khan";
//                //email.EmailHeader = "Gati Request Quote";
//                //email.EmailBody = "Request for a Quote notification";
//                string noreplay = emailSenderId.EmailID; //Properties.Settings.Default.EmailSenderID; //WebConfigurationManager.AppSettings["email-Form"].ToString();
//                var message = new MailMessage();
//                foreach (var item in ReceiverID)
//                {
//                    message.To.Add(item);//new MailAddress(aDistrict.DistrictEmail));
//                }

//                message.From = new MailAddress(noreplay);  // replace with valid value
//                message.Subject = subject;
//                //var body = body;
//                message.Body = string.Format(body, ReceiverID, ReceiverID, body);
//                message.IsBodyHtml = true;
//                //message.CC.Add("kamrulbd.live@Hotmail.com");//super admin user    
//                //message.Bcc.Add("kamrul.csepu@gmail.com");//super admin user

//                //if (string.IsNullOrEmpty(noreplay))
//                //{
//                //    message.Sender = new MailAddress("kamrul@primetechbd.com");
//                //}
//                //else
//                //{
//                //    message.Sender = new MailAddress(noreplay);
//                //}

//                using (var smtp = new SmtpClient())
//                {
//                    var credential = new NetworkCredential
//                    {
//                        UserName = emailSenderId.EmailID,
//                        Password = emailSenderId.EmailPassword
//                    };
//                    smtp.Credentials = credential;
//                    string host = emailSenderId.EmailHost;
//                    smtp.Host = host;
//                   // int Port = Convert.ToInt32(Properties.Settings.Default.EmailPort);
//                    smtp.Port = Convert.ToInt32(emailSenderId.Port);
//                    smtp.EnableSsl = emailSenderId.EnableSSL; //Convert.ToBoolean(WebConfigurationManager.AppSettings["smtp-EnableSsl"].ToString()); ;
//                    smtp.UseDefaultCredentials = emailSenderId.UseDefaultCredentials;
//                    smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
//                    //smtp.
//                    try
//                    {
//                        smtp.Send(message);
//                        using (var unit = new UnitOfWork())
//                        {
//                            Models.EmailSendingLog emailSendingLog = new Models.EmailSendingLog();

//                            emailSendingLog.EmailID = EmailID;
//                            emailSendingLog.SenderID = SenderID;

//                            emailSendingLog.SendingDate = DateTime.Now;
//                            emailSendingLog.SendingTime = DateTime.Now;
//                            emailSendingLog.SendingStatus = 0;
//                            emailSendingLog.IsRemoved = 0;

//                            unit.EmailSendingLogRepository.Insert(emailSendingLog);
//                            unit.Save();
//                        }

//                        return true;
//                    }
//                    catch (Exception ex)
//                    {
//                        Helper.ErrorLogSaveHelper.SaveError("EmailHelper", "SendEmailAsync", "SendEmailAsync", ex.Message);
//                        using (var unit = new UnitOfWork())
//                        {
//                            Models.EmailSendingLog emailSendingLog = new Models.EmailSendingLog();

//                            emailSendingLog.EmailID = EmailID;
//                            emailSendingLog.SenderID = SenderID;

//                            emailSendingLog.SendingDate = DateTime.Now;
//                            emailSendingLog.SendingTime = DateTime.Now;
//                            emailSendingLog.SendingStatus = 0;
//                            emailSendingLog.IsRemoved = 0;

//                            unit.EmailSendingLogRepository.Insert(emailSendingLog);
//                            unit.Save();
//                        }
//                        Helper.ErrorLogSaveHelper.SaveError("EmailHelper", "SendEmailAsync", "SendEmailAsync", ex.Message);
//                        return false;
//                    }
//                }


//            }

//            return false;
//        }


//    }
//}