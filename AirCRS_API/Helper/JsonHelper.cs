﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;

namespace AirCRS_API.Helper
{
    public abstract class JsonHelper
    {


        public static HttpResponseMessage JsonSerialize(Object responseString)
        {
            string json = JsonConvert.SerializeObject(responseString);

            var resp = new HttpResponseMessage()
            {
                Content = new StringContent(json)
            };
            resp.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            return resp;
        }

        public static HttpRequestMessage GenericJsonSerialize<T>(T tEntity)
        {
            string json = JsonConvert.SerializeObject(tEntity);

            var resp = new HttpRequestMessage()
            {
                Content = new StringContent(json)
            };
            resp.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            return resp;
        }
        public static async System.Threading.Tasks.Task<string> GetPaymentGetwayApiResponse(string BaseUrl, string MethodUrl)
        {
            HttpResponseMessage result = new HttpResponseMessage();
            var content2 = JsonConvert.SerializeObject("");
            HttpContent content = new StringContent(content2, Encoding.UTF8, "application/json");
            using (HttpClient client = new HttpClient { BaseAddress = new Uri(BaseUrl) })
            {
                var uri = new Uri(BaseUrl + MethodUrl);
                result = await client.GetAsync(uri);
            }
            var jsonString = result.Content.ReadAsStringAsync();
            jsonString.Wait();
            return jsonString.Result;
        }

    }
}