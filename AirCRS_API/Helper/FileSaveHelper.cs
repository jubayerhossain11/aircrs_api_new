﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace AirCRS_API.Helper
{
    public abstract class FileSaveHelper
    {
        public bool SaveFile(HttpPostedFile postedFile,string folderPath)
        {
            try
            {
                string path = System.Web.HttpContext.Current.Server.MapPath(folderPath);
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                var fileSavePath = Path.Combine(path, postedFile.FileName);

                if (!string.IsNullOrEmpty(fileSavePath))
                {
                    if (File.Exists(fileSavePath))
                    {
                        // deletevprevious file
                        File.Delete(fileSavePath);
                    }
                }

                bool checkFileSave = false;
               
                    postedFile.SaveAs(fileSavePath);
                    checkFileSave = true;
                   
                    checkFileSave = false;
                   
                return checkFileSave;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}