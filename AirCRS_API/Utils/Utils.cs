﻿using AirCRS_API.SERVICE.CommonServices.Dtos;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AirCRS_API.Utils
{
    public static class UtilsGetAirLines
    {
        public static List<AirlineJson> GetAllAirLinesData()
        {
            List<AirlineJson> Airlines = new List<AirlineJson>();
            try
            {
                using (System.IO.StreamReader read = new System.IO.StreamReader(System.Web.Hosting.HostingEnvironment.MapPath("~/Content/JsonFiles/AirportTimeZone.json")))
                {
                    var json = read.ReadToEnd();
                    Airlines = JsonConvert.DeserializeObject<List<AirlineJson>>(json);
                }
            }
            catch (Exception ex)
            {

            }
            return Airlines;

        }
    }


}