﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace AirCRS_API.Services.Utils
{
    abstract class Extension
    {
        //public static string SendEmailToUrl(SendEmail model)
        //{
        //    try
        //    {
        //        //PayloadResponse responseObj = new PayloadResponse();

        //        string path = System.Configuration.ConfigurationManager.AppSettings["email_send_url"];

        //        string BaseUrl = path;
        //        var content2 = JsonConvert.SerializeObject(model);
        //        HttpContent content = new StringContent(content2, Encoding.UTF8, "application/json");

        //        var client = new HttpClient { BaseAddress = new Uri(BaseUrl) };
        //        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

        //        var uri = new Uri(BaseUrl);

        //        HttpResponseMessage result = client.PostAsync(uri, content).Result;

        //        var jsonString = result.Content.ReadAsStringAsync();
        //        jsonString.Wait();

        //        //responseObj = JsonConvert.DeserializeObject<PayloadResponse>(jsonString.Result);
        //        //if (responseObj.Success)
        //        //{
        //        //    return responseObj;
        //        //}

        //        return jsonString.Result;
        //    }
        //    catch (Exception ex)
        //    {
        //        return "Error " + ex.ToString();
        //    }
        //}

        #region Methods For Amount In Words
        private static String ones(String Number)
        {
            int _Number = Convert.ToInt32(Number);
            String name = "";
            switch (_Number)
            {

                case 1:
                    name = "One";
                    break;
                case 2:
                    name = "Two";
                    break;
                case 3:
                    name = "Three";
                    break;
                case 4:
                    name = "Four";
                    break;
                case 5:
                    name = "Five";
                    break;
                case 6:
                    name = "Six";
                    break;
                case 7:
                    name = "Seven";
                    break;
                case 8:
                    name = "Eight";
                    break;
                case 9:
                    name = "Nine";
                    break;
            }
            return name;
        }

        private static String tens(String Number)
        {
            int _Number = Convert.ToInt32(Number);
            String name = null;
            switch (_Number)
            {
                case 10:
                    name = "Ten";
                    break;
                case 11:
                    name = "Eleven";
                    break;
                case 12:
                    name = "Twelve";
                    break;
                case 13:
                    name = "Thirteen";
                    break;
                case 14:
                    name = "Fourteen";
                    break;
                case 15:
                    name = "Fifteen";
                    break;
                case 16:
                    name = "Sixteen";
                    break;
                case 17:
                    name = "Seventeen";
                    break;
                case 18:
                    name = "Eighteen";
                    break;
                case 19:
                    name = "Nineteen";
                    break;
                case 20:
                    name = "Twenty";
                    break;
                case 30:
                    name = "Thirty";
                    break;
                case 40:
                    name = "Fourty";
                    break;
                case 50:
                    name = "Fifty";
                    break;
                case 60:
                    name = "Sixty";
                    break;
                case 70:
                    name = "Seventy";
                    break;
                case 80:
                    name = "Eighty";
                    break;
                case 90:
                    name = "Ninety";
                    break;
                default:
                    if (_Number > 0)
                    {
                        name = tens(Number.Substring(0, 1) + "0") + " " + ones(Number.Substring(1));
                    }
                    break;
            }
            return name;
        }

        private static String ConvertWholeNumber(String Number)
        {
            string word = "";
            try
            {
                bool beginsZero = false;//tests for 0XX    
                bool isDone = false;//test if already translated    
                double dblAmt = (Convert.ToDouble(Number));
                //if ((dblAmt > 0) && number.StartsWith("0"))    
                if (dblAmt > 0)
                {//test for zero or digit zero in a nuemric    
                    beginsZero = Number.StartsWith("0");

                    int numDigits = Number.Length;
                    int pos = 0;//store digit grouping    
                    String place = "";//digit grouping name:hundres,thousand,etc...    
                    switch (numDigits)
                    {
                        case 1://ones' range    

                            word = ones(Number);
                            isDone = true;
                            break;
                        case 2://tens' range    
                            word = tens(Number);
                            isDone = true;
                            break;
                        case 3://hundreds' range    
                            pos = (numDigits % 3) + 1;
                            place = " Hundred ";
                            break;
                        case 4://thousands' range    
                        case 5:
                        case 6:
                            pos = (numDigits % 4) + 1;
                            place = " Thousand ";
                            break;
                        case 7://millions' range    
                        case 8:
                        case 9:
                            pos = (numDigits % 6) + 1;
                            place = " Lac ";
                            break;
                        case 10://Billions's range    
                        case 11:
                        case 12:

                            pos = (numDigits % 8) + 1;
                            place = " Crore ";
                            break;
                        //add extra case options for anything above Billion...    
                        default:
                            isDone = true;
                            break;
                    }
                    if (!isDone)
                    {//if transalation is not done, continue...(Recursion comes in now!!)    
                        if (Number.Substring(0, pos) != "0" && Number.Substring(pos) != "0")
                        {
                            try
                            {
                                word = ConvertWholeNumber(Number.Substring(0, pos)) + place + ConvertWholeNumber(Number.Substring(pos));
                            }
                            catch { }
                        }
                        else
                        {
                            word = ConvertWholeNumber(Number.Substring(0, pos)) + ConvertWholeNumber(Number.Substring(pos));
                        }

                        //check for trailing zeros    
                        //if (beginsZero) word = " and " + word.Trim();    
                    }
                    //ignore digit grouping names    
                    if (word.Trim().Equals(place.Trim())) word = "";
                }
            }
            catch { }
            return word.Trim();
        }

        private static String ConvertToWords(String numb)
        {
            String val = "", wholeNo = numb, points = "", andStr = "", pointStr = "";
            String endStr = "Only";
            try
            {
                int decimalPlace = numb.IndexOf(".");
                if (decimalPlace > 0)
                {
                    wholeNo = numb.Substring(0, decimalPlace);
                    points = numb.Substring(decimalPlace + 1);
                    if (Convert.ToInt32(points) > 0)
                    {
                        andStr = "Taka and";// just to separate whole numbers from points/cents    
                        endStr = "Paisa " + endStr;//Cents    
                        pointStr = ConvertDecimals(points);
                    }
                }
                val = String.Format("{0} {1}{2} {3}", ConvertWholeNumber(wholeNo).Trim(), andStr, pointStr, endStr);
            }
            catch { }
            return val;
        }

        private static String ConvertDecimals(String number)
        {
            String cd = "", digit = "", engOne = "";
            for (int i = 0; i < number.Length; i++)
            {
                digit = number[i].ToString();
                if (digit.Equals("0"))
                {
                    engOne = "Zero";
                }
                else
                {
                    engOne = ones(digit);
                }
                cd += " " + engOne;
            }
            return cd;
        }
        #endregion

        #region Convert Amount in words BDT
        public static string comma(decimal amount)
        {
            string[] strArrays;
            string str = "";
            string str1 = "";
            string str2 = "";
            str1 = amount.ToString();
            int num = amount.ToString().IndexOf(".", 0);
            str2 = amount.ToString().Substring(num + 1);
            if (!(str1 == str2))
            {
                str1 = amount.ToString().Substring(0, amount.ToString().IndexOf(".", 0));
                str1 = str1.Replace(",", "").ToString();
            }
            else
            {
                str2 = "";
            }
            switch (str1.Length)
            {
                case 4:
                    {
                        if (!(str2 == ""))
                        {
                            strArrays = new string[] { str1.Substring(0, 1), ",", str1.Substring(1, 3), ".", str2 };
                            str = string.Concat(strArrays);
                        }
                        else
                        {
                            str = string.Concat(str1.Substring(0, 1), ",", str1.Substring(1, 3));
                        }
                        break;
                    }
                case 5:
                    {
                        if (!(str2 == ""))
                        {
                            strArrays = new string[] { str1.Substring(0, 2), ",", str1.Substring(2, 3), ".", str2 };
                            str = string.Concat(strArrays);
                        }
                        else
                        {
                            str = string.Concat(str1.Substring(0, 2), ",", str1.Substring(2, 3));
                        }
                        break;
                    }
                case 6:
                    {
                        if (!(str2 == ""))
                        {
                            strArrays = new string[] { str1.Substring(0, 1), ",", str1.Substring(1, 2), ",", str1.Substring(3, 3), ".", str2 };
                            str = string.Concat(strArrays);
                        }
                        else
                        {
                            strArrays = new string[] { str1.Substring(0, 1), ",", str1.Substring(1, 2), ",", str1.Substring(3, 3) };
                            str = string.Concat(strArrays);
                        }
                        break;
                    }
                case 7:
                    {
                        if (!(str2 == ""))
                        {
                            strArrays = new string[] { str1.Substring(0, 2), ",", str1.Substring(2, 2), ",", str1.Substring(4, 3), ".", str2 };
                            str = string.Concat(strArrays);
                        }
                        else
                        {
                            strArrays = new string[] { str1.Substring(0, 2), ",", str1.Substring(2, 2), ",", str1.Substring(4, 3) };
                            str = string.Concat(strArrays);
                        }
                        break;
                    }
                case 8:
                    {
                        if (!(str2 == ""))
                        {
                            strArrays = new string[] { str1.Substring(0, 1), ",", str1.Substring(1, 2), ",", str1.Substring(3, 2), ",", str1.Substring(5, 3), ".", str2 };
                            str = string.Concat(strArrays);
                        }
                        else
                        {
                            strArrays = new string[] { str1.Substring(0, 1), ",", str1.Substring(1, 2), ",", str1.Substring(3, 2), ",", str1.Substring(5, 3) };
                            str = string.Concat(strArrays);
                        }
                        break;
                    }
                case 9:
                    {
                        if (!(str2 == ""))
                        {
                            strArrays = new string[] { str1.Substring(0, 2), ",", str1.Substring(2, 2), ",", str1.Substring(4, 2), ",", str1.Substring(6, 3), ".", str2 };
                            str = string.Concat(strArrays);
                        }
                        else
                        {
                            strArrays = new string[] { str1.Substring(0, 2), ",", str1.Substring(2, 2), ",", str1.Substring(4, 2), ",", str1.Substring(6, 3) };
                            str = string.Concat(strArrays);
                        }
                        break;
                    }
                default:
                    {
                        str = (!(str2 == "") ? string.Concat(str1, ".", str2) : str1);
                        break;
                    }
            }
            return str;
        }

        public static string F_Crore(string amt, string amt_paisa)
        {
            string str = "";
            string str1 = "";
            string str2 = "";
            string str3 = "";
            string str4 = "";
            string str5 = "";
            int num = 0;
            int num1 = 0;
            int num2 = 0;
            int num3 = 0;
            int num4 = 0;
            if (amt_paisa == "")
            {
                num = Convert.ToInt32(amt.Substring(0, 1));
                str = (num <= 1 ? string.Concat(Tens(num.ToString()), " Crore") : string.Concat(Tens(num.ToString()), " Crores"));
                if (amt.Substring(1, 7) != "0000000")
                {
                    if (amt.Substring(1, 2) != "00")
                    {
                        if (!(amt.Substring(1, 1) != "0"))
                        {
                            num1 = Convert.ToInt32(amt.Substring(2, 1));
                            str1 = (!(amt.Substring(3, 5) == "00000") ? string.Concat(" ", Tens(num1.ToString())) : string.Concat(" and ", Tens(num1.ToString())));
                            str1 = (num1 <= 1 ? string.Concat(str1, " Lakh") : string.Concat(str1, " Lakhs"));
                        }
                        else
                        {
                            num1 = Convert.ToInt32(amt.Substring(1, 2));
                            str1 = (!(amt.Substring(3, 5) == "00000") ? string.Concat(" ", Word_Spell_Tens(num1.ToString()), " Lakhs") : string.Concat(" and ", Word_Spell_Tens(num1.ToString()), " Lakhs"));
                        }
                    }
                    if (amt.Substring(3, 2) != "00")
                    {
                        if (!(amt.Substring(3, 1) != "0"))
                        {
                            num2 = Convert.ToInt32(amt.Substring(4, 1));
                            str2 = (!(amt.Substring(5, 3) == "000") ? string.Concat(" ", Tens(num2.ToString())) : string.Concat(" and ", Tens(num2.ToString())));
                            str2 = (num2 <= 1 ? string.Concat(str2, " Thousand") : string.Concat(str2, " Thousands"));
                        }
                        else
                        {
                            num2 = Convert.ToInt32(amt.Substring(3, 2));
                            str2 = (!(amt.Substring(5, 3) == "000") ? string.Concat(" ", Word_Spell_Tens(num2.ToString()), " Thousands") : string.Concat(" and ", Word_Spell_Tens(num2.ToString()), " Thousands"));
                        }
                    }
                    if (amt.Substring(5, 3) != "000")
                    {
                        if (amt.Substring(5, 1) != "0")
                        {
                            num3 = Convert.ToInt32(amt.Substring(5, 1));
                            if (num3 <= 1)
                            {
                                str3 = (!(amt.Substring(6, 2) == "00") ? string.Concat(" ", Tens(num3.ToString()), " Hundred") : string.Concat(" and", Tens(num3.ToString()), " Hundred"));
                            }
                            else
                            {
                                str3 = (!(amt.Substring(6, 2) == "00") ? string.Concat(" ", Tens(num3.ToString()), " Hundreds") : string.Concat(" and", Tens(num3.ToString()), " Hundreds"));
                            }
                        }
                        if (amt.Substring(6, 2) != "00")
                        {
                            num4 = Convert.ToInt32(amt.Substring(6, 2));
                            str4 = (Convert.ToInt32(amt.Substring(6, 1)) == 0 ? string.Concat(" and ", Tens(num4.ToString())) : string.Concat(" and ", Word_Spell_Tens(num4.ToString())));
                        }
                    }
                }
            }
            else if (amt_paisa != "")
            {
                num = Convert.ToInt32(amt.Substring(0, 1));
                str = (num <= 1 ? string.Concat(Tens(num.ToString()), " Crore") : string.Concat(Tens(num.ToString()), " Crores"));
                if (amt.Substring(1, 7) != "0000000")
                {
                    if (amt.Substring(1, 2) != "00")
                    {
                        if (!(amt.Substring(1, 1) != "0"))
                        {
                            num1 = Convert.ToInt32(amt.Substring(2, 1));
                            str1 = (num1 <= 1 ? string.Concat(" ", Tens(num1.ToString()), " Lakh") : string.Concat(" ", Tens(num1.ToString()), " Lakhs"));
                        }
                        else
                        {
                            num1 = Convert.ToInt32(amt.Substring(1, 2));
                            str1 = string.Concat(" ", Word_Spell_Tens(num1.ToString()), " Lakhs");
                        }
                    }
                    if (amt.Substring(3, 2) != "00")
                    {
                        if (!(amt.Substring(3, 1) != "0"))
                        {
                            num2 = Convert.ToInt32(amt.Substring(4, 1));
                            str2 = (num2 <= 1 ? string.Concat(" ", Tens(num2.ToString()), " Thousand") : string.Concat(" ", Tens(num2.ToString()), " Thousands"));
                        }
                        else
                        {
                            num2 = Convert.ToInt32(amt.Substring(3, 2));
                            str2 = string.Concat(" ", Word_Spell_Tens(num2.ToString()), " Thousands");
                        }
                    }
                    if (amt.Substring(5, 3) != "000")
                    {
                        if (amt.Substring(5, 1) != "0")
                        {
                            num3 = Convert.ToInt32(amt.Substring(5, 1));
                            str3 = (num3 <= 1 ? string.Concat(" ", Tens(num3.ToString()), " Hundred") : string.Concat(" ", Tens(num3.ToString()), " Hundreds"));
                        }
                        if (amt.Substring(6, 2) != "00")
                        {
                            num4 = Convert.ToInt32(amt.Substring(6, 2));
                            str4 = (!(amt.Substring(6, 1) != "0") ? string.Concat(" ", Tens(num4.ToString())) : string.Concat(" ", Word_Spell_Tens(num4.ToString())));
                        }
                    }
                }
                if (amt_paisa.Substring(0, 2) != "00")
                {
                    str5 = (!(amt_paisa.Substring(0, 1) != "0") ? string.Concat(" ", Tens(amt_paisa.Substring(0, 2)), " Paisa") : string.Concat(" and ", Word_Spell_Tens(amt_paisa.Substring(0, 2)), " Paisa"));
                }
            }
            string[] strArrays = new string[] { "Taka ", str, str1, str2, str3, str4, str5, " Only" };
            return string.Concat(strArrays);
        }

        public static string F_Crores(string amt, string amt_paisa)
        {
            string str = "";
            string str1 = "";
            string str2 = "";
            string str3 = "";
            string str4 = "";
            string str5 = "";
            int num = 0;
            int num1 = 0;
            int num2 = 0;
            int num3 = 0;
            int num4 = 0;
            if (amt_paisa == "")
            {
                num = Convert.ToInt32(amt.Substring(0, 2));
                str = string.Concat(Word_Spell_Tens(num.ToString()), " Crores");
                if (amt.Substring(2, 7) != "0000000")
                {
                    if (amt.Substring(2, 2) != "00")
                    {
                        if (!(amt.Substring(2, 1) != "0"))
                        {
                            num1 = Convert.ToInt32(amt.Substring(3, 1));
                            str1 = (!(amt.Substring(4, 5) == "00000") ? string.Concat(" ", Tens(num1.ToString())) : string.Concat(" and ", Tens(num1.ToString())));
                            str1 = (num1 <= 1 ? string.Concat(str1, " Lakh") : string.Concat(str1, " Lakhs"));
                        }
                        else
                        {
                            num1 = Convert.ToInt32(amt.Substring(2, 2));
                            str1 = (!(amt.Substring(4, 5) == "00000") ? string.Concat(" ", Word_Spell_Tens(num1.ToString()), " Lakhs") : string.Concat(" and ", Word_Spell_Tens(num1.ToString()), " Lakhs"));
                        }
                    }
                    if (amt.Substring(4, 2) != "00")
                    {
                        if (!(amt.Substring(4, 1) != "0"))
                        {
                            num2 = Convert.ToInt32(amt.Substring(5, 1));
                            str2 = (!(amt.Substring(4, 5) == "000") ? string.Concat(" ", Tens(num2.ToString())) : string.Concat(" and ", Tens(num2.ToString())));
                            str2 = (num2 <= 1 ? string.Concat(str2, " Thousand") : string.Concat(str2, " Thousands"));
                        }
                        else
                        {
                            num2 = Convert.ToInt32(amt.Substring(4, 2));
                            str2 = (!(amt.Substring(6, 3) == "000") ? string.Concat(" ", Word_Spell_Tens(num2.ToString()), " Thousands") : string.Concat(" and ", Word_Spell_Tens(num2.ToString()), " Thousands"));
                        }
                    }
                    if (amt.Substring(6, 3) != "000")
                    {
                        if (amt.Substring(6, 1) != "0")
                        {
                            num3 = Convert.ToInt32(amt.Substring(6, 1));
                            if (num3 <= 1)
                            {
                                str3 = (!(amt.Substring(7, 2) == "00") ? string.Concat(" ", Tens(num3.ToString()), " Hundred") : string.Concat(" and", Tens(num3.ToString()), " Hundred"));
                            }
                            else
                            {
                                str3 = (!(amt.Substring(7, 2) == "00") ? string.Concat(" ", Tens(num3.ToString()), " Hundreds") : string.Concat(" and", Tens(num3.ToString()), " Hundreds"));
                            }
                        }
                        if (amt.Substring(7, 2) != "00")
                        {
                            num4 = Convert.ToInt32(amt.Substring(7, 2));
                            str4 = (Convert.ToInt32(amt.Substring(7, 1)) == 0 ? string.Concat(" and ", Tens(num4.ToString())) : string.Concat(" and ", Word_Spell_Tens(num4.ToString())));
                        }
                    }
                }
            }
            else if (amt_paisa != "")
            {
                num = Convert.ToInt32(amt.Substring(0, 2));
                str = string.Concat(Word_Spell_Tens(num.ToString()), " Crores");
                if (amt.Substring(2, 7) != "0000000")
                {
                    if (amt.Substring(2, 2) != "00")
                    {
                        if (!(amt.Substring(2, 1) != "0"))
                        {
                            num1 = Convert.ToInt32(amt.Substring(3, 1));
                            str1 = (num1 <= 1 ? string.Concat(" ", Tens(num1.ToString()), " Lakh") : string.Concat(" ", Tens(num1.ToString()), " Lakhs"));
                        }
                        else
                        {
                            num1 = Convert.ToInt32(amt.Substring(2, 2));
                            str1 = string.Concat(" ", Word_Spell_Tens(num1.ToString()), " Lakhs");
                        }
                    }
                    if (amt.Substring(4, 2) != "00")
                    {
                        if (!(amt.Substring(4, 1) != "0"))
                        {
                            num2 = Convert.ToInt32(amt.Substring(5, 1));
                            str2 = (num2 <= 1 ? string.Concat(" ", Tens(num2.ToString()), " Thousand") : string.Concat(" ", Tens(num2.ToString()), " Thousands"));
                        }
                        else
                        {
                            num2 = Convert.ToInt32(amt.Substring(4, 2));
                            str2 = string.Concat(" ", Word_Spell_Tens(num2.ToString()), " Thousands");
                        }
                    }
                    if (amt.Substring(6, 3) != "000")
                    {
                        if (amt.Substring(6, 1) != "0")
                        {
                            num3 = Convert.ToInt32(amt.Substring(6, 1));
                            str3 = (num3 <= 1 ? string.Concat(" ", Tens(num3.ToString()), " Hundred") : string.Concat(" ", Tens(num3.ToString()), " Hundreds"));
                        }
                        if (amt.Substring(7, 2) != "00")
                        {
                            num4 = Convert.ToInt32(amt.Substring(7, 2));
                            str4 = (!(amt.Substring(7, 1) != "0") ? string.Concat(" ", Tens(num4.ToString())) : string.Concat(" ", Word_Spell_Tens(num4.ToString())));
                        }
                    }
                }
                if (amt_paisa.Substring(0, 2) != "00")
                {
                    str5 = (!(amt_paisa.Substring(0, 1) != "0") ? string.Concat(" ", Tens(amt_paisa.Substring(0, 2)), " Paisa") : string.Concat(" and ", Word_Spell_Tens(amt_paisa.Substring(0, 2)), " Paisa"));
                }
            }
            string[] strArrays = new string[] { "Taka ", str, str1, str2, str3, str4, str5, " Only" };
            return string.Concat(strArrays);
        }

        public static string F_Hundred(string amt, string amt_paisa)
        {
            string str = "";
            string str1 = "";
            string str2 = "";
            int num = 0;
            int num1 = 0;
            if (amt_paisa == "")
            {
                if (amt.Substring(0, 3) != "000")
                {
                    if (amt.Substring(0, 1) != "0")
                    {
                        num = Convert.ToInt32(amt.Substring(0, 1));
                        str = (num <= 1 ? string.Concat(Tens(num.ToString()), " Hundred") : string.Concat(Tens(num.ToString()), " Hundreds"));
                    }
                    if (amt.Substring(1, 2) != "00")
                    {
                        num1 = Convert.ToInt32(amt.Substring(1, 2));
                        str1 = (Convert.ToInt32(amt.Substring(1, 1)) == 0 ? string.Concat(" and ", Tens(num1.ToString())) : string.Concat(" and ", Word_Spell_Tens(num1.ToString())));
                    }
                }
            }
            else if (amt_paisa != "")
            {
                if (amt.Substring(0, 3) != "000")
                {
                    if (amt.Substring(0, 1) != "0")
                    {
                        num = Convert.ToInt32(amt.Substring(0, 1));
                        str = (num <= 1 ? string.Concat(Tens(num.ToString()), " Hundred") : string.Concat(Tens(num.ToString()), " Hundreds"));
                    }
                    if (amt.Substring(1, 2) != "00")
                    {
                        num1 = Convert.ToInt32(amt.Substring(1, 2));
                        str1 = (!(amt.Substring(1, 1) != "0") ? string.Concat(" ", Tens(num1.ToString())) : string.Concat(" ", Word_Spell_Tens(num1.ToString())));
                    }
                }
                if (amt_paisa.Substring(0, 2) != "00")
                {
                    str2 = (!(amt_paisa.Substring(0, 1) != "0") ? string.Concat(" ", Tens(amt_paisa.Substring(0, 2)), " Paisa") : string.Concat(" and ", Word_Spell_Tens(amt_paisa.Substring(0, 2)), " Paisa"));
                }
            }
            string[] strArrays = new string[] { "Taka ", str, str1, str2, " Only" };
            return string.Concat(strArrays);
        }

        public static string F_Lakh(string amt, string amt_paisa)
        {
            string str = "";
            string str1 = "";
            string str2 = "";
            string str3 = "";
            string str4 = "";
            int num = 0;
            int num1 = 0;
            int num2 = 0;
            int num3 = 0;
            if (amt_paisa == "")
            {
                if (amt.Substring(0, 1) != "0")
                {
                    num = Convert.ToInt32(amt.Substring(0, 1));
                    str = (num <= 1 ? string.Concat(Tens(num.ToString()), " Lakh") : string.Concat(Tens(num.ToString()), " Lakhs"));
                }
                if (amt.Substring(1, 2) != "00")
                {
                    if (!(amt.Substring(1, 1) != "0"))
                    {
                        num1 = Convert.ToInt32(amt.Substring(2, 1));
                        str1 = (!(amt.Substring(3, 3) == "000") ? string.Concat(" ", Tens(num1.ToString())) : string.Concat(" and ", Tens(num1.ToString())));
                        str1 = (num1 <= 1 ? string.Concat(str1, " Thousand") : string.Concat(str1, " Thousands"));
                    }
                    else
                    {
                        num1 = Convert.ToInt32(amt.Substring(1, 2));
                        str1 = (!(amt.Substring(3, 3) == "000") ? string.Concat(" ", Word_Spell_Tens(num1.ToString()), " Thousands") : string.Concat(" and ", Word_Spell_Tens(num1.ToString()), " Thousands"));
                    }
                }
                if (amt.Substring(3, 3) != "000")
                {
                    if (amt.Substring(3, 1) != "0")
                    {
                        num2 = Convert.ToInt32(amt.Substring(3, 1));
                        if (num2 <= 1)
                        {
                            str2 = (!(amt.Substring(4, 2) == "00") ? string.Concat(" ", Tens(num2.ToString()), " Hundred") : string.Concat(" and", Tens(num2.ToString()), " Hundred"));
                        }
                        else
                        {
                            str2 = (!(amt.Substring(4, 2) == "00") ? string.Concat(" ", Tens(num2.ToString()), " Hundreds") : string.Concat(" and", Tens(num2.ToString()), " Hundreds"));
                        }
                    }
                    if (amt.Substring(4, 2) != "00")
                    {
                        num3 = Convert.ToInt32(amt.Substring(4, 2));
                        str3 = (Convert.ToInt32(amt.Substring(4, 1)) == 0 ? string.Concat(" and ", Tens(num3.ToString())) : string.Concat(" and ", Word_Spell_Tens(num3.ToString())));
                    }
                }
            }
            else if (amt_paisa != "")
            {
                if (amt.Substring(0, 1) != "0")
                {
                    num = Convert.ToInt32(amt.Substring(0, 1));
                    str = (num <= 1 ? string.Concat(Tens(num.ToString()), " Lakh") : string.Concat(Tens(num.ToString()), " Lakhs"));
                }
                if (amt.Substring(1, 2) != "00")
                {
                    if (!(amt.Substring(1, 1) != "0"))
                    {
                        num1 = Convert.ToInt32(amt.Substring(2, 1));
                        str1 = (num1 <= 1 ? string.Concat(" ", Tens(num1.ToString()), " Thousand") : string.Concat(" ", Tens(num1.ToString()), " Thousands"));
                    }
                    else
                    {
                        num1 = Convert.ToInt32(amt.Substring(1, 2));
                        str1 = string.Concat(" ", Word_Spell_Tens(num1.ToString()), " Thousands");
                    }
                }
                if (amt.Substring(3, 3) != "000")
                {
                    if (amt.Substring(3, 1) != "0")
                    {
                        num2 = Convert.ToInt32(amt.Substring(3, 1));
                        str2 = (num2 <= 1 ? string.Concat(" ", Tens(num2.ToString()), " Hundred") : string.Concat(" ", Tens(num2.ToString()), " Hundreds"));
                    }
                    if (amt.Substring(4, 2) != "00")
                    {
                        num3 = Convert.ToInt32(amt.Substring(4, 2));
                        str3 = (!(amt.Substring(4, 1) != "0") ? string.Concat(" ", Tens(num3.ToString())) : string.Concat(" ", Word_Spell_Tens(num3.ToString())));
                    }
                }
                if (amt_paisa.Substring(0, 2) != "00")
                {
                    str4 = (!(amt_paisa.Substring(0, 1) != "0") ? string.Concat(" ", Tens(amt_paisa.Substring(0, 2)), " Paisa") : string.Concat(" and ", Word_Spell_Tens(amt_paisa.Substring(0, 2)), " Paisa"));
                }
            }
            string[] strArrays = new string[] { "Taka ", str, str1, str2, str3, str4, " Only" };
            return string.Concat(strArrays);
        }

        public static string F_Lakhs(string amt, string amt_paisa)
        {
            string str = "";
            string str1 = "";
            string str2 = "";
            string str3 = "";
            string str4 = "";
            int num = 0;
            int num1 = 0;
            int num2 = 0;
            int num3 = 0;
            if (amt_paisa == "")
            {
                if (amt.Substring(0, 2) != "00")
                {
                    if (amt.Substring(0, 1) != "0")
                    {
                        num = Convert.ToInt32(amt.Substring(0, 2));
                        str = string.Concat(Word_Spell_Tens(num.ToString()), " Lakhs");
                    }
                }
                if (amt.Substring(2, 2) != "00")
                {
                    if (!(amt.Substring(2, 1) != "0"))
                    {
                        num1 = Convert.ToInt32(amt.Substring(3, 1));
                        str1 = (!(amt.Substring(4, 3) == "000") ? string.Concat(" ", Tens(num1.ToString())) : string.Concat(" and ", Tens(num1.ToString())));
                        str1 = (num1 <= 1 ? string.Concat(str1, " Thousand") : string.Concat(str1, " Thousands"));
                    }
                    else
                    {
                        num1 = Convert.ToInt32(amt.Substring(2, 2));
                        str1 = (!(amt.Substring(4, 3) == "000") ? string.Concat(" ", Word_Spell_Tens(num1.ToString()), " Thousands") : string.Concat(" and ", Word_Spell_Tens(num1.ToString()), " Thousands"));
                    }
                }
                if (amt.Substring(4, 3) != "000")
                {
                    if (amt.Substring(4, 1) != "0")
                    {
                        num2 = Convert.ToInt32(amt.Substring(4, 1));
                        if (num2 <= 1)
                        {
                            str2 = (!(amt.Substring(5, 2) == "00") ? string.Concat(" ", Tens(num2.ToString()), " Hundred") : string.Concat(" and", Tens(num2.ToString()), " Hundred"));
                        }
                        else
                        {
                            str2 = (!(amt.Substring(5, 2) == "00") ? string.Concat(" ", Tens(num2.ToString()), " Hundreds") : string.Concat(" and", Tens(num2.ToString()), " Hundreds"));
                        }
                    }
                    if (amt.Substring(5, 2) != "00")
                    {
                        num3 = Convert.ToInt32(amt.Substring(5, 2));
                        str3 = (Convert.ToInt32(amt.Substring(5, 1)) == 0 ? string.Concat(" and ", Tens(num3.ToString())) : string.Concat(" and ", Word_Spell_Tens(num3.ToString())));
                    }
                }
            }
            else if (amt_paisa != "")
            {
                if (amt.Substring(0, 2) != "00")
                {
                    if (amt.Substring(0, 1) != "0")
                    {
                        num = Convert.ToInt32(amt.Substring(0, 2));
                        str = string.Concat(" ", Word_Spell_Tens(num.ToString()), " Lakhs");
                    }
                }
                if (amt.Substring(2, 2) != "00")
                {
                    if (!(amt.Substring(2, 1) != "0"))
                    {
                        num1 = Convert.ToInt32(amt.Substring(3, 1));
                        str1 = (num1 <= 1 ? string.Concat(" ", Tens(num1.ToString()), " Thousand") : string.Concat(" ", Tens(num1.ToString()), " Thousands"));
                    }
                    else
                    {
                        num1 = Convert.ToInt32(amt.Substring(2, 2));
                        str1 = string.Concat(" ", Word_Spell_Tens(num1.ToString()), " Thousands");
                    }
                }
                if (amt.Substring(4, 3) != "000")
                {
                    if (amt.Substring(4, 1) != "0")
                    {
                        num2 = Convert.ToInt32(amt.Substring(4, 1));
                        str2 = (num2 <= 1 ? string.Concat(" ", Tens(num2.ToString()), " Hundred") : string.Concat(" ", Tens(num2.ToString()), " Hundreds"));
                    }
                    if (amt.Substring(5, 2) != "00")
                    {
                        num3 = Convert.ToInt32(amt.Substring(5, 2));
                        str3 = (!(amt.Substring(5, 1) != "0") ? string.Concat(" ", Tens(num3.ToString())) : string.Concat(" ", Word_Spell_Tens(num3.ToString())));
                    }
                }
                if (amt_paisa.Substring(0, 2) != "00")
                {
                    str4 = (!(amt_paisa.Substring(0, 1) != "0") ? string.Concat(" ", Tens(amt_paisa.Substring(0, 2)), " Paisa") : string.Concat(" and ", Word_Spell_Tens(amt_paisa.Substring(0, 2)), " Paisa"));
                }
            }
            string[] strArrays = new string[] { "Taka ", str, str1, str2, str3, str4, " Only" };
            return string.Concat(strArrays);
        }

        public static string F_Number(string amt, string amt_paisa)
        {
            string str = "";
            string str1 = "";
            int num = 0;
            if (amt_paisa == "")
            {
                if (!(amt.Substring(0, 2) != "00"))
                {
                    str = " Zero ";
                }
                else
                {
                    num = Convert.ToInt32(amt.Substring(0, 2));
                    str = (Convert.ToInt32(amt.Substring(0, 1)) == 0 ? Tens(num.ToString()) : Word_Spell_Tens(num.ToString()));
                }
            }
            else if (amt_paisa != "")
            {
                if (!(amt.Substring(0, 2) != "00"))
                {
                    str = " Zero ";
                }
                else
                {
                    num = Convert.ToInt32(amt.Substring(0, 2));
                    str = (!(amt.Substring(0, 1) != "0") ? Tens(num.ToString()) : Word_Spell_Tens(num.ToString()));
                }
                if (amt_paisa.Substring(0, 2) != "00")
                {
                    str1 = (!(amt_paisa.Substring(0, 1) != "0") ? string.Concat(" ", Tens(amt_paisa.Substring(0, 2)), " Paisa") : string.Concat(" and ", Word_Spell_Tens(amt_paisa.Substring(0, 2)), " Paisa"));
                }
            }
            return string.Concat("Taka ", str, str1, " Only");
        }

        public static string F_Thousand(string amt, string amt_paisa)
        {
            string str = "";
            string str1 = "";
            string str2 = "";
            string str3 = "";
            int num = 0;
            int num1 = 0;
            int num2 = 0;
            if (amt_paisa == "")
            {
                if (amt.Substring(0, 1) != "0")
                {
                    num = Convert.ToInt32(amt.Substring(0, 1));
                    str = (num <= 1 ? string.Concat(Tens(num.ToString()), " Thousand") : string.Concat(Tens(num.ToString()), " Thousands"));
                }
                if (amt.Substring(1, 3) != "000")
                {
                    if (amt.Substring(1, 1) != "0")
                    {
                        num1 = Convert.ToInt32(amt.Substring(1, 1));
                        if (num1 <= 1)
                        {
                            str1 = (!(amt.Substring(2, 2) == "00") ? string.Concat(" ", Tens(num1.ToString()), " Hundred") : string.Concat(" and", Tens(num1.ToString()), " Hundred"));
                        }
                        else
                        {
                            str1 = (!(amt.Substring(2, 2) == "00") ? string.Concat(" ", Tens(num1.ToString()), " Hundreds") : string.Concat(" and", Tens(num1.ToString()), " Hundreds"));
                        }
                    }
                    if (amt.Substring(2, 2) != "00")
                    {
                        num2 = Convert.ToInt32(amt.Substring(2, 2));
                        str2 = (Convert.ToInt32(amt.Substring(2, 1)) == 0 ? string.Concat(" and ", Tens(num2.ToString())) : string.Concat(" and ", Word_Spell_Tens(num2.ToString())));
                    }
                }
            }
            else if (amt_paisa != "")
            {
                if (amt.Substring(0, 1) != "0")
                {
                    num = Convert.ToInt32(amt.Substring(0, 1));
                    str = (num <= 1 ? string.Concat(Tens(num.ToString()), " Thousand") : string.Concat(Tens(num.ToString()), " Thousands"));
                }
                if (amt.Substring(1, 3) != "000")
                {
                    if (amt.Substring(1, 1) != "0")
                    {
                        num1 = Convert.ToInt32(amt.Substring(1, 1));
                        str1 = (num1 <= 1 ? string.Concat(" ", Tens(num1.ToString()), " Hundred") : string.Concat(" ", Tens(num1.ToString()), " Hundreds"));
                    }
                    if (amt.Substring(2, 2) != "00")
                    {
                        num2 = Convert.ToInt32(amt.Substring(2, 2));
                        str2 = (!(amt.Substring(2, 1) != "0") ? string.Concat(" ", Tens(num2.ToString())) : string.Concat(" ", Word_Spell_Tens(num2.ToString())));
                    }
                }
                if (amt_paisa.Substring(0, 2) != "00")
                {
                    str3 = (!(amt_paisa.Substring(0, 1) != "0") ? string.Concat(" ", Tens(amt_paisa.Substring(0, 2)), " Paisa") : string.Concat(" and ", Word_Spell_Tens(amt_paisa.Substring(0, 2)), " Paisa"));
                }
            }
            string[] strArrays = new string[] { "Taka ", str, str1, str2, str3, " Only" };
            return string.Concat(strArrays);
        }

        public static string F_Thousands(string amt, string amt_paisa)
        {
            string str = "";
            string str1 = "";
            string str2 = "";
            string str3 = "";
            int num = 0;
            int num1 = 0;
            int num2 = 0;
            if (amt_paisa == "")
            {
                if (amt.Substring(0, 1) != "0")
                {
                    num = Convert.ToInt32(amt.Substring(0, 2));
                    str = string.Concat(Word_Spell_Tens(num.ToString()), " Thousands");
                }
                if (amt.Substring(2, 3) != "000")
                {
                    if (amt.Substring(2, 1) != "0")
                    {
                        num1 = Convert.ToInt32(amt.Substring(2, 1));
                        if (num1 <= 1)
                        {
                            str1 = (!(amt.Substring(3, 2) == "00") ? string.Concat(" ", Tens(num1.ToString()), " Hundred") : string.Concat(" and", Tens(num1.ToString()), " Hundred"));
                        }
                        else
                        {
                            str1 = (!(amt.Substring(3, 2) == "00") ? string.Concat(" ", Tens(num1.ToString()), " Hundreds") : string.Concat(" and", Tens(num1.ToString()), " Hundreds"));
                        }
                    }
                    if (amt.Substring(3, 2) != "00")
                    {
                        num2 = Convert.ToInt32(amt.Substring(3, 2));
                        str2 = (Convert.ToInt32(amt.Substring(3, 1)) == 0 ? string.Concat(" and ", Tens(num2.ToString())) : string.Concat(" and ", Word_Spell_Tens(num2.ToString())));
                    }
                }
            }
            else if (amt_paisa != "")
            {
                if (amt.Substring(0, 1) != "0")
                {
                    num = Convert.ToInt32(amt.Substring(0, 2));
                    str = (num <= 1 ? string.Concat(Word_Spell_Tens(num.ToString()), " Thousand") : string.Concat(Word_Spell_Tens(num.ToString()), " Thousands"));
                }
                if (amt.Substring(2, 3) != "000")
                {
                    if (amt.Substring(2, 1) != "0")
                    {
                        num1 = Convert.ToInt32(amt.Substring(2, 1));
                        str1 = (num1 <= 1 ? string.Concat(" ", Tens(num1.ToString()), " Hundred") : string.Concat(" ", Tens(num1.ToString()), " Hundreds"));
                    }
                    if (amt.Substring(3, 2) != "00")
                    {
                        num2 = Convert.ToInt32(amt.Substring(3, 2));
                        str2 = (!(amt.Substring(3, 1) != "0") ? string.Concat(" ", Tens(num2.ToString())) : string.Concat(" ", Word_Spell_Tens(num2.ToString())));
                    }
                }
                if (amt_paisa.Substring(0, 2) != "00")
                {
                    str3 = (!(amt_paisa.Substring(0, 1) != "0") ? string.Concat(" ", Tens(amt_paisa.Substring(0, 2)), " Paisa") : string.Concat(" and ", Word_Spell_Tens(amt_paisa.Substring(0, 2)), " Paisa"));
                }
            }
            string[] strArrays = new string[] { "Taka ", str, str1, str2, str3, " Only" };
            return string.Concat(strArrays);
        }

        public static string InWords(decimal amount)
        {
            string str = "";
            string str1 = "";
            string str2 = "";
            str = amount.ToString();
            int num = amount.ToString().IndexOf(".", 0);
            str1 = amount.ToString().Substring(num + 1);
            if (!(str == str1))
            {
                str = amount.ToString().Substring(0, amount.ToString().IndexOf(".", 0));
                str = str.Replace(",", "").ToString();
            }
            else
            {
                str1 = "";
            }
            switch (str.Length)
            {
                case 1:
                    {
                        str2 = F_Number(string.Concat("0", str), str1);
                        break;
                    }
                case 2:
                    {
                        str2 = F_Number(str, str1);
                        break;
                    }
                case 3:
                    {
                        str2 = F_Hundred(str, str1);
                        break;
                    }
                case 4:
                    {
                        str2 = F_Thousand(str, str1);
                        break;
                    }
                case 5:
                    {
                        str2 = F_Thousands(str, str1);
                        break;
                    }
                case 6:
                    {
                        str2 = F_Lakh(str, str1);
                        break;
                    }
                case 7:
                    {
                        str2 = F_Lakhs(str, str1);
                        break;
                    }
                case 8:
                    {
                        str2 = F_Crore(str, str1);
                        break;
                    }
                case 9:
                    {
                        str2 = F_Crores(str, str1);
                        break;
                    }
            }
            return str2;
        }

        public static string Tens(string s_amt)
        {
            string str;
            string str1 = "";
            string sAmt = s_amt;
            if (sAmt != null)
            {
                switch (sAmt)
                {
                    case "0":
                        {
                            str1 = "";
                            break;
                        }
                    case "1":
                        {
                            str1 = "One";
                            break;
                        }
                    case "2":
                        {
                            str1 = "Two";
                            break;
                        }
                    case "3":
                        {
                            str1 = "Three";
                            break;
                        }
                    case "4":
                        {
                            str1 = "Four";
                            break;
                        }
                    case "5":
                        {
                            str1 = "Five";
                            break;
                        }
                    case "6":
                        {
                            str1 = "Six";
                            break;
                        }
                    case "7":
                        {
                            str1 = "Seven";
                            break;
                        }
                    case "8":
                        {
                            str1 = "Eight";
                            break;
                        }
                    case "9":
                        {
                            str1 = "Nine";
                            break;
                        }
                    case "10":
                        {
                            str1 = "Ten";
                            break;
                        }
                    case "11":
                        {
                            str1 = "Eleven";
                            break;
                        }
                    case "12":
                        {
                            str1 = "Twelve";
                            break;
                        }
                    case "13":
                        {
                            str1 = "Thirteen";
                            break;
                        }
                    case "14":
                        {
                            str1 = "Forteen";
                            break;
                        }
                    case "15":
                        {
                            str1 = "Fifteen";
                            break;
                        }
                    case "16":
                        {
                            str1 = "Sixteen";
                            break;
                        }
                    case "17":
                        {
                            str1 = "Seventeen";
                            break;
                        }
                    case "18":
                        {
                            str1 = "Eighteen";
                            break;
                        }
                    case "19":
                        {
                            str1 = "Nineteen";
                            break;
                        }
                    case "20":
                        {
                            str1 = "Twenty";
                            break;
                        }
                    case "30":
                        {
                            str1 = "Thirty";
                            break;
                        }
                    case "40":
                        {
                            str1 = "Forty";
                            break;
                        }
                    case "50":
                        {
                            str1 = "Fifty";
                            break;
                        }
                    case "60":
                        {
                            str1 = "Sixty";
                            break;
                        }
                    case "70":
                        {
                            str1 = "Seventy";
                            break;
                        }
                    case "80":
                        {
                            str1 = "Eighty";
                            break;
                        }
                    case "90":
                        {
                            str1 = "Ninety";
                            break;
                        }
                    default:
                        {
                            str1 = "Nothing";
                            str = str1;
                            return str;
                        }
                }
            }
            else
            {
                str1 = "Nothing";
                str = str1;
                return str;
            }
            str = str1;
            return str;
        }

        public static string Word_Spell_Tens(string amt)
        {
            string str = null;
            string str1 = null;
            string str2 = null;
            int num = 0;
            num = Convert.ToInt32(amt.Substring(0, 2));
            if (num <= 20)
            {
                str2 = Tens(num.ToString());
            }
            else
            {
                str = string.Concat(amt.Substring(0, 1), "0");
                str1 = amt.Substring(1, 1);
                str2 = string.Concat(Tens(str), " ", Tens(str1));
            }
            return str2;
        }
        #endregion
    }
}
