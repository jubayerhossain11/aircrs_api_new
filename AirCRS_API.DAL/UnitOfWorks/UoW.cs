﻿using AirCRS_API.DAL.DBContext;
using AirCRS_API.DAL.Entities;
using AirCRS_API.DAL.GenericRepositories;
using AirCRS_API.DAL.UnitOfWorks;
using System.Linq;

namespace AirCRS_API.DAL.UnitOfWorks
{
    public class UoW : IUoW
    {
        private AirCRSDBContext DbContext { get; set; }

        public UoW()
        {
            DbContext = new AirCRSDBContext();
            this.DbContext.Database.CommandTimeout = 180;
        }
        public void ExecuteSP(string command, params object[] param)
        {
            DbContext.Database.ExecuteSqlCommand(command, param);
        }
        public dynamic ExecuteSPGetList<T>(string command, params object[] param)
        {
            var output = DbContext.Database.SqlQuery<T>(command, param);
            return output.ToList();
        }
        public IRepository<Accesslist> AccesslistRepository
        {
            get { return new Repository<Accesslist>(DbContext); }
        }

        public IRepository<AccessMapper> AccessMapperRepository
        {
            get { return new Repository<AccessMapper>(DbContext); }
        }

        public IRepository<AccessToken> AccessTokenRepository
        {
            get { return new Repository<AccessToken>(DbContext); }
        }

        public IRepository<AccountLog> AccountLogRepository
        {
            get { return new Repository<AccountLog>(DbContext); }
        }

        public IRepository<Airline> AirlineRepository
        {
            get { return new Repository<Airline>(DbContext); }
        }

        public IRepository<AirportSearchCode> AirportSearchCodeRepository
        {
            get { return new Repository<AirportSearchCode>(DbContext); }
        }

        public IRepository<ApiRequestLog> ApiRequestLogRepository
        {
            get { return new Repository<ApiRequestLog>(DbContext); }
        }

        public IRepository<CrsUser> CrsUserRepository
        {
            get { return new Repository<CrsUser>(DbContext); }
        }

        public IRepository<CrsUserLoginHistory> CrsUserLoginHistoryRepository
        {
            get { return new Repository<CrsUserLoginHistory>(DbContext); }
        }

        public IRepository<CrsUserPermission> CrsUserPermissionRepository
        {
            get { return new Repository<CrsUserPermission>(DbContext); }
        }

        public IRepository<CrsUserPermissionHistory> CrsUserPermissionHistoryRepository
        {
            get { return new Repository<CrsUserPermissionHistory>(DbContext); }
        }

        public IRepository<CrsUserType> CrsUserTypeRepository
        {
            get { return new Repository<CrsUserType>(DbContext); }
        }

        public IRepository<Passenger> PassengerRepository
        {
            get { return new Repository<Passenger>(DbContext); }
        }

        public IRepository<PmsGroup> PmsGroupRepository
        {
            get { return new Repository<PmsGroup>(DbContext); }
        }

        public IRepository<Pnr> PnrRepository
        {
            get { return new Repository<Pnr>(DbContext); }
        }

        public IRepository<PnrLog> PnrLogRepository
        {
            get { return new Repository<PnrLog>(DbContext); }
        }

        public IRepository<PriceBreakdown> PriceBreakdownRepository
        {
            get { return new Repository<PriceBreakdown>(DbContext); }
        }
        public IRepository<PriceBreakdownDetails> PriceBreakdownDetailsRepository
        {
            get { return new Repository<PriceBreakdownDetails>(DbContext); }
        }
        public IRepository<Segment> SegmentRepository
        {
            get { return new Repository<Segment>(DbContext); }
        }
        public IRepository<Ticket> TicketRepository
        {
            get { return new Repository<Ticket>(DbContext); }
        }
        public IRepository<Trip> TripRepository
        {
            get { return new Repository<Trip>(DbContext); }
        }
        public IRepository<Module> ModuleRepository
        {
            get { return new Repository<Module>(DbContext); }
        }

        public IRepository<ApplicationUser> ApplicationUserRepository
        {
            get { return new Repository<ApplicationUser>(DbContext); }
        }
        public IRepository<Baggage> BaggageRepository
        {
            get { return new Repository<Baggage>(DbContext); }
        }
        public IRepository<Meal> MealRepository
        {
            get { return new Repository<Meal>(DbContext); }
        }
        public IRepository<WheelChair> WheelChairRepository
        {
            get { return new Repository<WheelChair>(DbContext); }
        }

        public void Commit()
        {
            DbContext.SaveChanges();
        }
    }
}
