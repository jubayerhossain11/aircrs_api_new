﻿using AirCRS_API.DAL.DBContext;
using AirCRS_API.DAL.Entities;
using AirCRS_API.DAL.GenericRepositories;

namespace AirCRS_API.DAL.UnitOfWorks
{
    public interface IUoW
    {
        void ExecuteSP(string command, params object[] param);
        //dynamic ExecuteSPGetData(dynamic model, string command, params object[] param);
        dynamic ExecuteSPGetList<T>(string command, params object[] param);

        IRepository<Accesslist> AccesslistRepository { get; }
        IRepository<AccessMapper> AccessMapperRepository { get; }
        IRepository<AccessToken> AccessTokenRepository { get; }
        IRepository<AccountLog> AccountLogRepository { get; }
        IRepository<Airline> AirlineRepository { get; }
        IRepository<AirportSearchCode> AirportSearchCodeRepository { get; }
        IRepository<ApiRequestLog> ApiRequestLogRepository { get; }
        IRepository<CrsUser> CrsUserRepository { get; }
        IRepository<CrsUserLoginHistory> CrsUserLoginHistoryRepository { get; }
        IRepository<CrsUserPermission> CrsUserPermissionRepository { get; }
        IRepository<CrsUserPermissionHistory> CrsUserPermissionHistoryRepository { get; }
        IRepository<CrsUserType> CrsUserTypeRepository { get; }
        IRepository<Passenger> PassengerRepository { get; }
        IRepository<PmsGroup> PmsGroupRepository { get; }
        IRepository<Pnr> PnrRepository { get; }
        IRepository<PnrLog> PnrLogRepository { get; }
        IRepository<PriceBreakdown> PriceBreakdownRepository { get; }
        IRepository<PriceBreakdownDetails> PriceBreakdownDetailsRepository { get; }
        IRepository<Segment> SegmentRepository { get; }
        IRepository<Ticket> TicketRepository { get; }
        IRepository<Trip> TripRepository { get; }
        IRepository<Module> ModuleRepository { get; }
        IRepository<Baggage> BaggageRepository { get; }
        IRepository<Meal> MealRepository { get; }
        IRepository<WheelChair> WheelChairRepository { get; }
        
    }
}
