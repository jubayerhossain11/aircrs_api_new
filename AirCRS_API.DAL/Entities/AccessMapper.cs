﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirCRS_API.DAL.Entities
{
    public class AccessMapper
    {
        [Key]
        public int MapperID { get; set; }
        public string AccessList { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime CreateTime { get; set; }
        public byte RoleStatus { get; set; }
        public byte IsVisible { get; set; }
        public string CreatedBy { get; set; }
        public byte IsRemoved { get; set; }

        public int PmsGroupGroupID { get; set; }

        public virtual PmsGroup PmsGroup { get; set; }
    }
}
