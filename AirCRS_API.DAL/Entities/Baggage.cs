﻿using AirCRS_API.DAL.CommonEntities;
using AirCRS_API.DAL.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirCRS_API.DAL.Entities
{
    public class Baggage : AuditEntity
    {
        public long Id { get; set; }
        public BaggageType BaggageType { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
        public bool IsActive { get; set; }

        public long TripId { get; set; }
        public virtual Trip Trip { get; set; }
    }
}
