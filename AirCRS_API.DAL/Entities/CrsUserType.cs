﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirCRS_API.DAL.Entities
{
    public class CrsUserType
    {
        [Key]
        public int ApiUserTypeID { get; set; }
        public string UserType { get; set; }
        public string ApiDescription { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreatedBy { get; set; }
    }
}
