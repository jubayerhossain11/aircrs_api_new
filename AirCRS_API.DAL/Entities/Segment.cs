﻿using AirCRS_API.DAL.CommonEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirCRS_API.DAL.Entities
{
    public class Segment: AuditEntity
    {
        [Key]
        public long SegmentId { get; set; }
        public int SegmentType { get; set; }
        public long TripId { get; set; }
        public DateTime? DepartureDate { get; set; }
        public DateTime? ArrivalDate { get; set; }
        public int? ClassType { get; set; }
        public string SegmentOrigin { get; set; }
        public DateTime?TravelTime { get; set; }
        public bool IsRemoved { get; set; }
        public string Duration { get; set; }
        public int SegmentSerialNo { get; set; }
        public string FlightNumber { get; set; }
        public string AirLines { get; set; }
        public string Baggage { get; set; }

        public virtual Trip Trip { get; set; }
    }
}
