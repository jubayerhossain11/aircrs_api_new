﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirCRS_API.DAL.Entities
{
    public class CrsUser
    {
        public int Id { get; set; }
        public int? UserTypeID { get; set; }
        public string UserName { get; set; }
        public string LoginID { get; set; }
        public string UserPassword { get; set; }
        public string UserEmail { get; set; }
        public string UserMobileNo { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime CreateTime { get; set; }
        public string CreatedBy { get; set; }
        public byte UserStatus { get; set; }
        public bool IsRemoved { get; set; }
        public int CrsUserBaseCurrency { get; set; }
        public string IdentityUserID { get; set; }
        public string CrsName { get; set; }
        public string Address { get; set; }
        public string CrsLogoName { get; set; }
        public string BannerName { get; set; }
        public string LogoImageUrl { get; set; }
    }
}
