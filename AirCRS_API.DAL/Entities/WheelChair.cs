﻿using AirCRS_API.DAL.CommonEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirCRS_API.DAL.Entities
{
    public class WheelChair : AuditEntity
    {
        public long Id { get; set; }
        public bool IsAvailable { get; set; }
        public decimal Price { get; set; }
        public bool IsActive { get; set; }

        public long TripId { get; set; }
        public virtual Trip Trip { get; set; }
    }
}
