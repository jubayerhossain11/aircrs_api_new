﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirCRS_API.DAL.Entities
{
    public class AccessToken
    {
        public int Id { get; set; }
        public string Token { get; set; }
        public DateTime IssueDate { get; set; }
        public DateTime ExpireDate { get; set; }
        public bool IsExpire { get; set; }
        public string ExpiresIn { get; set; }
    }
}
