﻿using AirCRS_API.DAL.CommonEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirCRS_API.DAL.Entities
{
    public class Passenger : AuditEntity
    {
        [Key]
        public long PassengerId { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FrequentFlyerNumber { get; set; }
        public string Email { get; set; }
        public DateTime? DOB { get; set; }
        public string Gender { get; set; }
        public string Nationality { get; set; }
        public string PassportNumber { get; set; }
        public DateTime? PassportExpDate { get; set; }
        public string ContractNumber { get; set; }
        public bool IsRemoved { get; set; }
        public string PassengerCode { get; set; }
        public string PassengerType { get; set; }

        public bool IsWheelChairNeeded { get; set; }
        public long? MealId { get; set; }
        public long? BaggageId { get; set; }
        public long? WheelChairId { get; set; }

        public virtual ICollection<PnrLog> PnrLogs { get; set; }
        public virtual Meal Meal { get; set; }
        public virtual Baggage Baggage { get; set; }
        public virtual WheelChair WheelChair { get; set; }
    }
}
