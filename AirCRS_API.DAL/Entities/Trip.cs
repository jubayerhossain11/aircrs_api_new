﻿using AirCRS_API.DAL.CommonEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirCRS_API.DAL.Entities
{
    public class Trip: AuditEntity
    {
        [Key]
        public long TripId { get; set; }
        public string TripUniqueNo { get; set; }
        public int? JournayType { get; set; }
        public DateTime JournayOnWardDate { get; set; }
        public DateTime? JournayReturnDate { get; set; }
        public int AirLineId { get; set; }
        public string Origin { get; set; }
        public string Destination { get; set; }
        public int NoOfTickets { get; set; }
        public bool IsIndividual { get; set; }
        public int TicketStatus { get; set; }
        public int ClassType { get; set; }
        public string ClassCode { get; set; }
        public bool IsRemoved { get; set; }
        public bool IsPublic { get; set; }
        public bool DirectTicketSell { get; set; }
        public bool IsActive { get; set; }
        public int? InitialNoOfTickets { get; set; }
       
        public virtual ICollection<ApiRequestLog> ApiRequestLogs { get; set; }
        public virtual ICollection<Pnr> Pnrs { get; set; }
        public virtual ICollection<PriceBreakdown> PriceBreakdowns { get; set; }
        public virtual ICollection<Segment> Segments { get; set; }
        public virtual ICollection<Ticket> Tickets { get; set; }
    }
}
