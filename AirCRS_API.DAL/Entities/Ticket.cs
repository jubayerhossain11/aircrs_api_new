﻿using AirCRS_API.DAL.CommonEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirCRS_API.DAL.Entities
{
    public class Ticket : AuditEntity
    {
        [Key]
        public long TicketId { get; set; }
        public long TripId { get; set; }
        public string TicketNumber { get; set; }
        public string ConfirmationNumber { get; set; }
        public int TStatus { get; set; }
        public bool IsRemoved { get; set; }

        public virtual Trip Trip { get; set; }
    }
}
