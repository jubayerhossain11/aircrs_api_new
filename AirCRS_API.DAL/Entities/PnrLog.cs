﻿using AirCRS_API.DAL.CommonEntities;
using AirCRS_API.DAL.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirCRS_API.DAL.Entities
{
    public class PnrLog : AuditEntity
    {
        [Key]
        public long PnrLogId { get; set; }
        public long PnrId { get; set; }
        public int UsesFlag { get; set; }
        public string UsedBy { get; set; }
        public bool IsRemoved { get; set; }
        public long PriceBreakdownDetailsId { get; set; }
        public decimal TotalPrice { get; set; }
        public bool IsSeen { get; set; }
        public string TicketNumber { get; set; }
        public long PassengerId { get; set; }
        public BookingSourceType BookingSourceType { get; set; }

        public virtual PriceBreakdownDetails PriceBreakdownDetails { get; set; }
        public virtual Passenger Passenger { get; set; }
        public virtual Pnr Pnr { get; set; }
    }
}
