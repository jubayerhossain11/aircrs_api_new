﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirCRS_API.DAL.Entities
{
    public class Module
    {
        [Key]
        public int ModuleID { get; set; }
        public string ModuleName { get; set; }
        public string ModuleIcon { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime CreateTime { get; set; }
        public byte IsVisible { get; set; }
        public string CreatedBy { get; set; }
        public byte IsRemoved { get; set; }
        public int? MenueOrder { get; set; }
        public bool IsCrsUser { get; set; }
    }
}
