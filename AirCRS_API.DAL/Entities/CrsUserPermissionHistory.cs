﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirCRS_API.DAL.Entities
{
    public class CrsUserPermissionHistory
    {
        public int Id { get; set; }
        public int ApiUserID { get; set; }
        public string ApiPermissns { get; set; }
        public string ChangeBy { get; set; }
        public DateTime? ChangeDate { get; set; }
        public byte IsRemoved { get; set; }
    }
}
