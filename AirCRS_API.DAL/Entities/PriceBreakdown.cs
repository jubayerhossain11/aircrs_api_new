﻿using AirCRS_API.DAL.CommonEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirCRS_API.DAL.Entities
{
    public class PriceBreakdown : AuditEntity
    {
        public long Id { get; set; }
        public string PriceSlotName { get; set; }
        public int PassengerLimit { get; set; }
        public decimal SingleAdultPrice { get; set; }
        public string Currency { get; set; }
        public int PriceNo { get; set; }
        public long TripId { get; set; }
        public virtual Trip Trip { get; set; }
        public virtual ICollection<PriceBreakdownDetails> PriceBreakdownDetails { get; set; }
    }
}
