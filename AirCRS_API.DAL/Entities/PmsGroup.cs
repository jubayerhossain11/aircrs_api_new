﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirCRS_API.DAL.Entities
{
    public class PmsGroup
    {
        [Key]
        public long GroupID { get; set; }
        public string GroupName { get; set; }
        public string GroupDescription { get; set; }
        public System.DateTime CreateDate { get; set; }
        public System.DateTime CreateTime { get; set; }
        public string CreatedBy { get; set; }
        public byte GroupStatus { get; set; }
        public byte IsVisible { get; set; }
        public byte IsRemoved { get; set; }
        public int? CrsUserId { get; set; }
    }
}
