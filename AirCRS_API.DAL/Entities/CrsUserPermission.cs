﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirCRS_API.DAL.Entities
{
    public class CrsUserPermission
    {
        [Key]
        public int ApiUserID { get; set; }
        public string ApiPermissns { get; set; }
        public string AdminUserID { get; set; }
        public byte IsRemoved { get; set; }
        public int? PmsGroupId { get; set; }
    }
}
