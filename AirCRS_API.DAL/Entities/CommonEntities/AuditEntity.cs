﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AirCRS_API.DAL.CommonEntities
{
    public class AuditEntity
    {
        public DateTime CreatedDate { get; set; }
        [Required]
        [StringLength(200)]
        public string CreatedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        [StringLength(200)]
        public string ModifiedBy { get; set; }
        public bool IsRemove { get; set; }
    }
}
