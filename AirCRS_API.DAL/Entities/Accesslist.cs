﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirCRS_API.DAL.Entities
{
    public class Accesslist
    {
        [Key]
        public long AccessID { get; set; }
        public string ControllerName { get; set; }
        public string ActionName { get; set; }
        public string Mask { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime CreateTime { get; set; }
        public byte AccessStatus { get; set; }
        public byte IsVisible { get; set; }
        public string CreatedBy { get; set; }
        public byte IsRemoved { get; set; }
        public string IconClass { get; set; }
        public int BaseModule { get; set; }
        public int? BaseModuleIndex { get; set; }
    }
}
