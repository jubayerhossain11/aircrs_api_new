﻿using AirCRS_API.DAL.CommonEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirCRS_API.DAL.Entities
{
    public class PriceBreakdownDetails: AuditEntity
    {
        public long Id { get; set; }
        public long PriceBreakdownId { get; set; }
        public int PaxType { get; set; }
        public decimal? BaseFare { get; set; }
        public decimal? Tax { get; set; }
        public decimal? ServiceCharge { get; set; }
        public bool IsLive { get; set; }

        public virtual PriceBreakdown PriceBreakdown { get; set; }
    }
}
