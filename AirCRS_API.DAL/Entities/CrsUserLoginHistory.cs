﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirCRS_API.DAL.Entities
{
    public class CrsUserLoginHistory
    {
        public int Id { get; set; }
        public int LoginUserID { get; set; }
        public DateTime LoginDate { get; set; }
        public DateTime LoginTime { get; set; }
        public  DateTime? LogOutTime { get; set; }
        public string LoginIP { get; set; }
        public byte IsRemoved { get; set; }
    }
}
