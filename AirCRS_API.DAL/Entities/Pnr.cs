﻿using AirCRS_API.DAL.CommonEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirCRS_API.DAL.Entities
{
    public class Pnr : AuditEntity
    {
        [Key]
        public long PnrId { get; set; }
        public long TripId { get; set; }
        public string PnrValue { get; set; }
        public int? PnrCount { get; set; }
        public bool IsRemoved { get; set; }

        public virtual Trip Trip { get; set; }
        public virtual ICollection<PnrLog> PnrLogs { get; set; }
    }
}
