﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirCRS_API.DAL.Entities
{
    public class ApiRequestLog
    {
        [Key]
        public long ApiRequestLogId { get; set; }
        public long TripId { get; set; }
        public int? NumberOfAdult { get; set; }
        public int? NumberOfChild { get; set; }
        public int? NumberOfInfrant { get; set; }
        public decimal? TotalPrice { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool IsRemoved { get; set; }
        public int Status { get; set; }

        public virtual Trip Trip { get; set; }
    }
}
