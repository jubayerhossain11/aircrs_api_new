﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirCRS_API.DAL.Entities
{
    public class Airline
    {
        public int Id { get; set; }
        public string AriLineName { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime CreateTime { get; set; }
        public string CreatedBy { get; set; }
        public byte IsRemoved { get; set; }
        public string Code { get; set; }
    }
}
