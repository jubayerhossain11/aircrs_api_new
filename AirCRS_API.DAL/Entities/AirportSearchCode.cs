﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirCRS_API.DAL.Entities
{
    public class AirportSearchCode
    {
        [Key]
        public string AirportCode { get; set; }
        public string SearchString { get; set; }
    }
}
