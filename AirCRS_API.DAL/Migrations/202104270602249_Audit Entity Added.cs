namespace AirCRS_API.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AuditEntityAdded : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Trips", "IsRemove", c => c.Boolean(nullable: false));
            AddColumn("dbo.Pnrs", "IsRemove", c => c.Boolean(nullable: false));
            AddColumn("dbo.PnrLogs", "IsRemove", c => c.Boolean(nullable: false));
            AddColumn("dbo.Passengers", "IsRemove", c => c.Boolean(nullable: false));
            AddColumn("dbo.PriceBreakdownDetails", "CreatedDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.PriceBreakdowns", "CreatedDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.Segments", "IsRemove", c => c.Boolean(nullable: false));
            AddColumn("dbo.Tickets", "IsRemove", c => c.Boolean(nullable: false));
            AlterColumn("dbo.Trips", "CreatedBy", c => c.String(nullable: false, maxLength: 200));
            AlterColumn("dbo.Trips", "ModifiedBy", c => c.String(maxLength: 200));
            AlterColumn("dbo.Pnrs", "CreatedBy", c => c.String(nullable: false, maxLength: 200));
            AlterColumn("dbo.Pnrs", "ModifiedBy", c => c.String(maxLength: 200));
            AlterColumn("dbo.PnrLogs", "CreatedBy", c => c.String(nullable: false, maxLength: 200));
            AlterColumn("dbo.PnrLogs", "ModifiedBy", c => c.String(maxLength: 200));
            AlterColumn("dbo.Passengers", "CreatedBy", c => c.String(nullable: false, maxLength: 200));
            AlterColumn("dbo.Passengers", "ModifiedBy", c => c.String(maxLength: 200));
            AlterColumn("dbo.Passengers", "ModifiedDate", c => c.DateTime());
            AlterColumn("dbo.Segments", "CreatedBy", c => c.String(nullable: false, maxLength: 200));
            AlterColumn("dbo.Segments", "ModifiedBy", c => c.String(maxLength: 200));
            AlterColumn("dbo.Tickets", "CreatedBy", c => c.String(nullable: false, maxLength: 200));
            AlterColumn("dbo.Tickets", "ModifiedBy", c => c.String(maxLength: 200));
            DropColumn("dbo.PriceBreakdownDetails", "CreateDate");
            DropColumn("dbo.PriceBreakdowns", "CreateDate");
        }
        
        public override void Down()
        {
            AddColumn("dbo.PriceBreakdowns", "CreateDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.PriceBreakdownDetails", "CreateDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Tickets", "ModifiedBy", c => c.String());
            AlterColumn("dbo.Tickets", "CreatedBy", c => c.String());
            AlterColumn("dbo.Segments", "ModifiedBy", c => c.String());
            AlterColumn("dbo.Segments", "CreatedBy", c => c.String());
            AlterColumn("dbo.Passengers", "ModifiedDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Passengers", "ModifiedBy", c => c.String());
            AlterColumn("dbo.Passengers", "CreatedBy", c => c.String());
            AlterColumn("dbo.PnrLogs", "ModifiedBy", c => c.String());
            AlterColumn("dbo.PnrLogs", "CreatedBy", c => c.String());
            AlterColumn("dbo.Pnrs", "ModifiedBy", c => c.String());
            AlterColumn("dbo.Pnrs", "CreatedBy", c => c.String());
            AlterColumn("dbo.Trips", "ModifiedBy", c => c.String());
            AlterColumn("dbo.Trips", "CreatedBy", c => c.String());
            DropColumn("dbo.Tickets", "IsRemove");
            DropColumn("dbo.Segments", "IsRemove");
            DropColumn("dbo.PriceBreakdowns", "CreatedDate");
            DropColumn("dbo.PriceBreakdownDetails", "CreatedDate");
            DropColumn("dbo.Passengers", "IsRemove");
            DropColumn("dbo.PnrLogs", "IsRemove");
            DropColumn("dbo.Pnrs", "IsRemove");
            DropColumn("dbo.Trips", "IsRemove");
        }
    }
}
