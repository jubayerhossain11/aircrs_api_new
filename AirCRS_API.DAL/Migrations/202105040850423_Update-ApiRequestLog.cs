namespace AirCRS_API.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateApiRequestLog : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.ApiRequestLogs", "ModifiedDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ApiRequestLogs", "ModifiedDate", c => c.DateTime(nullable: false));
        }
    }
}
