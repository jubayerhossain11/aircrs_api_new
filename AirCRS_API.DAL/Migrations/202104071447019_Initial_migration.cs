namespace AirCRS_API.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial_migration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Accesslists",
                c => new
                    {
                        AccessID = c.Long(nullable: false, identity: true),
                        ControllerName = c.String(),
                        ActionName = c.String(),
                        Mask = c.String(),
                        CreateDate = c.DateTime(nullable: false),
                        CreateTime = c.DateTime(nullable: false),
                        AccessStatus = c.Byte(nullable: false),
                        IsVisible = c.Byte(nullable: false),
                        CreatedBy = c.String(),
                        IsRemoved = c.Byte(nullable: false),
                        IconClass = c.String(),
                        BaseModule = c.Int(nullable: false),
                        BaseModuleIndex = c.Int(),
                    })
                .PrimaryKey(t => t.AccessID);
            
            CreateTable(
                "dbo.AccessMappers",
                c => new
                    {
                        MapperID = c.Int(nullable: false, identity: true),
                        AccessList = c.String(),
                        CreateDate = c.DateTime(nullable: false),
                        CreateTime = c.DateTime(nullable: false),
                        RoleStatus = c.Byte(nullable: false),
                        IsVisible = c.Byte(nullable: false),
                        CreatedBy = c.String(),
                        IsRemoved = c.Byte(nullable: false),
                    })
                .PrimaryKey(t => t.MapperID);
            
            CreateTable(
                "dbo.AccessTokens",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Token = c.String(),
                        IssueDate = c.DateTime(nullable: false),
                        ExpireDate = c.DateTime(nullable: false),
                        IsExpire = c.Boolean(nullable: false),
                        ExpiresIn = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AccountLogs",
                c => new
                    {
                        TransactionID = c.Int(nullable: false, identity: true),
                        AccountID = c.Int(nullable: false),
                        TransactionDate = c.DateTime(nullable: false),
                        TransactionTime = c.DateTime(nullable: false),
                        TransactionRef = c.String(),
                        DebitBalance = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CreditBalance = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Attachment = c.String(),
                        Remarks = c.String(),
                        CreatedBy = c.String(),
                        IsRemoved = c.Byte(nullable: false),
                    })
                .PrimaryKey(t => t.TransactionID);
            
            CreateTable(
                "dbo.Airlines",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AriLineName = c.String(),
                        CreateDate = c.DateTime(nullable: false),
                        CreateTime = c.DateTime(nullable: false),
                        CreatedBy = c.String(),
                        IsRemoved = c.Byte(nullable: false),
                        Code = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AirportSearchCodes",
                c => new
                    {
                        AirportCode = c.String(nullable: false, maxLength: 128),
                        SearchString = c.String(),
                    })
                .PrimaryKey(t => t.AirportCode);
            
            CreateTable(
                "dbo.ApiRequestLogs",
                c => new
                    {
                        ApiRequestLogId = c.Int(nullable: false, identity: true),
                        TripId = c.Int(nullable: false),
                        NumberOfAdult = c.Int(),
                        NumberOfChild = c.Int(),
                        NumberOfInfrant = c.Int(),
                        TotalPrice = c.Decimal(precision: 18, scale: 2),
                        CreatedBy = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedBy = c.String(),
                        ModifiedDate = c.DateTime(nullable: false),
                        IsRemoved = c.Boolean(nullable: false),
                        Status = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ApiRequestLogId)
                .ForeignKey("dbo.Trips", t => t.TripId, cascadeDelete: true)
                .Index(t => t.TripId);
            
            CreateTable(
                "dbo.Trips",
                c => new
                    {
                        TripId = c.Int(nullable: false, identity: true),
                        JournayType = c.Int(),
                        JournayOnWardDate = c.DateTime(nullable: false),
                        JournayReturnDate = c.DateTime(),
                        AirLineId = c.Int(nullable: false),
                        Origin = c.String(),
                        Destination = c.String(),
                        NoOfTickets = c.Int(nullable: false),
                        IsIndividual = c.Boolean(nullable: false),
                        TicketStatus = c.Int(nullable: false),
                        ClassType = c.Int(nullable: false),
                        ClassCode = c.String(),
                        CreatedBy = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedBy = c.String(),
                        ModifiedDate = c.DateTime(),
                        IsRemoved = c.Boolean(nullable: false),
                        IsPublic = c.Boolean(nullable: false),
                        DirectTicketSell = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        InitialNoOfTickets = c.Int(),
                    })
                .PrimaryKey(t => t.TripId);
            
            CreateTable(
                "dbo.Pnrs",
                c => new
                    {
                        PnrId = c.Int(nullable: false, identity: true),
                        TripId = c.Int(nullable: false),
                        PnrValue = c.String(),
                        PnrCount = c.Int(),
                        CreatedBy = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedBy = c.String(),
                        ModifiedDate = c.DateTime(),
                        IsRemoved = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.PnrId)
                .ForeignKey("dbo.Trips", t => t.TripId, cascadeDelete: true)
                .Index(t => t.TripId);
            
            CreateTable(
                "dbo.PnrLogs",
                c => new
                    {
                        PnrLogId = c.Int(nullable: false, identity: true),
                        PnrId = c.Int(nullable: false),
                        UsesFlag = c.Int(nullable: false),
                        UsedBy = c.String(),
                        CreatedBy = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedBy = c.String(),
                        ModifiedDate = c.DateTime(),
                        IsRemoved = c.Boolean(nullable: false),
                        PriceBreckdownId = c.Int(nullable: false),
                        TotalPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsSeen = c.Boolean(nullable: false),
                        TicketNumber = c.String(),
                        PassengerId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.PnrLogId)
                .ForeignKey("dbo.Passengers", t => t.PassengerId, cascadeDelete: true)
                .ForeignKey("dbo.Pnrs", t => t.PnrId, cascadeDelete: true)
                .Index(t => t.PnrId)
                .Index(t => t.PassengerId);
            
            CreateTable(
                "dbo.Passengers",
                c => new
                    {
                        PassengerId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Email = c.String(),
                        DOB = c.DateTime(),
                        Gender = c.String(),
                        Nationality = c.String(),
                        PassportNumber = c.String(),
                        PassportExpDate = c.DateTime(),
                        ContractNumber = c.String(),
                        CreatedBy = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedBy = c.String(),
                        ModifiedDate = c.DateTime(nullable: false),
                        IsRemoved = c.Boolean(nullable: false),
                        PassengerCode = c.String(),
                    })
                .PrimaryKey(t => t.PassengerId);
            
            CreateTable(
                "dbo.PriceBreakdowns",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PriceSlotName = c.String(),
                        PassengerLimit = c.Int(nullable: false),
                        SingleAdultPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Currency = c.String(),
                        PriceNo = c.Int(nullable: false),
                        Trip_TripId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Trips", t => t.Trip_TripId)
                .Index(t => t.Trip_TripId);
            
            CreateTable(
                "dbo.PriceBreakdownDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PriceBreakdownId = c.Int(nullable: false),
                        PaxType = c.Int(nullable: false),
                        BaseFare = c.Int(),
                        Tax = c.Int(),
                        ServiceCharge = c.Int(),
                        IsLive = c.Boolean(nullable: false),
                        CreateDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(nullable: false, maxLength: 200),
                        ModifiedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 200),
                        IsRemove = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PriceBreakdowns", t => t.PriceBreakdownId, cascadeDelete: true)
                .Index(t => t.PriceBreakdownId);
            
            CreateTable(
                "dbo.Segments",
                c => new
                    {
                        SegmentId = c.Int(nullable: false, identity: true),
                        SegmentType = c.Int(nullable: false),
                        TripId = c.Int(nullable: false),
                        DepartureDate = c.DateTime(),
                        ArrivalDate = c.DateTime(),
                        ClassType = c.Int(),
                        SegmentOrigin = c.String(),
                        TravelTime = c.DateTime(),
                        CreatedBy = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedBy = c.String(),
                        ModifiedDate = c.DateTime(),
                        IsRemoved = c.Boolean(nullable: false),
                        Duration = c.String(),
                        SegmentSerialNo = c.Int(nullable: false),
                        FlightNumber = c.String(),
                        AirLines = c.String(),
                        Baggage = c.String(),
                    })
                .PrimaryKey(t => t.SegmentId)
                .ForeignKey("dbo.Trips", t => t.TripId, cascadeDelete: true)
                .Index(t => t.TripId);
            
            CreateTable(
                "dbo.Tickets",
                c => new
                    {
                        TicketId = c.Int(nullable: false, identity: true),
                        TripId = c.Int(nullable: false),
                        TicketNumber = c.String(),
                        ConfirmationNumber = c.String(),
                        TStatus = c.Int(nullable: false),
                        CreatedBy = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedBy = c.String(),
                        ModifiedDate = c.DateTime(),
                        IsRemoved = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.TicketId)
                .ForeignKey("dbo.Trips", t => t.TripId, cascadeDelete: true)
                .Index(t => t.TripId);
            
            CreateTable(
                "dbo.CrsUserLoginHistories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LoginUserID = c.Int(nullable: false),
                        LoginDate = c.DateTime(nullable: false),
                        LoginTime = c.DateTime(nullable: false),
                        LogOutTime = c.DateTime(),
                        LoginIP = c.String(),
                        IsRemoved = c.Byte(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.CrsUserPermissionHistories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ApiUserID = c.Int(nullable: false),
                        ApiPermissns = c.String(),
                        ChangeBy = c.String(),
                        ChangeDate = c.DateTime(),
                        IsRemoved = c.Byte(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.CrsUserPermissions",
                c => new
                    {
                        ApiUserID = c.Int(nullable: false, identity: true),
                        ApiPermissns = c.String(),
                        AdminUserID = c.String(),
                        IsRemoved = c.Byte(nullable: false),
                        PmsGroupId = c.Int(),
                    })
                .PrimaryKey(t => t.ApiUserID);
            
            CreateTable(
                "dbo.CrsUsers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserTypeID = c.Int(),
                        UserName = c.String(),
                        LoginID = c.String(),
                        UserPassword = c.String(),
                        UserEmail = c.String(),
                        UserMobileNo = c.String(),
                        CreateDate = c.DateTime(nullable: false),
                        CreateTime = c.DateTime(nullable: false),
                        CreatedBy = c.String(),
                        UserStatus = c.Byte(nullable: false),
                        IsRemoved = c.Boolean(nullable: false),
                        CrsUserBaseCurrency = c.Int(nullable: false),
                        IdentityUserID = c.String(),
                        CrsName = c.String(),
                        Address = c.String(),
                        CrsLogoName = c.String(),
                        BannerName = c.String(),
                        LogoImageUrl = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.CrsUserTypes",
                c => new
                    {
                        ApiUserTypeID = c.Int(nullable: false, identity: true),
                        UserType = c.String(),
                        ApiDescription = c.String(),
                        CreateDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(),
                    })
                .PrimaryKey(t => t.ApiUserTypeID);
            
            CreateTable(
                "dbo.Modules",
                c => new
                    {
                        ModuleID = c.Int(nullable: false, identity: true),
                        ModuleName = c.String(),
                        ModuleIcon = c.String(),
                        CreateDate = c.DateTime(nullable: false),
                        CreateTime = c.DateTime(nullable: false),
                        IsVisible = c.Byte(nullable: false),
                        CreatedBy = c.String(),
                        IsRemoved = c.Byte(nullable: false),
                        MenueOrder = c.Int(),
                        IsCrsUser = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ModuleID);
            
            CreateTable(
                "dbo.PmsGroups",
                c => new
                    {
                        GroupID = c.Int(nullable: false, identity: true),
                        GroupName = c.String(),
                        GroupDescription = c.String(),
                        CreateDate = c.DateTime(nullable: false),
                        CreateTime = c.DateTime(nullable: false),
                        CreatedBy = c.String(),
                        GroupStatus = c.Byte(nullable: false),
                        IsVisible = c.Byte(nullable: false),
                        IsRemoved = c.Byte(nullable: false),
                        CrsUserId = c.Int(),
                    })
                .PrimaryKey(t => t.GroupID);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        PmsGroupID = c.Int(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.Tickets", "TripId", "dbo.Trips");
            DropForeignKey("dbo.Segments", "TripId", "dbo.Trips");
            DropForeignKey("dbo.PriceBreakdowns", "Trip_TripId", "dbo.Trips");
            DropForeignKey("dbo.PriceBreakdownDetails", "PriceBreakdownId", "dbo.PriceBreakdowns");
            DropForeignKey("dbo.Pnrs", "TripId", "dbo.Trips");
            DropForeignKey("dbo.PnrLogs", "PnrId", "dbo.Pnrs");
            DropForeignKey("dbo.PnrLogs", "PassengerId", "dbo.Passengers");
            DropForeignKey("dbo.ApiRequestLogs", "TripId", "dbo.Trips");
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.Tickets", new[] { "TripId" });
            DropIndex("dbo.Segments", new[] { "TripId" });
            DropIndex("dbo.PriceBreakdownDetails", new[] { "PriceBreakdownId" });
            DropIndex("dbo.PriceBreakdowns", new[] { "Trip_TripId" });
            DropIndex("dbo.PnrLogs", new[] { "PassengerId" });
            DropIndex("dbo.PnrLogs", new[] { "PnrId" });
            DropIndex("dbo.Pnrs", new[] { "TripId" });
            DropIndex("dbo.ApiRequestLogs", new[] { "TripId" });
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.PmsGroups");
            DropTable("dbo.Modules");
            DropTable("dbo.CrsUserTypes");
            DropTable("dbo.CrsUsers");
            DropTable("dbo.CrsUserPermissions");
            DropTable("dbo.CrsUserPermissionHistories");
            DropTable("dbo.CrsUserLoginHistories");
            DropTable("dbo.Tickets");
            DropTable("dbo.Segments");
            DropTable("dbo.PriceBreakdownDetails");
            DropTable("dbo.PriceBreakdowns");
            DropTable("dbo.Passengers");
            DropTable("dbo.PnrLogs");
            DropTable("dbo.Pnrs");
            DropTable("dbo.Trips");
            DropTable("dbo.ApiRequestLogs");
            DropTable("dbo.AirportSearchCodes");
            DropTable("dbo.Airlines");
            DropTable("dbo.AccountLogs");
            DropTable("dbo.AccessTokens");
            DropTable("dbo.AccessMappers");
            DropTable("dbo.Accesslists");
        }
    }
}
