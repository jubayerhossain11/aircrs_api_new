namespace AirCRS_API.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update_PriceSegment_Entity : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.PriceBreakdowns", "Trip_TripId", "dbo.Trips");
            DropIndex("dbo.PriceBreakdowns", new[] { "Trip_TripId" });
            RenameColumn(table: "dbo.PriceBreakdowns", name: "Trip_TripId", newName: "TripId");
            AlterColumn("dbo.PriceBreakdowns", "TripId", c => c.Int(nullable: false));
            CreateIndex("dbo.PriceBreakdowns", "TripId");
            AddForeignKey("dbo.PriceBreakdowns", "TripId", "dbo.Trips", "TripId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PriceBreakdowns", "TripId", "dbo.Trips");
            DropIndex("dbo.PriceBreakdowns", new[] { "TripId" });
            AlterColumn("dbo.PriceBreakdowns", "TripId", c => c.Int());
            RenameColumn(table: "dbo.PriceBreakdowns", name: "TripId", newName: "Trip_TripId");
            CreateIndex("dbo.PriceBreakdowns", "Trip_TripId");
            AddForeignKey("dbo.PriceBreakdowns", "Trip_TripId", "dbo.Trips", "TripId");
        }
    }
}
