namespace AirCRS_API.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update_AccessMapper_Entity : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AccessMappers", "PmsGroupId", c => c.Int(nullable: false));
            AddColumn("dbo.AccessMappers", "PmsGroup_GroupID", c => c.Int());
            CreateIndex("dbo.AccessMappers", "PmsGroup_GroupID");
            AddForeignKey("dbo.AccessMappers", "PmsGroup_GroupID", "dbo.PmsGroups", "GroupID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AccessMappers", "PmsGroup_GroupID", "dbo.PmsGroups");
            DropIndex("dbo.AccessMappers", new[] { "PmsGroup_GroupID" });
            DropColumn("dbo.AccessMappers", "PmsGroup_GroupID");
            DropColumn("dbo.AccessMappers", "PmsGroupId");
        }
    }
}
