namespace AirCRS_API.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PnrLog_Entity_Updated : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PnrLogs", "BookingSourceType", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PnrLogs", "BookingSourceType");
        }
    }
}
