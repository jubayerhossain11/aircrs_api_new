namespace AirCRS_API.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_NewEntities_And_UpdateAll_Entities : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.AccessMappers", "PmsGroupGroupID", "dbo.PmsGroups");
            DropForeignKey("dbo.AccessMappers", "PmsGroup_GroupID", "dbo.PmsGroups");
            DropForeignKey("dbo.ApiRequestLogs", "TripId", "dbo.Trips");
            DropForeignKey("dbo.Baggages", "TripId", "dbo.Trips");
            DropForeignKey("dbo.Meals", "TripId", "dbo.Trips");
            DropForeignKey("dbo.WheelChairs", "TripId", "dbo.Trips");
            DropForeignKey("dbo.PriceBreakdowns", "TripId", "dbo.Trips");
            DropForeignKey("dbo.Pnrs", "TripId", "dbo.Trips");
            DropForeignKey("dbo.Segments", "TripId", "dbo.Trips");
            DropForeignKey("dbo.Tickets", "TripId", "dbo.Trips");
            DropForeignKey("dbo.PnrLogs", "PnrId", "dbo.Pnrs");
            DropForeignKey("dbo.PnrLogs", "PassengerId", "dbo.Passengers");
            DropForeignKey("dbo.PnrLogs", "PriceBreakdownDetailsId", "dbo.PriceBreakdownDetails");
            DropForeignKey("dbo.PriceBreakdownDetails", "PriceBreakdownId", "dbo.PriceBreakdowns");
            DropIndex("dbo.AccessMappers", new[] { "PmsGroupGroupID" });
            DropIndex("dbo.ApiRequestLogs", new[] { "TripId" });
            DropIndex("dbo.Pnrs", new[] { "TripId" });
            DropIndex("dbo.PnrLogs", new[] { "PnrId" });
            DropIndex("dbo.PnrLogs", new[] { "PriceBreakdownDetailsId" });
            DropIndex("dbo.PnrLogs", new[] { "PassengerId" });
            DropIndex("dbo.PriceBreakdownDetails", new[] { "PriceBreakdownId" });
            DropIndex("dbo.PriceBreakdowns", new[] { "TripId" });
            DropIndex("dbo.Segments", new[] { "TripId" });
            DropIndex("dbo.Tickets", new[] { "TripId" });
            DropPrimaryKey("dbo.PmsGroups");
            DropPrimaryKey("dbo.ApiRequestLogs");
            DropPrimaryKey("dbo.Trips");
            DropPrimaryKey("dbo.Pnrs");
            DropPrimaryKey("dbo.PnrLogs");
            DropPrimaryKey("dbo.Passengers");
            DropPrimaryKey("dbo.PriceBreakdownDetails");
            DropPrimaryKey("dbo.PriceBreakdowns");
            DropPrimaryKey("dbo.Segments");
            DropPrimaryKey("dbo.Tickets");
            CreateTable(
                "dbo.Baggages",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        BaggageType = c.Int(nullable: false),
                        Quantity = c.Int(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsActive = c.Boolean(nullable: false),
                        TripId = c.Long(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(nullable: false, maxLength: 200),
                        ModifiedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 200),
                        IsRemove = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Trips", t => t.TripId, cascadeDelete: false)
                .Index(t => t.TripId);
            
            CreateTable(
                "dbo.Meals",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Code = c.String(),
                        Name = c.String(),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsActive = c.Boolean(nullable: false),
                        TripId = c.Long(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(nullable: false, maxLength: 200),
                        ModifiedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 200),
                        IsRemove = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Trips", t => t.TripId, cascadeDelete: false)
                .Index(t => t.TripId);
            
            CreateTable(
                "dbo.WheelChairs",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        IsAvailable = c.Boolean(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsActive = c.Boolean(nullable: false),
                        TripId = c.Long(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(nullable: false, maxLength: 200),
                        ModifiedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 200),
                        IsRemove = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Trips", t => t.TripId, cascadeDelete: false)
                .Index(t => t.TripId);
            
            AddColumn("dbo.AccessMappers", "PmsGroup_GroupID", c => c.Long());
            AddColumn("dbo.Passengers", "Title", c => c.String());
            AddColumn("dbo.Passengers", "FirstName", c => c.String());
            AddColumn("dbo.Passengers", "LastName", c => c.String());
            AddColumn("dbo.Passengers", "FrequentFlyerNumber", c => c.String());
            AddColumn("dbo.Passengers", "PassengerType", c => c.String());
            AddColumn("dbo.Passengers", "MealId", c => c.Long(nullable: false));
            AddColumn("dbo.Passengers", "BaggageId", c => c.Long(nullable: false));
            AddColumn("dbo.Passengers", "WheelChairId", c => c.Long(nullable: false));
            AlterColumn("dbo.PmsGroups", "GroupID", c => c.Long(nullable: false, identity: true));
            AlterColumn("dbo.ApiRequestLogs", "ApiRequestLogId", c => c.Long(nullable: false, identity: true));
            AlterColumn("dbo.ApiRequestLogs", "TripId", c => c.Long(nullable: false));
            AlterColumn("dbo.Trips", "TripId", c => c.Long(nullable: false, identity: true));
            AlterColumn("dbo.Pnrs", "PnrId", c => c.Long(nullable: false, identity: true));
            AlterColumn("dbo.Pnrs", "TripId", c => c.Long(nullable: false));
            AlterColumn("dbo.PnrLogs", "PnrLogId", c => c.Long(nullable: false, identity: true));
            AlterColumn("dbo.PnrLogs", "PnrId", c => c.Long(nullable: false));
            AlterColumn("dbo.PnrLogs", "PriceBreakdownDetailsId", c => c.Long(nullable: false));
            AlterColumn("dbo.PnrLogs", "PassengerId", c => c.Long(nullable: false));
            AlterColumn("dbo.Passengers", "PassengerId", c => c.Long(nullable: false, identity: true));
            AlterColumn("dbo.PriceBreakdownDetails", "Id", c => c.Long(nullable: false, identity: true));
            AlterColumn("dbo.PriceBreakdownDetails", "PriceBreakdownId", c => c.Long(nullable: false));
            AlterColumn("dbo.PriceBreakdowns", "Id", c => c.Long(nullable: false, identity: true));
            AlterColumn("dbo.PriceBreakdowns", "TripId", c => c.Long(nullable: false));
            AlterColumn("dbo.Segments", "SegmentId", c => c.Long(nullable: false, identity: true));
            AlterColumn("dbo.Segments", "TripId", c => c.Long(nullable: false));
            AlterColumn("dbo.Tickets", "TicketId", c => c.Long(nullable: false, identity: true));
            AlterColumn("dbo.Tickets", "TripId", c => c.Long(nullable: false));
            AddPrimaryKey("dbo.PmsGroups", "GroupID");
            AddPrimaryKey("dbo.ApiRequestLogs", "ApiRequestLogId");
            AddPrimaryKey("dbo.Trips", "TripId");
            AddPrimaryKey("dbo.Pnrs", "PnrId");
            AddPrimaryKey("dbo.PnrLogs", "PnrLogId");
            AddPrimaryKey("dbo.Passengers", "PassengerId");
            AddPrimaryKey("dbo.PriceBreakdownDetails", "Id");
            AddPrimaryKey("dbo.PriceBreakdowns", "Id");
            AddPrimaryKey("dbo.Segments", "SegmentId");
            AddPrimaryKey("dbo.Tickets", "TicketId");
            CreateIndex("dbo.AccessMappers", "PmsGroup_GroupID");
            CreateIndex("dbo.ApiRequestLogs", "TripId");
            CreateIndex("dbo.Pnrs", "TripId");
            CreateIndex("dbo.PnrLogs", "PnrId");
            CreateIndex("dbo.PnrLogs", "PriceBreakdownDetailsId");
            CreateIndex("dbo.PnrLogs", "PassengerId");
            CreateIndex("dbo.Passengers", "MealId");
            CreateIndex("dbo.Passengers", "BaggageId");
            CreateIndex("dbo.Passengers", "WheelChairId");
            CreateIndex("dbo.PriceBreakdownDetails", "PriceBreakdownId");
            CreateIndex("dbo.PriceBreakdowns", "TripId");
            CreateIndex("dbo.Segments", "TripId");
            CreateIndex("dbo.Tickets", "TripId");
            AddForeignKey("dbo.Passengers", "BaggageId", "dbo.Baggages", "Id", cascadeDelete: false);
            AddForeignKey("dbo.Passengers", "MealId", "dbo.Meals", "Id", cascadeDelete: false);
            AddForeignKey("dbo.Passengers", "WheelChairId", "dbo.WheelChairs", "Id", cascadeDelete: false);
            AddForeignKey("dbo.AccessMappers", "PmsGroup_GroupID", "dbo.PmsGroups", "GroupID");
            AddForeignKey("dbo.ApiRequestLogs", "TripId", "dbo.Trips", "TripId", cascadeDelete: false);
            AddForeignKey("dbo.PriceBreakdowns", "TripId", "dbo.Trips", "TripId", cascadeDelete: false);
            AddForeignKey("dbo.Pnrs", "TripId", "dbo.Trips", "TripId", cascadeDelete: false);
            AddForeignKey("dbo.Segments", "TripId", "dbo.Trips", "TripId", cascadeDelete: false);
            AddForeignKey("dbo.Tickets", "TripId", "dbo.Trips", "TripId", cascadeDelete: false);
            AddForeignKey("dbo.PnrLogs", "PnrId", "dbo.Pnrs", "PnrId", cascadeDelete: false);
            AddForeignKey("dbo.PnrLogs", "PassengerId", "dbo.Passengers", "PassengerId", cascadeDelete: false);
            AddForeignKey("dbo.PnrLogs", "PriceBreakdownDetailsId", "dbo.PriceBreakdownDetails", "Id", cascadeDelete: false);
            AddForeignKey("dbo.PriceBreakdownDetails", "PriceBreakdownId", "dbo.PriceBreakdowns", "Id", cascadeDelete: false);
            DropColumn("dbo.Passengers", "Name");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Passengers", "Name", c => c.String());
            DropForeignKey("dbo.PriceBreakdownDetails", "PriceBreakdownId", "dbo.PriceBreakdowns");
            DropForeignKey("dbo.PnrLogs", "PriceBreakdownDetailsId", "dbo.PriceBreakdownDetails");
            DropForeignKey("dbo.PnrLogs", "PassengerId", "dbo.Passengers");
            DropForeignKey("dbo.PnrLogs", "PnrId", "dbo.Pnrs");
            DropForeignKey("dbo.Tickets", "TripId", "dbo.Trips");
            DropForeignKey("dbo.Segments", "TripId", "dbo.Trips");
            DropForeignKey("dbo.Pnrs", "TripId", "dbo.Trips");
            DropForeignKey("dbo.PriceBreakdowns", "TripId", "dbo.Trips");
            DropForeignKey("dbo.ApiRequestLogs", "TripId", "dbo.Trips");
            DropForeignKey("dbo.AccessMappers", "PmsGroup_GroupID", "dbo.PmsGroups");
            DropForeignKey("dbo.Passengers", "WheelChairId", "dbo.WheelChairs");
            DropForeignKey("dbo.WheelChairs", "TripId", "dbo.Trips");
            DropForeignKey("dbo.Passengers", "MealId", "dbo.Meals");
            DropForeignKey("dbo.Meals", "TripId", "dbo.Trips");
            DropForeignKey("dbo.Passengers", "BaggageId", "dbo.Baggages");
            DropForeignKey("dbo.Baggages", "TripId", "dbo.Trips");
            DropIndex("dbo.Tickets", new[] { "TripId" });
            DropIndex("dbo.Segments", new[] { "TripId" });
            DropIndex("dbo.PriceBreakdowns", new[] { "TripId" });
            DropIndex("dbo.PriceBreakdownDetails", new[] { "PriceBreakdownId" });
            DropIndex("dbo.WheelChairs", new[] { "TripId" });
            DropIndex("dbo.Meals", new[] { "TripId" });
            DropIndex("dbo.Baggages", new[] { "TripId" });
            DropIndex("dbo.Passengers", new[] { "WheelChairId" });
            DropIndex("dbo.Passengers", new[] { "BaggageId" });
            DropIndex("dbo.Passengers", new[] { "MealId" });
            DropIndex("dbo.PnrLogs", new[] { "PassengerId" });
            DropIndex("dbo.PnrLogs", new[] { "PriceBreakdownDetailsId" });
            DropIndex("dbo.PnrLogs", new[] { "PnrId" });
            DropIndex("dbo.Pnrs", new[] { "TripId" });
            DropIndex("dbo.ApiRequestLogs", new[] { "TripId" });
            DropIndex("dbo.AccessMappers", new[] { "PmsGroup_GroupID" });
            DropPrimaryKey("dbo.Tickets");
            DropPrimaryKey("dbo.Segments");
            DropPrimaryKey("dbo.PriceBreakdowns");
            DropPrimaryKey("dbo.PriceBreakdownDetails");
            DropPrimaryKey("dbo.Passengers");
            DropPrimaryKey("dbo.PnrLogs");
            DropPrimaryKey("dbo.Pnrs");
            DropPrimaryKey("dbo.Trips");
            DropPrimaryKey("dbo.ApiRequestLogs");
            DropPrimaryKey("dbo.PmsGroups");
            AlterColumn("dbo.Tickets", "TripId", c => c.Int(nullable: false));
            AlterColumn("dbo.Tickets", "TicketId", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.Segments", "TripId", c => c.Int(nullable: false));
            AlterColumn("dbo.Segments", "SegmentId", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.PriceBreakdowns", "TripId", c => c.Int(nullable: false));
            AlterColumn("dbo.PriceBreakdowns", "Id", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.PriceBreakdownDetails", "PriceBreakdownId", c => c.Int(nullable: false));
            AlterColumn("dbo.PriceBreakdownDetails", "Id", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.Passengers", "PassengerId", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.PnrLogs", "PassengerId", c => c.Int(nullable: false));
            AlterColumn("dbo.PnrLogs", "PriceBreakdownDetailsId", c => c.Int(nullable: false));
            AlterColumn("dbo.PnrLogs", "PnrId", c => c.Int(nullable: false));
            AlterColumn("dbo.PnrLogs", "PnrLogId", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.Pnrs", "TripId", c => c.Int(nullable: false));
            AlterColumn("dbo.Pnrs", "PnrId", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.Trips", "TripId", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.ApiRequestLogs", "TripId", c => c.Int(nullable: false));
            AlterColumn("dbo.ApiRequestLogs", "ApiRequestLogId", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.PmsGroups", "GroupID", c => c.Int(nullable: false, identity: true));
            DropColumn("dbo.Passengers", "WheelChairId");
            DropColumn("dbo.Passengers", "BaggageId");
            DropColumn("dbo.Passengers", "MealId");
            DropColumn("dbo.Passengers", "PassengerType");
            DropColumn("dbo.Passengers", "FrequentFlyerNumber");
            DropColumn("dbo.Passengers", "LastName");
            DropColumn("dbo.Passengers", "FirstName");
            DropColumn("dbo.Passengers", "Title");
            DropColumn("dbo.AccessMappers", "PmsGroup_GroupID");
            DropTable("dbo.WheelChairs");
            DropTable("dbo.Meals");
            DropTable("dbo.Baggages");
            AddPrimaryKey("dbo.Tickets", "TicketId");
            AddPrimaryKey("dbo.Segments", "SegmentId");
            AddPrimaryKey("dbo.PriceBreakdowns", "Id");
            AddPrimaryKey("dbo.PriceBreakdownDetails", "Id");
            AddPrimaryKey("dbo.Passengers", "PassengerId");
            AddPrimaryKey("dbo.PnrLogs", "PnrLogId");
            AddPrimaryKey("dbo.Pnrs", "PnrId");
            AddPrimaryKey("dbo.Trips", "TripId");
            AddPrimaryKey("dbo.ApiRequestLogs", "ApiRequestLogId");
            AddPrimaryKey("dbo.PmsGroups", "GroupID");
            CreateIndex("dbo.Tickets", "TripId");
            CreateIndex("dbo.Segments", "TripId");
            CreateIndex("dbo.PriceBreakdowns", "TripId");
            CreateIndex("dbo.PriceBreakdownDetails", "PriceBreakdownId");
            CreateIndex("dbo.PnrLogs", "PassengerId");
            CreateIndex("dbo.PnrLogs", "PriceBreakdownDetailsId");
            CreateIndex("dbo.PnrLogs", "PnrId");
            CreateIndex("dbo.Pnrs", "TripId");
            CreateIndex("dbo.ApiRequestLogs", "TripId");
            CreateIndex("dbo.AccessMappers", "PmsGroupGroupID");
            AddForeignKey("dbo.PriceBreakdownDetails", "PriceBreakdownId", "dbo.PriceBreakdowns", "Id", cascadeDelete: true);
            AddForeignKey("dbo.PnrLogs", "PriceBreakdownDetailsId", "dbo.PriceBreakdownDetails", "Id", cascadeDelete: true);
            AddForeignKey("dbo.PnrLogs", "PassengerId", "dbo.Passengers", "PassengerId", cascadeDelete: true);
            AddForeignKey("dbo.PnrLogs", "PnrId", "dbo.Pnrs", "PnrId", cascadeDelete: true);
            AddForeignKey("dbo.Tickets", "TripId", "dbo.Trips", "TripId", cascadeDelete: true);
            AddForeignKey("dbo.Segments", "TripId", "dbo.Trips", "TripId", cascadeDelete: true);
            AddForeignKey("dbo.Pnrs", "TripId", "dbo.Trips", "TripId", cascadeDelete: true);
            AddForeignKey("dbo.PriceBreakdowns", "TripId", "dbo.Trips", "TripId", cascadeDelete: true);
            AddForeignKey("dbo.WheelChairs", "TripId", "dbo.Trips", "TripId", cascadeDelete: true);
            AddForeignKey("dbo.Meals", "TripId", "dbo.Trips", "TripId", cascadeDelete: true);
            AddForeignKey("dbo.Baggages", "TripId", "dbo.Trips", "TripId", cascadeDelete: true);
            AddForeignKey("dbo.ApiRequestLogs", "TripId", "dbo.Trips", "TripId", cascadeDelete: true);
            AddForeignKey("dbo.AccessMappers", "PmsGroup_GroupID", "dbo.PmsGroups", "GroupID");
            AddForeignKey("dbo.AccessMappers", "PmsGroupGroupID", "dbo.PmsGroups", "GroupID", cascadeDelete: true);
        }
    }
}
