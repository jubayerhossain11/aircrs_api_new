namespace AirCRS_API.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Trip_Entity_Updated : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Trips", "TripUniqueNo", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Trips", "TripUniqueNo");
        }
    }
}
