namespace AirCRS_API.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update_PriceBreakDownDetails_Entity : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PnrLogs", "PriceBreakdownDetailsId", c => c.Int(nullable: false));
            CreateIndex("dbo.PnrLogs", "PriceBreakdownDetailsId");
            AddForeignKey("dbo.PnrLogs", "PriceBreakdownDetailsId", "dbo.PriceBreakdownDetails", "Id", cascadeDelete: false);
            DropColumn("dbo.PnrLogs", "PriceBreckdownId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.PnrLogs", "PriceBreckdownId", c => c.Int(nullable: false));
            DropForeignKey("dbo.PnrLogs", "PriceBreakdownDetailsId", "dbo.PriceBreakdownDetails");
            DropIndex("dbo.PnrLogs", new[] { "PriceBreakdownDetailsId" });
            DropColumn("dbo.PnrLogs", "PriceBreakdownDetailsId");
        }
    }
}
