namespace AirCRS_API.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update_AccessMapper_Entity2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.AccessMappers", "PmsGroup_GroupID", "dbo.PmsGroups");
            DropIndex("dbo.AccessMappers", new[] { "PmsGroup_GroupID" });
            RenameColumn(table: "dbo.AccessMappers", name: "PmsGroup_GroupID", newName: "PmsGroupGroupID");
            AlterColumn("dbo.AccessMappers", "PmsGroupGroupID", c => c.Int(nullable: false));
            CreateIndex("dbo.AccessMappers", "PmsGroupGroupID");
            AddForeignKey("dbo.AccessMappers", "PmsGroupGroupID", "dbo.PmsGroups", "GroupID", cascadeDelete: true);
            DropColumn("dbo.AccessMappers", "PmsGroupId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AccessMappers", "PmsGroupId", c => c.Int(nullable: false));
            DropForeignKey("dbo.AccessMappers", "PmsGroupGroupID", "dbo.PmsGroups");
            DropIndex("dbo.AccessMappers", new[] { "PmsGroupGroupID" });
            AlterColumn("dbo.AccessMappers", "PmsGroupGroupID", c => c.Int());
            RenameColumn(table: "dbo.AccessMappers", name: "PmsGroupGroupID", newName: "PmsGroup_GroupID");
            CreateIndex("dbo.AccessMappers", "PmsGroup_GroupID");
            AddForeignKey("dbo.AccessMappers", "PmsGroup_GroupID", "dbo.PmsGroups", "GroupID");
        }
    }
}
