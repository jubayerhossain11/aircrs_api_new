namespace AirCRS_API.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update_Passenger_Entity : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Passengers", "BaggageId", "dbo.Baggages");
            DropForeignKey("dbo.Passengers", "MealId", "dbo.Meals");
            DropForeignKey("dbo.Passengers", "WheelChairId", "dbo.WheelChairs");
            DropIndex("dbo.Passengers", new[] { "MealId" });
            DropIndex("dbo.Passengers", new[] { "BaggageId" });
            DropIndex("dbo.Passengers", new[] { "WheelChairId" });
            AddColumn("dbo.Passengers", "IsWheelChairNeeded", c => c.Boolean(nullable: false));
            AlterColumn("dbo.Passengers", "MealId", c => c.Long());
            AlterColumn("dbo.Passengers", "BaggageId", c => c.Long());
            AlterColumn("dbo.Passengers", "WheelChairId", c => c.Long());
            CreateIndex("dbo.Passengers", "MealId");
            CreateIndex("dbo.Passengers", "BaggageId");
            CreateIndex("dbo.Passengers", "WheelChairId");
            AddForeignKey("dbo.Passengers", "BaggageId", "dbo.Baggages", "Id");
            AddForeignKey("dbo.Passengers", "MealId", "dbo.Meals", "Id");
            AddForeignKey("dbo.Passengers", "WheelChairId", "dbo.WheelChairs", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Passengers", "WheelChairId", "dbo.WheelChairs");
            DropForeignKey("dbo.Passengers", "MealId", "dbo.Meals");
            DropForeignKey("dbo.Passengers", "BaggageId", "dbo.Baggages");
            DropIndex("dbo.Passengers", new[] { "WheelChairId" });
            DropIndex("dbo.Passengers", new[] { "BaggageId" });
            DropIndex("dbo.Passengers", new[] { "MealId" });
            AlterColumn("dbo.Passengers", "WheelChairId", c => c.Long(nullable: false));
            AlterColumn("dbo.Passengers", "BaggageId", c => c.Long(nullable: false));
            AlterColumn("dbo.Passengers", "MealId", c => c.Long(nullable: false));
            DropColumn("dbo.Passengers", "IsWheelChairNeeded");
            CreateIndex("dbo.Passengers", "WheelChairId");
            CreateIndex("dbo.Passengers", "BaggageId");
            CreateIndex("dbo.Passengers", "MealId");
            AddForeignKey("dbo.Passengers", "WheelChairId", "dbo.WheelChairs", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Passengers", "MealId", "dbo.Meals", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Passengers", "BaggageId", "dbo.Baggages", "Id", cascadeDelete: true);
        }
    }
}
