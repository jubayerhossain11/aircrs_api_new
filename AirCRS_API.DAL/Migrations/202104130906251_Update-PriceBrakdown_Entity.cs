namespace AirCRS_API.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdatePriceBrakdown_Entity : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PriceBreakdowns", "CreateDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.PriceBreakdowns", "CreatedBy", c => c.String(nullable: false, maxLength: 200));
            AddColumn("dbo.PriceBreakdowns", "ModifiedDate", c => c.DateTime());
            AddColumn("dbo.PriceBreakdowns", "ModifiedBy", c => c.String(maxLength: 200));
            AddColumn("dbo.PriceBreakdowns", "IsRemove", c => c.Boolean(nullable: false));
            AlterColumn("dbo.PriceBreakdownDetails", "BaseFare", c => c.Decimal(precision: 18, scale: 2));
            AlterColumn("dbo.PriceBreakdownDetails", "Tax", c => c.Decimal(precision: 18, scale: 2));
            AlterColumn("dbo.PriceBreakdownDetails", "ServiceCharge", c => c.Decimal(precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.PriceBreakdownDetails", "ServiceCharge", c => c.Int());
            AlterColumn("dbo.PriceBreakdownDetails", "Tax", c => c.Int());
            AlterColumn("dbo.PriceBreakdownDetails", "BaseFare", c => c.Int());
            DropColumn("dbo.PriceBreakdowns", "IsRemove");
            DropColumn("dbo.PriceBreakdowns", "ModifiedBy");
            DropColumn("dbo.PriceBreakdowns", "ModifiedDate");
            DropColumn("dbo.PriceBreakdowns", "CreatedBy");
            DropColumn("dbo.PriceBreakdowns", "CreateDate");
        }
    }
}
