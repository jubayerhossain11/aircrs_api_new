﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirCRS_API.DAL.Enums
{
    public enum PnrUsesType
    {
        //booked means ticket not added
        Booked = 1,
        //Ticketed means ticket added
        Ticketed = 2,
        Cancel = 3,
    }
}
