﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirCRS_API.DAL.Enums
{
    public enum BookingSourceType
    {
        CRS_Portal = 1,
        Admin_Portal = 2,
        Api = 3
    }
}
