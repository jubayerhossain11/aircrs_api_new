﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirCRS_API.DAL.Enums
{
    public enum ApiRequestLogStatus
    {
        Search = 0,
        CheckingPrice = 1,
        CheckPriceError = 2,
        Booking = 3,
        BookingCanceled = 4,
        Ticketing = 5,
        TicketingCancel = 6,
        Success = 7
    }
}
