﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirCRS_API.DAL.Enums
{
    public enum BaggageType
    {
        Piece = 1,
        KG = 2
    }
}
