﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirCRS_API.DAL.Enums
{
    public enum AmountType
    {
        Percentage = 1,
        Flat = 2
    }
}
