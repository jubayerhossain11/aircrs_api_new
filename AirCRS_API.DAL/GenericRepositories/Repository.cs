﻿using AirCRS_API.DAL.DBContext;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace AirCRS_API.DAL.GenericRepositories
{
    public class Repository<T> : IRepository<T> where T : class
    {
        private IDbSet<T> dbEntity;
        private AirCRSDBContext _context;

        public Repository(AirCRSDBContext context)
        {
            this._context = context;
            dbEntity = _context.Set<T>();
        }

        #region Generics

        public virtual IEnumerable<T> Get(
           Expression<Func<T, bool>> filter = null,
           Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
           string includeProperties = "")
        {
            IQueryable<T> query = dbEntity;

            if (filter != null)
            {
                query = query.Where(filter);
            }

            foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            if (orderBy != null)
            {
                return orderBy(query).ToList();
            }
            else
            {
                return query.ToList();
            }
        }


        public void Delete(dynamic id)
        {
            T model = dbEntity.Find(id);
            dbEntity.Remove(model);
        }

        public IEnumerable<T> GetAll()
        {
            return dbEntity.ToList();
        }

        public IDbSet<T> GetDbSet()
        {
            return dbEntity;
        }

        public IQueryable<T> GetAllIQueryable()
        {
            return dbEntity.AsQueryable();
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await dbEntity.ToListAsync();
        }
        public T GetByID(dynamic id)
        {
            return dbEntity.Find(id);
        }

        public T GetByDoubleID(dynamic id1, dynamic id2)
        {
            return dbEntity.Find(id1, id2);
        }


        public void Insert(T model)
        {
            try
            {
                dbEntity.Add(model);

            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }

        }

        public void Update(T model)
        {
            dbEntity.Attach(model);
            _context.Entry(model).State = EntityState.Modified;
        }

        public bool IsExist(dynamic id)
        {
            try
            {
                var ty = dbEntity.Find(id);
                if (ty != null)
                {
                    return true;
                }
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool IsExist(dynamic id1, dynamic id2)
        {
            try
            {
                var ty = dbEntity.Find(id1, id2);
                if (ty != null)
                {
                    return true;
                }
                return false;
            }
            catch (Exception)
            {
                return false;
            }


        }

        T IRepository<T>.GetByDoubleID(dynamic id1, dynamic id2)
        {
            return dbEntity.Find(id1, id2);
        }

        public void Save()
        {
            try
            {
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #region AsyncFunction
        public virtual async Task<T> FindAsync(Expression<Func<T, bool>> match)
        {
            return await _context.Set<T>().SingleOrDefaultAsync(match);
        }

        //public async Task<ICollection<T>> FindAllAsync(Expression<Func<T, bool>> match)
        //{
        //    return await _context.Set<T>().Where(match).ToListAsync();
        //}

        public async Task<ICollection<T>> FindAllAsync()
        {
            return await _context.Set<T>().ToListAsync();
        }

        public virtual async Task<int> DeleteAsyn(T entity)
        {
            _context.Set<T>().Remove(entity);
            return await _context.SaveChangesAsync();
        }

        public async virtual Task<int> SaveAsync()
        {
            return await _context.SaveChangesAsync();
        }

        public IQueryable<T> GetAllIncluding(params Expression<Func<T, object>>[] includeProperties)
        {

            IQueryable<T> queryable = GetAllIQueryable();
            foreach (Expression<Func<T, object>> includeProperty in includeProperties)
            {
                queryable = queryable.Include<T, object>(includeProperty);
            }

            return queryable;
        }

        public virtual async Task<T> UpdateAsyn(T t, object key)
        {
            if (t == null)
                return null;
            T exist = await _context.Set<T>().FindAsync(key);
            if (exist != null)
            {
                _context.Entry(exist).CurrentValues.SetValues(t);
                await _context.SaveChangesAsync();
            }
            return exist;
        }
        #endregion




    }
}
