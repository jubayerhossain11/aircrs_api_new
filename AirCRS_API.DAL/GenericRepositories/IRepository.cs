﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace AirCRS_API.DAL.GenericRepositories
{
    public interface IRepository<T> where T : class
    {
        IEnumerable<T> GetAll();
        IDbSet<T> GetDbSet();
        IQueryable<T> GetAllIQueryable();
        Task<IEnumerable<T>> GetAllAsync();
        // object GetAllForJoin(T model);
        T GetByID(dynamic id);
        // T GetByStringID(dynamic id);
        T GetByDoubleID(dynamic id1, dynamic id2);
        IEnumerable<T> Get(
           Expression<Func<T, bool>> filter = null,
           Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
           string includeProperties = "");
        void Insert(T model);

        void Update(T model);

        void Delete(dynamic id);

        bool IsExist(dynamic id);

        bool IsExist(dynamic id1, dynamic id2);

        //T FindBy(Expression<Func<T, bool>> predicate);
        void Save();
        void Dispose();


       Task<ICollection<T>> FindAllAsync();
    }
}
