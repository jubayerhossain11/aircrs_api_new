﻿
using AirCRS_API.DAL.Entities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;

namespace AirCRS_API.DAL.DBContext
{
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager, string authenticationType)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            //var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            // Add custom user claims here
            //userIdentity.AddClaim(new Claim("TenantId", this.TenantId.ToString()));

            //userIdentity.AddClaim(new Claim("TenantId", TenantId == null ? "" : TenantId.ToString()));
            //userIdentity.AddClaim(new Claim("BranchMasterId", BranchMasterId == null ? "" : BranchMasterId.ToString()));
            //userIdentity.AddClaim(new Claim("LoginUserType", Convert.ToInt32(LoginUserType).ToString()));
            return userIdentity;

        }
        public int PmsGroupID { get; set; }
        public bool IsActive { get; set; }
    }
    public class AirCRSDBContext : IdentityDbContext<ApplicationUser>
    {
        public AirCRSDBContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public virtual DbSet<Accesslist> Accesslists { get; set; }
        public virtual DbSet<AccessMapper> AccessMappers { get; set; }
        public virtual DbSet<AccessToken> AccessTokens { get; set; }
        public virtual DbSet<AccountLog> AccountLogs { get; set; }
        public virtual DbSet<Airline> Airlines { get; set; }
        public virtual DbSet<AirportSearchCode> AirportSearchCodes { get; set; }
        public virtual DbSet<ApiRequestLog> ApiRequestLogs { get; set; }
        public virtual DbSet<CrsUser> CrsUsers { get; set; }
        public virtual DbSet<CrsUserLoginHistory> CrsUserLoginHistories { get; set; }
        public virtual DbSet<CrsUserPermission> CrsUserPermissions { get; set; }
        public virtual DbSet<CrsUserPermissionHistory> CrsUserPermissionHistories { get; set; }
        public virtual DbSet<CrsUserType> CrsUserTypes { get; set; }
        public virtual DbSet<Passenger> Passengers { get; set; }
        public virtual DbSet<PmsGroup> PmsGroups { get; set; }
        public virtual DbSet<Pnr> Pnrs { get; set; }
        public virtual DbSet<PnrLog> PnrLogs { get; set; }
        public virtual DbSet<PriceBreakdown> PriceBreakdowns { get; set; }
        public virtual DbSet<PriceBreakdownDetails> PriceBreakdownDetails { get; set; }
        public virtual DbSet<Segment> Segments { get; set; }
        public virtual DbSet<Ticket> Tickets { get; set; }
        public virtual DbSet<Trip> Trips { get; set; }
        public virtual DbSet<Module> Modules { get; set; }
        public virtual DbSet<Baggage> Baggages { get; set; }
        public virtual DbSet<Meal> Meals { get; set; }
        public virtual DbSet<WheelChair> WheelChairs { get; set; }

        public static AirCRSDBContext Create()
        {
            return new AirCRSDBContext();
        }
    }
}
