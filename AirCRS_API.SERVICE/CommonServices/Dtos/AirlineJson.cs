﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirCRS_API.SERVICE.CommonServices.Dtos
{
    public class AirlineJson
    {
        public string AirPortCode { get; set; }
        public string TimeZone { get; set; }
    }
}
