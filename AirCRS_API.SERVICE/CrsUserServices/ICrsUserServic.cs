﻿using AirCRS_API.SERVICE.CrsUserServices.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirCRS_API.SERVICE.CrsUserServices
{
    public interface ICrsUserServic
    {
        CrsUserOutputDto GetCrsUserById(CrsUserOutputDto ReqApi);
    }
}
