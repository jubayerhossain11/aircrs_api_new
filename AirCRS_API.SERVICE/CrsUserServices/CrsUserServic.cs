﻿using AirCRS_API.DAL.DBContext;
using AirCRS_API.DAL.Entities;
using AirCRS_API.DAL.UnitOfWorks;
using AirCRS_API.SERVICE.CrsUserServices.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirCRS_API.SERVICE.CrsUserServices
{
    public class CrsUserServic: ICrsUserServic
    {
        private readonly IUoW _uow;
        public CrsUserServic(IUoW uow)
        {
            _uow = uow;
        }
        public CrsUserOutputDto GetCrsUserById(CrsUserOutputDto ReqApi)
        {
            CrsUserOutputDto objOfViewModel = new CrsUserOutputDto();

            CrsUser api = _uow.CrsUserRepository.Get(x=>x.Id == ReqApi.Id).FirstOrDefault();


            objOfViewModel.UserTypeID = api.UserTypeID;
            objOfViewModel.CreateDate = api.CreateDate;
            //var currency = unit.CrossCurrencyRepository.GetByID(api.ApiUserBaseCurrency);

            objOfViewModel.Id = api.Id;
            objOfViewModel.LoginID = api.LoginID;
            //objOfViewModel.UserDomain = api.UserDomain;
            objOfViewModel.UserEmail = api.UserEmail;
            //objOfViewModel.UserID = api.UserID;
            //objOfViewModel.UserIP = api.UserIP;
            objOfViewModel.UserMobileNo = api.UserMobileNo;


            objOfViewModel.CrsName = api.CrsName;
            objOfViewModel.LogoImageUrl = api.LogoImageUrl;


            objOfViewModel.UserName = api.UserName;

            objOfViewModel.BannerName = api.BannerName;
            objOfViewModel.Address = api.Address;
            objOfViewModel.IdentityUserID = api.IdentityUserID;

            return objOfViewModel;
        }
       
    }
}
