﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirCRS_API.SERVICE.CrsUserServices.Dtos
{
    public class CrsUserOutputDto
    {

        public int? Id { get; set; }
        public string UserName { get; set; }
        public string LoginID { get; set; }
        public string UserPassword { get; set; }
        public string UserEmail { get; set; }
        public string UserMobileNo { get; set; }
        public System.DateTime CreateDate { get; set; }
        public System.DateTime CreateTime { get; set; }
        public string CreatedBy { get; set; }
        public byte UserStatus { get; set; }

        public string IdentityUserID { get; set; }

        public Nullable<int> UserTypeID { get; set; }

        public string CrsLogoName { get; set; }
        public string Address { get; set; }
        public string BannerName { get; set; }
        public string CrsName { get; set; }
        public string LogoImageUrl { get; set; }

        //public int AgentID { get; set; }
        //public string AgentName { get; set; }
        //public string LoginName { get; set; }
        //public string LoginPassword { get; set; }
        //public string UserEmail { get; set; }
        //public string UserMobileNo { get; set; }
        //public System.DateTime CreateDate { get; set; }
        //public System.DateTime CreateTime { get; set; }
        //public string CreatedBy { get; set; }
        //public byte UserStatus { get; set; }
        //public byte IsRemoved { get; set; }
        //public int? CrsUserBaseCurrency { get; set; }
        //public string CurrencyName { get; set; }
        //public string CurrencyCode { get; set; }
        //public string CurrencySymbol { get; set; }
        //public bool? SearchCheck { get; set; }
        //public int? UserTypeID { get; set; }
        //public bool? GatwayChargeApplicable { get; set; }


        //public decimal UserNetBalance { get; set; }
        //public decimal LoanBalance { get; set; }
        //public decimal FlightMarkup { get; set; }
        //public decimal FlightCommission { get; set; }
        //public decimal HotelMarkup { get; set; }
        //public decimal HotelCommission { get; set; }
    }
}
