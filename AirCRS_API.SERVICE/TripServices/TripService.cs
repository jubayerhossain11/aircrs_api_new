﻿using AirCRS_API.DAL.DBContext;
using AirCRS_API.DAL.Entities;
using AirCRS_API.DAL.Enums;
using AirCRS_API.DAL.UnitOfWorks;
using AirCRS_API.SERVICE.CommonServices.Dtos;
using AirCRS_API.SERVICE.TripServices.Dtos;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace AirCRS_API.SERVICE.TripServices
{
    public class TripService : ITripService
    {
        private readonly IUoW _uow;
        public TripService(IUoW uow)
        {
            _uow = uow;
        }

        public List<Trip> GetAll()
        {
            return _uow.TripRepository.GetAll().ToList();
        }
        public List<TripVM> GetTripList(int? pmsGroupId, string loggedinUserId, string IdentityUserID, bool isAdmin, DateTime? startDate, DateTime? endDate, bool MyTripOnly)
        {
            var tripList = (from t in _uow.TripRepository.GetAll().Where(x => x.IsRemoved == false)
                            join a in _uow.AirlineRepository.GetAll()
                            on t.AirLineId equals a.Id
                            join ts in _uow.AirportSearchCodeRepository.GetAll()
                            on t.Origin equals ts.AirportCode
                            join td in _uow.AirportSearchCodeRepository.GetAll()
                            on t.Destination equals td.AirportCode
                            join s in _uow.SegmentRepository.GetAll().Where(x => x.IsRemoved == false)
                            on t.TripId equals s.TripId into segmentTemp
                            join pnr in _uow.PnrRepository.GetAll().Where(x => x.IsRemoved == false)
                            on t.TripId equals pnr.TripId into pnrTemp
                            join pb in _uow.PriceBreakdownRepository.GetAll()
                            on t.TripId equals pb.TripId into priceBTemp
                            join crsUser in _uow.CrsUserRepository.GetAll()
                            on t.CreatedBy equals crsUser.IdentityUserID
                            orderby t.TripId descending
                            where (pmsGroupId == 2 ? (t.CreatedBy == loggedinUserId || t.IsPublic) : true) &&
                                   (isAdmin && !string.IsNullOrEmpty(IdentityUserID) ? t.CreatedBy == IdentityUserID : true) &&
                                   (!isAdmin && MyTripOnly ? t.CreatedBy == loggedinUserId : true) &&
                                   (startDate != null && endDate != null ? t.JournayOnWardDate >= startDate && t.JournayOnWardDate <= endDate : true)
                            select new TripVM()
                            {
                                TripUniqueNo = t.TripUniqueNo,
                                AirLineId = t.AirLineId,
                                AirLineCode = a.Code,
                                AirLineFullName = a.AriLineName,
                                TripId = t.TripId,
                                ClassCode = t.ClassCode,
                                classTypeName = t.ClassType == 1 ? "Economy" : "Business",
                                Destination = t.Destination,
                                DestinationFullName = td.AirportCode,
                                IsIndividual = t.IsIndividual,
                                JournayOnWardDate = t.JournayOnWardDate,
                                JournayReturnDate = t.JournayReturnDate,
                                JournayType = t.JournayType,
                                NoOfTickets = t.NoOfTickets,
                                InitialNoOfTickets = t.InitialNoOfTickets.HasValue ? (int)t.InitialNoOfTickets : 0,
                                Origin = t.Origin,
                                OriginFullName = ts.AirportCode,
                                IsPublic = t.IsPublic,
                                DirectTicketSell = t.DirectTicketSell,
                                CRSName = crsUser.CrsName,
                                IsActive = t.IsActive,
                                CreatorUserId = t.CreatedBy,
                                LoggedinUserId = loggedinUserId,
                                Pnrs = pnrTemp.Select(x => new PnrVM() { PnrCount = x.PnrCount, PnrId = x.PnrId, PnrValue = x.PnrValue, TripId = x.TripId }).ToList(),
                                Segments = segmentTemp.Select(x => new SegmentVM { SegmentId = x.SegmentId, TripId = x.TripId }).ToList(),
                                //PriceBreckdowns = priceBTemp.ToList()
                            }).ToList();

            return tripList;
        }
        public bool CreateOrUpdateTrip(TripVM tripInput, List<AirlineJson> AirportTimes)
        {
            using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
            {

                var trip = new Trip();
                trip.TripUniqueNo = tripInput.TripUniqueNo;
                trip.AirLineId = tripInput.AirLineId;
                trip.ClassCode = tripInput.ClassCode;
                trip.ClassType = tripInput.ClassType;
                trip.CreatedBy = tripInput.CreatedBy;
                trip.CreatedDate = DateTime.Now;
                trip.Destination = tripInput.Destination;
                trip.IsIndividual = tripInput.IsIndividual;
                trip.IsRemoved = false;
                trip.JournayOnWardDate = tripInput.Segments.Where(x => x.SegmentSerialNo == 1).FirstOrDefault().DepartureDate.Value;
                trip.JournayReturnDate = tripInput.JournayReturnDate;
                trip.JournayType = tripInput.JournayType;
                //trip.ModifiedBy = User.Identity.GetUserId(); ;
                // trip.ModifiedDate = ;
                trip.NoOfTickets = tripInput.NoOfTickets;
                trip.InitialNoOfTickets = tripInput.InitialNoOfTickets;
                trip.Origin = tripInput.Origin;
                trip.TicketStatus = 1;
                trip.IsPublic = tripInput.IsPublic;
                trip.IsActive = true;
                trip.DirectTicketSell = tripInput.DirectTicketSell;
                _uow.TripRepository.Insert(trip);
                _uow.TripRepository.Save();

                #region add Trip Pnrs

                foreach (var item in tripInput.Pnrs)
                {
                    var vm = new Pnr();

                    vm.CreatedBy = tripInput.CreatedBy;
                    vm.CreatedDate = DateTime.Now;
                    vm.IsRemoved = false;
                    //vm.ModifiedBy = ;
                    //vm.ModifiedDate = ;
                    vm.PnrCount = 1;
                    vm.PnrValue = item.PnrValue;
                    vm.TripId = trip.TripId;

                    _uow.PnrRepository.Insert(vm);
                }
                _uow.PnrRepository.Save();

                #endregion

                #region add Tickets

                if (tripInput.DirectTicketSell == true)
                {
                    foreach (var item in tripInput.Tickets)
                    {
                        var ticket = new Ticket()
                        {
                            TicketNumber = item,
                            TStatus = 0,
                            TripId = trip.TripId,
                            ConfirmationNumber = Guid.NewGuid().ToString(),
                            CreatedBy = tripInput.CreatedBy,
                            CreatedDate = DateTime.Now,
                            IsRemoved = false,
                        };

                        _uow.TicketRepository.Insert(ticket);
                        _uow.TicketRepository.Save();
                    }

                }


                #endregion

                #region add price Segments

                if (tripInput.Segments != null)
                {
                    DateTime? inatialDepartureDate = trip.JournayOnWardDate;
                    DateTime? inatialDepartureDateForSegment = trip.JournayOnWardDate;
                    var itemCount = 0;
                    var inatialSource = trip.Origin;
                    foreach (var item in tripInput.Segments)
                    {
                        var vm = new Segment();
                        vm.SegmentSerialNo = item.SegmentSerialNo;
                        vm.SegmentType = item.SegmentType;
                        vm.TripId = trip.TripId;

                        vm.FlightNumber = item.FlightNumber;
                        vm.Baggage = item.Baggage;
                        vm.CreatedBy = tripInput.CreatedBy;
                        vm.CreatedDate = DateTime.Now;
                        vm.IsRemoved = false;
                        //vm.ModifiedBy = ;
                        //vm.ModifiedDate = ;
                        vm.SegmentOrigin = item.SegmentOrigin;
                        vm.ArrivalDate = item.ArrivalDate;
                        vm.ClassType = item.ClassType;
                        vm.DepartureDate = item.DepartureDate;
                        // vm.Destination = item.Destination;

                        var dtz = AirportTimes.Where(x => x.AirPortCode == item.SegmentOrigin).FirstOrDefault();

                        var OnwordDepartureStr = item.ArrivalDate.Value.ToString("yyyy-MM-dd HH:mm:ss") + dtz.TimeZone;
                        DateTime departureDate = Convert.ToDateTime(OnwordDepartureStr);


                        var atz = AirportTimes.Where(x => x.AirPortCode == inatialSource).FirstOrDefault();
                        var OnwordArrivalStr = item.DepartureDate.Value.ToString("yyyy-MM-dd HH:mm:ss") + atz.TimeZone;
                        DateTime arivalDate = Convert.ToDateTime(OnwordArrivalStr);

                        TimeSpan ts = departureDate.Subtract(arivalDate);

                        vm.Duration = ts.ToString();

                        _uow.SegmentRepository.Insert(vm);
                        _uow.SegmentRepository.Save();

                        inatialDepartureDate = item.DepartureDate;

                        itemCount++;

                        inatialSource = item.SegmentOrigin;
                    }

                }
                #endregion

                #region add Pricebreckdown

                foreach (var price in tripInput.PriceBreckdowns)
                {
                    var priceModel = new PriceBreakdown()
                    {
                        CreatedBy = tripInput.CreatedBy,
                        CreatedDate = DateTime.Now,
                        IsRemove = false,
                        PassengerLimit = price.PassengerLimit,
                        PriceNo = price.PriceNo,
                        PriceSlotName = price.PriceSlotName,
                        TripId = trip.TripId,
                        SingleAdultPrice = price.PriceBreakdownDetails.Where(x => x.PaxType == 1).FirstOrDefault().BaseFare.Value,
                    };
                    _uow.PriceBreakdownRepository.Insert(priceModel);
                    _uow.PriceBreakdownRepository.Save();

                    foreach (var priceDetails in price.PriceBreakdownDetails)
                    {
                        var priceDetailsModel = new PriceBreakdownDetails()
                        {
                            BaseFare = priceDetails.BaseFare != null ? priceDetails.BaseFare : 0,
                            CreatedDate = DateTime.Now,
                            CreatedBy = tripInput.CreatedBy,
                            IsLive = true,
                            IsRemove = false,
                            PaxType = priceDetails.PaxType,
                            PriceBreakdownId = priceModel.Id,
                            ServiceCharge = priceDetails.ServiceCharge == null ? 0 : priceDetails.ServiceCharge,
                            Tax = priceDetails.Tax != null ? priceDetails.Tax : 0
                        };
                        _uow.PriceBreakdownDetailsRepository.Insert(priceDetailsModel);
                        _uow.PriceBreakdownDetailsRepository.Save();
                    }
                }

                #endregion

                scope.Complete();
                return true;

            }
        }


        public bool UpdateTripPriceBreakDown(List<PriceBreakdown> priceBreakdowns, string modifiedBy)
        {
            foreach (var price in priceBreakdowns)
            {
                var priceItem = _uow.PriceBreakdownRepository.Get(x => x.Id == price.Id).FirstOrDefault();

                priceItem.PassengerLimit = price.PassengerLimit;
                priceItem.PriceSlotName = price.PriceSlotName;
                priceItem.ModifiedDate = DateTime.Now;
                priceItem.ModifiedBy = modifiedBy;
                priceItem.SingleAdultPrice = price.PriceBreakdownDetails.Where(x => x.PaxType == 1).FirstOrDefault().BaseFare.Value;

                _uow.PriceBreakdownRepository.Update(priceItem);
                _uow.PriceBreakdownRepository.Save();

                var priceDetailsToUpdate = _uow.PriceBreakdownDetailsRepository.Get(x => x.PriceBreakdownId == price.Id).ToList();

                foreach (var priceDetails in price.PriceBreakdownDetails)
                {
                    var itemToUpdate = priceDetailsToUpdate.Where(x => x.PaxType == priceDetails.PaxType).FirstOrDefault();

                    itemToUpdate.BaseFare = priceDetails.BaseFare != null ? priceDetails.BaseFare : 0;
                    itemToUpdate.ServiceCharge = priceDetails.ServiceCharge != null ? priceDetails.ServiceCharge : 0;
                    itemToUpdate.Tax = priceDetails.Tax != null ? priceDetails.Tax : 0;
                    itemToUpdate.ModifiedDate = DateTime.Now;
                    itemToUpdate.ModifiedBy = modifiedBy;

                    _uow.PriceBreakdownDetailsRepository.Update(itemToUpdate);
                    _uow.PriceBreakdownDetailsRepository.Save();
                }
            }
            return true;
        }


        public List<PriceBreckdownVM> GetPriceBreakDownForViewPartial(long tripId)
        {
            var output = (from t in _uow.TripRepository.GetAllIQueryable()
                          join price in _uow.PriceBreakdownRepository.GetAllIQueryable()
                          on t.TripId equals price.TripId
                          join priceDetails in _uow.PriceBreakdownDetailsRepository.GetAllIQueryable()
                          on price.Id equals priceDetails.PriceBreakdownId into PriceDetailsTemp
                          where t.TripId == tripId
                          select new PriceBreckdownVM()
                          {
                              PriceNo = price.PriceNo,
                              PassengerLimit = price.PassengerLimit,
                              PriceSlotName = price.PriceSlotName,
                              PriceBreakdownDetails = PriceDetailsTemp.Where(x => !x.IsRemove && (x.BaseFare != null && x.BaseFare > 0)).Select(x => new PriceBreakDownDetails()
                              {
                                  BaseFare = x.BaseFare,
                                  PaxType = x.PaxType,
                                  IsLive = x.IsLive,
                                  ServiceCharge = x.ServiceCharge,
                                  Tax = x.Tax,
                                  PriceBreakdownId = x.PriceBreakdownId,
                                  Id = x.Id,
                              }).ToList()
                          }).ToList();

            var pnrLogList = _uow.PnrLogRepository.Get(x => !x.IsRemove).ToList();

            foreach (var item in output)
            {
                var priceBreakDownIds = item.PriceBreakdownDetails.Select(x => x.Id).ToArray();
                item.TotalSellCount = pnrLogList.Where(x => priceBreakDownIds.Contains(x.PriceBreakdownDetailsId) && !x.IsRemoved && x.UsesFlag != (int)PnrUsesType.Cancel).Count();
            }

            return output;


        }
        public TripVM GetTripDetails(TripVM input)
        {
            var tripInfo = (from t in _uow.TripRepository.GetAll().Where(x => x.IsRemoved == false)
                            join a in _uow.AirlineRepository.GetAll()
                            on t.AirLineId equals a.Id
                            join ts in _uow.AirportSearchCodeRepository.GetAll()
                            on t.Origin equals ts.AirportCode
                            join td in _uow.AirportSearchCodeRepository.GetAll()
                            on t.Destination equals td.AirportCode
                            join s in _uow.SegmentRepository.GetAll().Where(x => x.IsRemoved == false)
                            on t.TripId equals s.TripId into segmentTemp
                            join pnr in _uow.PnrRepository.GetAll().Where(x => x.IsRemoved == false)
                            on t.TripId equals pnr.TripId into pnrTemp
                            join pb in _uow.PriceBreakdownRepository.GetAll()
                            on t.TripId equals pb.TripId into priceBTemp
                            join crsUser in _uow.CrsUserRepository.GetAll()
                            on t.CreatedBy equals crsUser.IdentityUserID
                            join ticket in _uow.TicketRepository.GetAll()
                            on t.TripId equals ticket.TripId into ticketsTemp
                            where t.TripId == input.TripId
                            select new TripVM()
                            {
                                AirLineId = t.AirLineId,
                                AirLineCode = a.Code,
                                AirLineFullName = a.AriLineName,
                                TripId = t.TripId,
                                ClassCode = t.ClassCode,
                                classTypeName = t.ClassType == 1 ? "Economy" : "Business",
                                Destination = t.Destination,
                                DestinationFullName = td.AirportCode,
                                IsIndividual = t.IsIndividual,
                                JournayOnWardDate = t.JournayOnWardDate,
                                JournayReturnDate = t.JournayReturnDate,
                                JournayType = t.JournayType,
                                NoOfTickets = t.NoOfTickets,
                                InitialNoOfTickets = t.InitialNoOfTickets.HasValue ? (int)t.InitialNoOfTickets : 0,
                                Origin = t.Origin,
                                OriginFullName = ts.AirportCode,
                                IsPublic = t.IsPublic,
                                CRSName = crsUser.CrsName,
                                IsActive = t.IsActive,
                                CreatorUserId = t.CreatedBy,
                                AgencyName = crsUser.CrsName,
                                AgenctContactNo = crsUser.UserMobileNo,
                                AgencyAddress = crsUser.Address,

                                Pnrs = pnrTemp.Select(x => new PnrVM()
                                {
                                    PnrCount = x.PnrCount,
                                    PnrId = x.PnrId,
                                    PnrValue = x.PnrValue,
                                    TripId = x.TripId
                                }).ToList(),
                                //PriceBreckdowns = priceBTemp.Select(x => new PriceBreckdownVM()
                                //{
                                //    BaseFare = x.BaseFare,
                                //    PaxType = x.PaxType,
                                //    PriceBreckdownId = x.PriceBreckdownId,
                                //    Tax = x.Tax,
                                //    ServiceCharge = x.ServiceCharge,
                                //    TripId = x.TripId
                                //}).ToList(),
                                Tickets = ticketsTemp.Select(tic => tic.TicketNumber).ToList(),

                            }).FirstOrDefault();

            var allAirports = _uow.AirportSearchCodeRepository.GetAll();

            var setments = (from s in _uow.SegmentRepository.GetAll().Where(x => x.IsRemoved == false)
                            join ts in _uow.AirportSearchCodeRepository.GetAll()
                            on s.SegmentOrigin equals ts.AirportCode
                            where s.TripId == tripInfo.TripId
                            orderby s.SegmentSerialNo ascending
                            select new SegmentVM()
                            {
                                TripId = s.TripId,
                                ArrivalDate = s.ArrivalDate,
                                DepartureDate = s.DepartureDate,
                                Origin = ts.AirportCode,
                                Duration = s.Duration,
                                FlightNumber = s.FlightNumber,
                                Baggage = s.Baggage,
                                ClassType = s.ClassType,
                                SegmentSerialNo = s.SegmentSerialNo,
                                SegmentType = s.SegmentType,
                                ClassTypeName = s.ClassType == 1 ? "Economy" : "Business",
                                AirLines = s.AirLines,

                            }).ToList();

            var inatialSource = tripInfo.Origin;

            var count = 0;

            foreach (var item in setments)
            {
                var segmentItem = new SegmentVM()
                {
                    TripId = item.TripId,
                    Origin = inatialSource,
                    Destination = item.Origin,
                    ArrivalDate = item.ArrivalDate,
                    DepartureDate = item.DepartureDate,
                    ClassTypeName = item.ClassTypeName,
                    SegmentType = item.SegmentType,
                    SegmentSerialNo = item.SegmentSerialNo,
                    FlightNumber = item.FlightNumber,
                    Baggage = item.Baggage,
                    TravelTime = item.TravelTime,
                    Duration = item.Duration,
                    AirLines = item.AirLines,
                    AirportFullNameForDestination = allAirports.Where(x => x.AirportCode == item.Origin).FirstOrDefault()?.SearchString,
                    AirportFullNameForOrigin = allAirports.Where(x => x.AirportCode == inatialSource).FirstOrDefault()?.SearchString
                };

                tripInfo.Segments.Add(segmentItem);
                //update source
                inatialSource = segmentItem.Destination;
                count++;
            }
            return tripInfo;
        }

        public bool ChangeTripStatus(Trip tripInput)
        {
            try
            {
                var trip = new Trip();
                trip = _uow.TripRepository.GetByID(tripInput.TripId);
                trip.IsPublic = !trip.IsPublic;
                _uow.TripRepository.Update(trip);
                _uow.TripRepository.Save();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public TicketUpdateVM GetTicketByTrip(Trip tripInput)
        {
            var output = (from trip in _uow.TripRepository.GetAll().Where(x => x.TripId == tripInput.TripId)
                          join ticket in _uow.TicketRepository.GetAll()
                             on trip.TripId equals ticket.TripId into ticketTemp
                          select new TicketUpdateVM()
                          {
                              NoOfTicket = trip.NoOfTickets,
                              InitialNoOfTickets = trip.InitialNoOfTickets.HasValue ? (int)trip.InitialNoOfTickets : 0,
                              TripId = trip.TripId,
                              TicketList = ticketTemp.Select(x => new TicketList()
                              {
                                  TripId = trip.TripId,
                                  TicketId = x.TicketId,
                                  TicketNo = x.TicketNumber
                              }).ToList()
                          }).FirstOrDefault();
            return output;
        }

        public bool ChangeTripActiveStatus(Trip tripInput)
        {
            try
            {
                var trip = new Trip();
                trip = _uow.TripRepository.GetByID(tripInput.TripId);
                trip.IsActive = !trip.IsActive;
                _uow.TripRepository.Update(trip);
                _uow.TripRepository.Save();

                return true;
            }
            catch (Exception)
            {
                return false;
            }

        } 
        

        public bool CheckGroupUniqueNumber(string groupUniqueNumber, string loggedInUserId)
        {
            try
            {
                var output = _uow.TripRepository.Get(x => x.TripUniqueNo.ToLower() == groupUniqueNumber.ToLower() && x.CreatedBy == loggedInUserId).Any();
                return output;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public List<TripReportOutputDto> GetTripReportList(TripSearchInputDto input)
        {
            DateTime? startDate = null;
            DateTime? endDate = null;

            if (input.StartDate.HasValue && input.EndDate.HasValue)
            {
                endDate = new DateTime(input.EndDate.Value.Year, input.EndDate.Value.Month, input.EndDate.Value.Day, 23, 59, 59, 999);
                startDate = new DateTime(input.StartDate.Value.Year, input.StartDate.Value.Month, input.StartDate.Value.Day, 0, 0, 0, 0);
            }

            return (from trip in _uow.TripRepository.GetAll().Where(x => !x.IsRemoved)
                    join airline in _uow.AirlineRepository.GetAll()
                    on trip.AirLineId equals airline.Id
                    join pnr in _uow.PnrRepository.GetAll().Where(x => !x.IsRemoved)
                    on trip.TripId equals pnr.TripId
                    join pnrLog in _uow.PnrLogRepository.GetAll().Where(x => !x.IsRemoved)
                    on pnr.PnrId equals pnrLog.PnrId
                    join priceBreckDownDetails in _uow.PriceBreakdownDetailsRepository.GetAll()//.Where(x => !x.IsRemoved && x.IsLive)
                    on pnrLog.PriceBreakdownDetailsId equals priceBreckDownDetails.Id
                    where (input.OnlySuccess ? pnrLog.UsesFlag == (int)PnrUsesType.Ticketed : (pnrLog.UsesFlag == (int)PnrUsesType.Ticketed || pnrLog.UsesFlag == (int)PnrUsesType.Cancel)) &&
                          trip.CreatedBy == input.CreatedBy &&
                          (startDate != null && endDate != null ? pnrLog.CreatedDate > startDate && pnrLog.CreatedDate < endDate : true)
                    orderby pnrLog.PnrLogId descending
                    select new TripReportOutputDto
                    {
                        TripUniqueNo = trip.TripUniqueNo,
                        PnrLogId = pnrLog.PnrLogId,
                        TripId = trip.TripId,
                        PnrId = pnr.PnrId,
                        Origin = trip.Origin,
                        Destination = trip.Destination,
                        PassengerType = priceBreckDownDetails.PaxType == 1 ? "ADULT" : priceBreckDownDetails.PaxType == 2 ? "CHILD" : "INFRNT",
                        PnrNumber = pnr.PnrValue,
                        TotalPrice = pnrLog.TotalPrice,
                        AirLines = airline.AriLineName,
                        PnrUsesType = ((PnrUsesType)pnrLog.UsesFlag).ToString()
                    }).ToList();
        }

        public TicketDetailDto GetTicketInfo(PnrLog input)
        {
            return (from pnrLog in _uow.PnrLogRepository.GetAll().Where(x => !x.IsRemoved && x.PnrLogId == input.PnrLogId)
                    join priceBreckDownDetails in _uow.PriceBreakdownDetailsRepository.GetAll()//.Where(x => !x.IsRemoved && x.IsLive)
                    on pnrLog.PriceBreakdownDetailsId equals priceBreckDownDetails.Id
                    join passenger in _uow.PassengerRepository.GetAll()
                    on pnrLog.PassengerId equals passenger.PassengerId
                    join pnr in _uow.PnrRepository.GetAll()
                    on pnrLog.PnrId equals pnr.PnrId
                    join trip in _uow.TripRepository.GetAll()
                    on pnr.TripId equals trip.TripId
                    join crs in _uow.CrsUserRepository.GetAll()
                    on trip.CreatedBy equals crs.IdentityUserID
                    join airline in _uow.AirlineRepository.GetAll()
                    on trip.AirLineId equals airline.Id
                    select new TicketDetailDto
                    {
                        PnrLogId = pnrLog.PnrLogId,
                        TripId = trip.TripId,
                        PnrId = pnr.PnrId,
                        Origin = trip.Origin,
                        Destination = trip.Destination,
                        PassengerType = priceBreckDownDetails.PaxType == 1 ? "ADULT" : priceBreckDownDetails.PaxType == 2 ? "CHILD" : "INFRNT",
                        PnrNumber = pnr.PnrValue,
                        //Status = pnrLog.UsesFlag == (int)PnrUsesType.Booked ? "Booked" : pnrLog.UsesFlag == (int)PnrUsesType.Ticketed ? "Success" : pnrLog.UsesFlag == (int)PnrUsesType.Cancel ? "Cancel" : "N/A",
                        Status = ((PnrUsesType)pnrLog.UsesFlag).ToString(),
                        TicketNumber = pnrLog.TicketNumber,
                        TotalPrice = pnrLog.TotalPrice,
                        AirLines = airline.AriLineName,
                        //PnrUsesType = pnrLog.UsesFlag == 1 ? "Booked" : pnrLog.UsesFlag == 2 ? "SUCCESS" : "CANCEL",
                        PnrUsesType = ((PnrUsesType)pnrLog.UsesFlag).ToString(),
                        AgenctContactNo = crs.UserMobileNo,
                        AgencyAddress = crs.Address,
                        AgentAddress = crs.Address,
                        AgencyName = crs.CrsName,
                        DateOfBirth = passenger.DOB,
                        ClassType = trip.ClassType == 1 ? "Economy" : trip.ClassType == 2 ? "Business" : "N/A",
                        Gender = passenger.Gender,
                        ContactNo = passenger.ContractNumber,
                        PassengerName = $"{passenger.Title} {passenger.FirstName} {passenger.LastName}",
                        PassportNo = passenger.PassportNumber,
                    }).FirstOrDefault();
        }

        public List<PriceBreckdownVM> GetTripPriceForUpdate(long tripId, string LoggedinUserId)
        {
            var output = _uow.PriceBreakdownRepository.GetAll().Where(x => x.TripId == tripId)
                .Select(x => new PriceBreckdownVM()
                {
                    Id = x.Id,
                    Currency = x.Currency,
                    PassengerLimit = x.PassengerLimit,
                    PriceNo = x.PriceNo,
                    PriceSlotName = x.PriceSlotName,
                    LoggedinUserId = LoggedinUserId,
                    TotalTripTicketCount = x.Trip.NoOfTickets,
                    PriceBreakdownDetails = x.PriceBreakdownDetails.Select(pb => new PriceBreakDownDetails()
                    {
                        Id = pb.Id,
                        BaseFare = pb.BaseFare,
                        IsLive = pb.IsLive,
                        PaxType = pb.PaxType,
                        PriceBreakdownId = pb.PriceBreakdownId,
                        ServiceCharge = pb.ServiceCharge,
                        Tax = pb.Tax
                    }).ToList(),
                    CreatorUserId = x.CreatedBy,
                }).ToList();

            foreach (var item in output)
            {
                var priceDetailsIds = item.PriceBreakdownDetails.Select(x => x.Id).ToArray();
                item.IsPriceUsed = _uow.PnrLogRepository.Get(x => priceDetailsIds.Contains(x.PriceBreakdownDetailsId)).Any();
            }

            return output;
        }
        public bool UpdateTripTickets(TicketUpdateVM input)
        {
            try
            {
                if (input.TicketList.Count > 0)
                {
                    foreach (var item in input.TicketList)
                    {

                        var tickets = _uow.TicketRepository.Get(x => x.TicketId == item.TicketId).FirstOrDefault();
                        var pnrLog = _uow.PnrLogRepository.Get(x => x.PnrLogId == item.PnrLogId).FirstOrDefault();
                        if (tickets != null)
                        {
                            tickets.TicketNumber = item.TicketNo;
                            _uow.TicketRepository.Update(tickets);
                            _uow.TicketRepository.Save();

                        }
                        else
                        {
                            Ticket ticket = new Ticket();
                            ticket.TicketNumber = item.TicketNo;
                            ticket.TStatus = 0;
                            ticket.TripId = item.TripId;
                            ticket.ConfirmationNumber = Guid.NewGuid().ToString();
                            ticket.CreatedBy = input.CreatedBy;
                            ticket.CreatedDate = DateTime.Now;
                            ticket.IsRemoved = false;
                            _uow.TicketRepository.Insert(ticket);
                            _uow.TicketRepository.Save();
                        }

                        if (pnrLog != null)
                        {
                            pnrLog.TicketNumber = item.TicketNo;
                            pnrLog.UsesFlag = !string.IsNullOrWhiteSpace(item.TicketNo) ? (int)PnrUsesType.Ticketed : pnrLog.UsesFlag;
                            _uow.PnrLogRepository.Update(pnrLog);
                            _uow.TicketRepository.Save();
                        }
                    }
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public CrsAdminDashboardOutputDto GetCrsAdminDashboardSummary(string logedInUserId)
        {
            var output = new CrsAdminDashboardOutputDto();

            var queary = (from trip in _uow.TripRepository.GetAll().Where(x => !x.IsRemoved)
                          join pnr in _uow.PnrRepository.GetAll().Where(x => !x.IsRemoved)
                          on trip.TripId equals pnr.TripId
                          //from Tpnr in pnrTemp.DefaultIfEmpty()
                          join pLog in _uow.PnrLogRepository.GetAll().Where(x => !x.IsRemoved)
                          on pnr.PnrId equals pLog.PnrId into pnrLogTemp
                          where trip.CreatedBy == logedInUserId
                          select new
                          {
                              trip.TripId,
                              trip.Origin,
                              trip.Destination,
                              trip.NoOfTickets,
                              pnr.PnrValue,
                              pnrLogTemp
                          }).ToList();

            var totalSitOfCrs = 0;
            var totalSoldOfCrs = 0;

            foreach (var item in queary)
            {
                var tripInfo = new TripInfo()
                {
                    Origin = item.Origin,
                    Destination = item.Destination,
                    TotalSit = item.NoOfTickets,
                    Sold = item.pnrLogTemp.Where(x => x.UsesFlag == (int)PnrUsesType.Ticketed || x.UsesFlag == (int)PnrUsesType.Booked).Count(),
                    Remaining = item.NoOfTickets - item.pnrLogTemp.Where(x => x.UsesFlag == (int)PnrUsesType.Ticketed || x.UsesFlag == (int)PnrUsesType.Booked).Count()
                };

                totalSitOfCrs = totalSitOfCrs + item.NoOfTickets;
                totalSoldOfCrs = totalSoldOfCrs + tripInfo.Sold;

                output.TripInfos.Add(tripInfo);
            }

            output.TotalSit = totalSitOfCrs;
            output.TotalSold = totalSoldOfCrs;

            output.PnrLogList = GetPNRLogData(logedInUserId);

            return output;
        }

        public List<BookingOutputDto> GetAllBookingList(SearchInputModel input)
        {
            var unSeenBookingList = _uow.PnrLogRepository.GetAll().Where(x => x.IsRemoved == false && !x.IsSeen && x.CreatedBy == input.LogedInUserId && x.UsesFlag == (int)PnrUsesType.Booked).ToList();

            foreach (var item in unSeenBookingList)
            {
                item.IsSeen = true;
                _uow.PnrLogRepository.Update(item);
            }
            _uow.PnrLogRepository.Save();

            DateTime? startDate = null;
            DateTime? endDate = null;

            if (input.StartDate.HasValue && input.EndDate.HasValue)
            {
                endDate = new DateTime(input.EndDate.Value.Year, input.EndDate.Value.Month, input.EndDate.Value.Day, 23, 59, 59, 999);
                startDate = new DateTime(input.StartDate.Value.Year, input.StartDate.Value.Month, input.StartDate.Value.Day, 0, 0, 0, 0);
            }

            var output = (from trip in _uow.TripRepository.GetAll().Where(x => !x.IsRemoved)
                          join airline in _uow.AirlineRepository.GetAll()
                          on trip.AirLineId equals airline.Id
                          join pnr in _uow.PnrRepository.GetAll().Where(x => !x.IsRemoved)
                          on trip.TripId equals pnr.TripId
                          join pnrLog in _uow.PnrLogRepository.GetAll().Where(x => !x.IsRemoved)
                          on pnr.PnrId equals pnrLog.PnrId
                          join priceBreckDownDetails in _uow.PriceBreakdownDetailsRepository.GetAll()
                          on pnrLog.PriceBreakdownDetailsId equals priceBreckDownDetails.Id
                          where pnrLog.UsesFlag == (int)PnrUsesType.Booked &&
                               trip.CreatedBy == input.LogedInUserId &&
                               (startDate != null && endDate != null ? (pnrLog.CreatedDate > startDate && pnrLog.CreatedDate < endDate) : true)
                          orderby pnrLog.PnrLogId descending
                          select new BookingOutputDto
                          {
                              TripUniqueNo = trip.TripUniqueNo,
                              PnrLogId = pnrLog.PnrLogId,
                              TripId = trip.TripId,
                              PnrId = pnr.PnrId,
                              Origin = trip.Origin,
                              Destination = trip.Destination,
                              PassengerType = priceBreckDownDetails.PaxType == 1 ? "ADULT" : priceBreckDownDetails.PaxType == 2 ? "CHILD" : "INFRNT",
                              PnrNumber = pnr.PnrValue,
                              TotalPrice = pnrLog.TotalPrice,
                              AirLines = airline.AriLineName
                          }).ToList();

            return output;
        }

        public int GetNewBookingNotification()
        {
            var output = _uow.PnrLogRepository.GetAll().Where(x => x.IsRemoved == false && !x.IsSeen && x.UsesFlag == (int)PnrUsesType.Booked).Count();
            return output;
        }

        public List<string> ValidateTripTicketNumber(List<string> tickets)
        {
            var hasTickes = _uow.TicketRepository.GetAll().Where(x => !x.IsRemoved && tickets.Contains(x.TicketNumber)).Select(x => x.TicketNumber).ToList();
            return hasTickes;
        }


        public TicketUpdateVM GetTicketByPnr(long tripId)
        {
            var trip = _uow.TripRepository.GetAll().Where(t => t.TripId == tripId).FirstOrDefault();
            var pnr = _uow.PnrRepository.GetAll().Where(p => p.TripId == tripId).FirstOrDefault();
            var pnrLogTemp = _uow.PnrLogRepository.GetAll().Where(x => x.PnrId == pnr.PnrId).ToList();
            var ticketTemp = _uow.TicketRepository.GetAll().Where(t => t.TripId == tripId).ToList();
            var passengers = _uow.PassengerRepository.GetAll().Where(x => pnrLogTemp.Any(p => p.PassengerId == x.PassengerId)).ToList();
            var output = new TicketUpdateVM()
            {
                TripId = trip.TripId,
                InitialNoOfTickets = trip.InitialNoOfTickets.HasValue ? (int)trip.InitialNoOfTickets : 0,
                NoOfTicket = trip.NoOfTickets,
                CreatedBy = trip.CreatedBy,
                TicketList = pnrLogTemp.Select(x => new TicketList()
                {
                    TripId = trip.TripId,
                    PnrLogId = x.PnrLogId,
                    TicketId = ticketTemp.Where(t => t.TicketNumber == x.TicketNumber).FirstOrDefault() != null ? (int)ticketTemp.Where(t => t.TicketNumber == x.TicketNumber).FirstOrDefault()?.TicketId : 0,
                    TicketNo = x.TicketNumber,
                    PassengerName = passengers.Where(t => t.PassengerId == x.PassengerId).FirstOrDefault() != null ? $"{ passengers.Where(t => t.PassengerId == x.PassengerId).FirstOrDefault().Title} {passengers.Where(t => t.PassengerId == x.PassengerId).FirstOrDefault().FirstName} {passengers.Where(t => t.PassengerId == x.PassengerId).FirstOrDefault().LastName}" : "",
                }).ToList()
            };

            return output;
        }

        public TripExtraServicesDto GetTripExtraService(long tripId)
        {
            var extraServices = (from trip in _uow.TripRepository.GetAllIQueryable()
                                 join baggage in _uow.BaggageRepository.GetAllIQueryable()
                                 on trip.TripId equals baggage.TripId into BaggageTemp
                                 join meal in _uow.MealRepository.GetAllIQueryable()
                                 on trip.TripId equals meal.TripId into mealTemp
                                 join wheelChair in _uow.WheelChairRepository.GetAllIQueryable()
                                 on trip.TripId equals wheelChair.TripId into wheelChairTemp
                                 where trip.TripId == tripId
                                 select new TripExtraServicesDto()
                                 {
                                     TripId = trip.TripId,
                                     BaggageList = BaggageTemp.Select(b => new BaggageDto()
                                     {
                                         Id = b.Id,
                                         TripId = b.TripId,
                                         BaggageType = b.BaggageType,
                                         IsActive = b.IsActive,
                                         Price = b.Price,
                                         Quantity = b.Quantity
                                     }).ToList(),
                                     MealsList = mealTemp.Select(m => new MealsDto()
                                     {
                                         Price = m.Price,
                                         Code = m.Code,
                                         Id = m.Id,
                                         IsActive = m.IsActive,
                                         Name = m.Name,
                                         TripId = m.TripId
                                     }).ToList(),
                                     WheelChaiList = wheelChairTemp.Select(wh => new WheelChairDto()
                                     {
                                         TripId = wh.TripId,
                                         Id = wh.Id,
                                         IsActive = wh.IsActive,
                                         IsAvailable = wh.IsAvailable,
                                         Price = wh.Price
                                     }).ToList()
                                 }).FirstOrDefault();

            return extraServices;
        }

        #region Baggage
        public BaggageDto GetBaggageForUpdate(long id)
        {
            return _uow.BaggageRepository.Get(x => x.Id == id).Select(s => new BaggageDto()
            {
                Id = s.Id,
                BaggageType = s.BaggageType,
                IsActive = true,
                Price = s.Price,
                Quantity = s.Quantity,
                TripId = s.TripId
            }).FirstOrDefault();
        }
        public bool CreateOrUpdateBaggage(BaggageDto input)
        {
            var itemToCreateOrUpdate = _uow.BaggageRepository.GetAll().Where(x => x.Id == input.Id).FirstOrDefault() ?? new Baggage();
            if (itemToCreateOrUpdate.Id <= 0)
            {
                itemToCreateOrUpdate.BaggageType = input.BaggageType;
                itemToCreateOrUpdate.Quantity = input.Quantity;
                itemToCreateOrUpdate.Price = input.Price;
                itemToCreateOrUpdate.IsRemove = false;
                itemToCreateOrUpdate.TripId = input.TripId;
                itemToCreateOrUpdate.CreatedDate = DateTime.Now;
                itemToCreateOrUpdate.CreatedBy = input.CreatedBy;
                itemToCreateOrUpdate.IsActive = true;

                _uow.BaggageRepository.Insert(itemToCreateOrUpdate);
                _uow.BaggageRepository.Save();

                return true;
            }
            else if (itemToCreateOrUpdate.BaggageType != input.BaggageType || itemToCreateOrUpdate.Price != input.Price || itemToCreateOrUpdate.Quantity != input.Quantity)
            {

                itemToCreateOrUpdate.ModifiedDate = DateTime.Now;
                itemToCreateOrUpdate.ModifiedBy = input.ModifiedBy;
                itemToCreateOrUpdate.IsActive = false;

                _uow.BaggageRepository.Update(itemToCreateOrUpdate);
                _uow.BaggageRepository.Save();


                itemToCreateOrUpdate.BaggageType = input.BaggageType;
                itemToCreateOrUpdate.Quantity = input.Quantity;
                itemToCreateOrUpdate.Price = input.Price;
                itemToCreateOrUpdate.IsRemove = false;
                itemToCreateOrUpdate.TripId = input.TripId;
                itemToCreateOrUpdate.CreatedDate = DateTime.Now;
                itemToCreateOrUpdate.CreatedBy = input.CreatedBy;
                itemToCreateOrUpdate.IsActive = true;
                itemToCreateOrUpdate.ModifiedDate = null;
                itemToCreateOrUpdate.ModifiedBy = string.Empty;

                _uow.BaggageRepository.Insert(itemToCreateOrUpdate);
                _uow.BaggageRepository.Save();

                return true;
            }
            return true;
        }
        #endregion

        #region Meal
        public MealsDto GetMealForUpdate(long id)
        {
            return _uow.MealRepository.Get(x => x.Id == id).Select(s => new MealsDto()
            {
                Id = s.Id,
                Price = s.Price,
                Code = s.Code,
                Name = s.Name,
                IsActive = s.IsActive,
                TripId = s.TripId
            }).FirstOrDefault();
        }
        public bool CreateOrUpdateMeal(MealsDto input)
        {
            var itemToCreateOrUpdate = _uow.MealRepository.GetAll().Where(x => x.Id == input.Id).FirstOrDefault() ?? new Meal();
            if (itemToCreateOrUpdate.Id <= 0)
            {
                itemToCreateOrUpdate.Code = input.Code;
                itemToCreateOrUpdate.Name = input.Name;
                itemToCreateOrUpdate.Price = input.Price;
                itemToCreateOrUpdate.IsRemove = false;
                itemToCreateOrUpdate.TripId = input.TripId;
                itemToCreateOrUpdate.CreatedDate = DateTime.Now;
                itemToCreateOrUpdate.CreatedBy = input.CreatedBy;

                itemToCreateOrUpdate.IsActive = true;

                _uow.MealRepository.Insert(itemToCreateOrUpdate);
                _uow.MealRepository.Save();

                return true;
            }
            else if (itemToCreateOrUpdate.Code != input.Code || itemToCreateOrUpdate.Price != input.Price || itemToCreateOrUpdate.Name != input.Name)
            {

                itemToCreateOrUpdate.ModifiedDate = DateTime.Now;
                itemToCreateOrUpdate.ModifiedBy = input.ModifiedBy;
                itemToCreateOrUpdate.IsActive = false;

                _uow.MealRepository.Update(itemToCreateOrUpdate);
                _uow.MealRepository.Save();


                itemToCreateOrUpdate.Code = input.Code;
                itemToCreateOrUpdate.Name = input.Name;
                itemToCreateOrUpdate.Price = input.Price;
                itemToCreateOrUpdate.IsRemove = false;
                itemToCreateOrUpdate.TripId = input.TripId;
                itemToCreateOrUpdate.CreatedDate = DateTime.Now;
                itemToCreateOrUpdate.CreatedBy = input.CreatedBy;
                itemToCreateOrUpdate.IsActive = true;
                itemToCreateOrUpdate.ModifiedDate = null;
                itemToCreateOrUpdate.ModifiedBy = string.Empty;

                _uow.MealRepository.Insert(itemToCreateOrUpdate);
                _uow.MealRepository.Save();

                return true;
            }
            return true;
        }
        #endregion

        public long? CreateOrUpdateWheelChair(WheelChairDto input)
        {
            var itemToCreateOrUpdate = _uow.WheelChairRepository.GetAll().Where(x => x.Id == input.Id).FirstOrDefault() ?? new WheelChair();
            if (itemToCreateOrUpdate.Id <= 0)
            {
                itemToCreateOrUpdate.IsAvailable = input.IsAvailable;
                itemToCreateOrUpdate.Price = input.IsAvailable ? input.Price : 0;
                itemToCreateOrUpdate.IsRemove = false;
                itemToCreateOrUpdate.TripId = input.TripId;
                itemToCreateOrUpdate.CreatedDate = DateTime.Now;
                itemToCreateOrUpdate.CreatedBy = input.CreatedBy;

                itemToCreateOrUpdate.IsActive = true;

                _uow.WheelChairRepository.Insert(itemToCreateOrUpdate);
                _uow.WheelChairRepository.Save();

                return itemToCreateOrUpdate.Id;
            }
            else if (itemToCreateOrUpdate.IsAvailable != input.IsAvailable || itemToCreateOrUpdate.Price != input.Price || itemToCreateOrUpdate.Price != input.Price)
            {

                itemToCreateOrUpdate.ModifiedDate = DateTime.Now;
                itemToCreateOrUpdate.ModifiedBy = input.ModifiedBy;
                itemToCreateOrUpdate.IsActive = false;

                _uow.WheelChairRepository.Update(itemToCreateOrUpdate);
                _uow.WheelChairRepository.Save();


                itemToCreateOrUpdate.IsAvailable = input.IsAvailable;
                itemToCreateOrUpdate.Price = input.IsAvailable ? input.Price : 0;
                itemToCreateOrUpdate.IsRemove = false;
                itemToCreateOrUpdate.TripId = input.TripId;
                itemToCreateOrUpdate.CreatedDate = DateTime.Now;
                itemToCreateOrUpdate.CreatedBy = input.CreatedBy;
                itemToCreateOrUpdate.IsActive = true;
                itemToCreateOrUpdate.ModifiedDate = null;
                itemToCreateOrUpdate.ModifiedBy = string.Empty;

                _uow.WheelChairRepository.Insert(itemToCreateOrUpdate);
                _uow.WheelChairRepository.Save();

                return itemToCreateOrUpdate.Id;
            }
            else
            {
                return null;
            }
        }

        #region private Method

        private List<BookingOutputDto> GetPNRLogData(string logedInUserId)
        {
            try
            {
                DateTime tenDaysBeforeDate = DateTime.Now.AddDays(-10);

                var query = (from trip in _uow.TripRepository.GetAll().Where(x => !x.IsRemoved)
                             join airline in _uow.AirlineRepository.GetAll()
                             on trip.AirLineId equals airline.Id
                             join pnr in _uow.PnrRepository.GetAll().Where(x => !x.IsRemoved)
                             on trip.TripId equals pnr.TripId
                             join pnrLog in _uow.PnrLogRepository.GetAll().Where(x => !x.IsRemoved)
                             on pnr.PnrId equals pnrLog.PnrId
                             join priceBreckDown in _uow.PriceBreakdownDetailsRepository.GetAll()
                             on pnrLog.PriceBreakdownDetailsId equals priceBreckDown.Id
                             where (pnrLog.UsesFlag == (int)PnrUsesType.Booked || pnrLog.UsesFlag == (int)PnrUsesType.Ticketed) && trip.CreatedBy == logedInUserId && (pnrLog.CreatedDate > tenDaysBeforeDate && pnrLog.CreatedDate < DateTime.Now)
                             orderby pnrLog.PnrLogId descending
                             select new BookingOutputDto
                             {
                                 PnrLogId = pnrLog.PnrLogId,
                                 TripId = trip.TripId,
                                 PnrId = pnr.PnrId,
                                 Origin = trip.Origin,
                                 Destination = trip.Destination,
                                 PassengerType = priceBreckDown.PaxType == 1 ? "ADULT" : priceBreckDown.PaxType == 2 ? "CHILD" : "INFANT",
                                 PnrNumber = pnr.PnrValue,
                                 TotalPrice = pnrLog.TotalPrice,
                                 AirLines = airline.AriLineName,
                                 PNRUsesType = pnrLog.UsesFlag
                             });


                return query.ToList();

            }
            catch (Exception ex)
            {
                return new List<BookingOutputDto>();
            }
        }

        #endregion
    }
}
