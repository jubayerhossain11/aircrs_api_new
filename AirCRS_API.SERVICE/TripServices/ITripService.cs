﻿using AirCRS_API.DAL.Entities;
using AirCRS_API.SERVICE.CommonServices.Dtos;
using AirCRS_API.SERVICE.TripServices.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirCRS_API.SERVICE.TripServices
{
    public interface ITripService
    {
        List<Trip> GetAll();
        bool CreateOrUpdateTrip(TripVM tripInput, List<AirlineJson> AirportTimes);
        List<TripVM> GetTripList(int? pmsGroupId, string loggedinUserId, string IdentityUserID, bool isAdmin, DateTime? startDate, DateTime? endDate, bool MyTripOnly);
        TripVM GetTripDetails(TripVM input);
        bool ChangeTripStatus(Trip tripInput);
        TicketUpdateVM GetTicketByTrip(Trip tripInput);
        bool ChangeTripActiveStatus(Trip tripInput);
        List<TripReportOutputDto> GetTripReportList(TripSearchInputDto input);
        TicketDetailDto GetTicketInfo(PnrLog input);
        bool UpdateTripTickets(TicketUpdateVM input);
        CrsAdminDashboardOutputDto GetCrsAdminDashboardSummary(string logedInUserId);
        List<BookingOutputDto> GetAllBookingList(SearchInputModel input);
        int GetNewBookingNotification();

        List<string> ValidateTripTicketNumber(List<string> tickets);

        bool UpdateTripPriceBreakDown(List<PriceBreakdown> priceBreakdowns, string modifiedBy);

        List<PriceBreckdownVM> GetTripPriceForUpdate(long tripId, string LoggedinUserId);

        TicketUpdateVM GetTicketByPnr(long tripId);

        TripExtraServicesDto GetTripExtraService(long tripId);
        BaggageDto GetBaggageForUpdate(long id);
        bool CreateOrUpdateBaggage(BaggageDto input);  
        MealsDto GetMealForUpdate(long id);
        bool CreateOrUpdateMeal(MealsDto input);
        long? CreateOrUpdateWheelChair(WheelChairDto input);

        List<PriceBreckdownVM> GetPriceBreakDownForViewPartial(long tripId);
        bool CheckGroupUniqueNumber(string groupUniqueNumber, string loggedInUserId);

    }
}
