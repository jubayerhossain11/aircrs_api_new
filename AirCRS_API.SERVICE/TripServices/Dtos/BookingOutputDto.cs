﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirCRS_API.SERVICE.TripServices.Dtos
{
    public class BookingOutputDto
    {
        public long TripId { get; set; }
        public long PnrId { get; set; }
        public long PnrLogId { get; set; }
        public string TripUniqueNo { get; set; }
        public string PnrNumber { get; set; }
        public string Origin { get; set; }
        public string Destination { get; set; }
        public string AirLines { get; set; }
        public string PassengerType { get; set; }

        public decimal TotalPrice { get; set; }
        public int PNRUsesType { get; set; }
    }
}
