﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirCRS_API.SERVICE.TripServices.Dtos
{
    

    public class CrsAdminDashboardOutputDto
    {
        public CrsAdminDashboardOutputDto()
        {
            TripInfos = new List<TripInfo>();
            PnrLogList = new List<BookingOutputDto>();
        }

        public int TotalSit { get; set; }
        public int TotalSold { get; set; }

        public List<TripInfo> TripInfos { get; set; }
        public List<BookingOutputDto> PnrLogList { get; set; }

    }


    public class TripInfo
    {
        public int TripId { get; set; }
        public string Origin { get; set; }
        public string Destination { get; set; }
        public int TotalSit { get; set; }
        public int Sold { get; set; }
        public int Remaining { get; set; }
    }
}
