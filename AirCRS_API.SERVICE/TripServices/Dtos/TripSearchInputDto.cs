﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirCRS_API.SERVICE.TripServices.Dtos
{
    public class TripSearchInputDto
    {
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool OnlySuccess { get; set; }
        public bool MyTripOnly { get; set; }
        public string IdentityUserID { get; set; }
        public string CreatedBy { get; set; }
    }
}
