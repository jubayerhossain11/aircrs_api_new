﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirCRS_API.SERVICE.TripServices.Dtos
{
    public class TicketDetailDto
    {
        public long TripId { get; set; }
        public long PnrId { get; set; }
        public long PnrLogId { get; set; }

        //PassengerInfo
        public string PassengerName { get; set; }
        public string PassportNo { get; set; }
        public string Gender { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string ContactNo { get; set; }
        public string PassengerType { get; set; }

        //Ticket Details
        public string PnrNumber { get; set; }
        public string TicketNumber { get; set; }
        public string Origin { get; set; }
        public string Destination { get; set; }
        public string AirLines { get; set; }
        public string ClassType { get; set; }
        public string Status { get; set; }
        public string PnrUsesType { get; set; }
        public decimal TotalPrice { get; set; }

        //Agency Information
        public string AgencyName { get; set; }
        public string AgencyAddress { get; set; }
        public string AgentAddress { get; set; }
        public string AgenctContactNo { get; set; }
    }
}
