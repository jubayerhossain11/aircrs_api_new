﻿using AirCRS_API.DAL.CommonEntities;
using AirCRS_API.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirCRS_API.SERVICE.TripServices.Dtos
{
    public class TripVM : AuditEntity
    {
        public TripVM()
        {
            Segments = new List<SegmentVM>();
            PriceBreckdowns = new List<PriceBreakdown>();
            Pnrs = new List<PnrVM>();
            PnrLog = new List<PnrLog>();
            Tickets = new List<string>();
        }

        public long? TripId { get; set; }
        public string TripUniqueNo { get; set; }
        public int? JournayType { get; set; }
        public System.DateTime JournayOnWardDate { get; set; }
        public DateTime? JournayReturnDate { get; set; }
        public int AirLineId { get; set; }
        public string AirLineCode { get; set; }
        public string AirLineFullName { get; set; }
        public string Origin { get; set; }
        public string OriginFullName { get; set; }
        public string Destination { get; set; }
        public string DestinationFullName { get; set; }
        public int NoOfTickets { get; set; }
        public int InitialNoOfTickets { get; set; }
        public bool IsIndividual { get; set; }
        public bool IsPublic { get; set; }
        public bool DirectTicketSell { get; set; }
        public int TicketStatus { get; set; }
        public int ClassType { get; set; }
        public string classTypeName { get; set; }
        public string ClassCode { get; set; }
        public string CRSName { get; set; }
        public bool IsActive { get; set; }
        public string CreatorUserId { get; set; }
        public string LoggedinUserId { get; set; }
        public List<SegmentVM> Segments { get; set; }
        public List<PriceBreakdown> PriceBreckdowns { get; set; }
        public List<PnrVM> Pnrs { get; set; }
        public List<PnrLog> PnrLog { get; set; }
        public List<string> Tickets { get; set; }

        //Agency Information
        public string AgencyName { get; set; }
        public string AgencyAddress { get; set; }
        public string AgentAddress { get; set; }
        public string AgenctContactNo { get; set; }

    }

    public class SegmentVM
    {
        public long? SegmentId { get; set; }
        public int SegmentType { get; set; }
        public long? TripId { get; set; }
        public DateTime? DepartureDate { get; set; }
        public DateTime? ArrivalDate { get; set; }
        public int? ClassType { get; set; }
        public string SegmentOrigin { get; set; }
        public string Destination { get; set; }
        public DateTime? TravelTime { get; set; }

        public string Duration { get; set; }
        public int SegmentSerialNo { get; set; }
        public string FlightNumber { get; set; }
        public string AirLines { get; set; }
        public string Baggage { get; set; }

        public string ClassTypeName { get; set; }
        public string Origin { get; set; }
        public string AirportFullNameForOrigin { get; set; }
        public string AirportFullNameForDestination { get; set; }

    }
    public class PriceBreckdownVM
    {
        public PriceBreckdownVM()
        {
            PriceBreakdownDetails =  new List<PriceBreakDownDetails>();
        }
        public long Id { get; set; }
        public string PriceSlotName { get; set; }
        public int PassengerLimit { get; set; }
        public decimal SingleAdultPrice { get; set; }
        public string Currency { get; set; }
        public int PriceNo { get; set; }
        public long TripId { get; set; }
        public int TotalTripTicketCount { get; set; }

        public int TotalSellCount { get; set; }
        public List<PriceBreakDownDetails> PriceBreakdownDetails { get; set; }
        public string CreatorUserId { get; set; }
        public string LoggedinUserId { get; set; }

        public bool IsPriceUsed { get; set; }
    }

    public class PriceBreakDownDetails
    {
        public long Id { get; set; }
        public long PriceBreakdownId { get; set; }
        public int PaxType { get; set; }
        public decimal? BaseFare { get; set; }
        public decimal? Tax { get; set; }
        public decimal? ServiceCharge { get; set; }
        public bool IsLive { get; set; }
    }

    public class PnrVM
    {
        public long? PnrId { get; set; }
        public long? TripId { get; set; }
        public string PnrValue { get; set; }
        public int? PnrCount { get; set; }
    }

    public class TicketUpdateVM : AuditEntity
    {

        public TicketUpdateVM()
        {
            TicketList = new List<TicketList>();
        }
        public long TripId { get; set; }

        public int NoOfTicket { get; set; }
        public int InitialNoOfTickets { get; set; }

        public string LoggedInUserId { get; set; }
        public List<TicketList> TicketList { get; set; }

    }

    public class TicketList
    {
        public long TripId { get; set; }
        public long PnrLogId { get; set; }
        public string TicketNo { get; set; }
        public string PassengerName { get; set; }
        public long TicketId { get; set; }

    }
}
