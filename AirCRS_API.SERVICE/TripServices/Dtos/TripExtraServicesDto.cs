﻿using AirCRS_API.DAL.CommonEntities;
using AirCRS_API.DAL.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirCRS_API.SERVICE.TripServices.Dtos
{
    public class TripExtraServicesDto
    {
        public TripExtraServicesDto()
        {
            MealsList = new List<MealsDto>();
            BaggageList = new List<BaggageDto>();
            WheelChaiList = new List<WheelChairDto>();
        }
        public long TripId { get; set; }
        public string LoggedInUserId { get; set; }
        public string CreatedBy { get; set; }

        public List<MealsDto> MealsList { get; set; }
        public List<BaggageDto> BaggageList { get; set; }
        public List<WheelChairDto> WheelChaiList { get; set; }
    }

    public class MealsDto : AuditEntity
    {
        public long Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public bool IsActive { get; set; }
        public long TripId { get; set; }
    }
    public class BaggageDto : AuditEntity
    {
        public long Id { get; set; }
        public BaggageType BaggageType { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
        public bool IsActive { get; set; }
        public long TripId { get; set; }
    }
    public class WheelChairDto : AuditEntity
    {
        public long Id { get; set; }
        public bool IsAvailable { get; set; }
        public decimal Price { get; set; }
        public bool IsActive { get; set; }
        public long TripId { get; set; }
    }

    public class ApiResponseBaggageModel
    {
        public long Id { get; set; }
        public string BaggageType { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
    }

    public class ApiResponseMealModel
    {
        public long Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
    
    }

    public class ApiResponseWheelChairModel
    {
        public long Id { get; set; }
        public bool IsAvailable { get; set; }
        public decimal Price { get; set; }

    }
}
