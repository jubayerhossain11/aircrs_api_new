﻿using AirCRS_API.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirCRS_API.SERVICE.TripServices.Dtos
{
    public class TripResponseDto
    {
        public TripResponseDto()
        {
            TripList = new List<TripVM>();
            CrsUsers = new List<CrsUser>();
        }

        public bool IsSysAdmin { get; set; }

        public List<TripVM> TripList { get; set; }
        public List<CrsUser> CrsUsers { get; set; }
    }
}
