﻿using AirCRS_API.DAL.Entities;
using AirCRS_API.SERVICE.AirPriceServices.Dtos;
using AirCRS_API.SERVICE.AirSearchApiServices.ApiRequestDots;
using AirCRS_API.SERVICE.AirSearchApiServices.Dtos;
using AirModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirCRS_API.SERVICE.AirSearchApiServices
{
    public interface IAirSearchApiService
    {
        List<AirSearchResponse> GerAirSearchData(AirSearchRequestInput airSearchRequest, DateTime FormatedDepartureDate, string loggedinUserId, int totalRequestedTickets);
        AirPriceCheckResponse GetAirPriceData(AirPriceCheckRequestInput airSearchRequest, ApiRequestLog getRequestLogDataByLogId, int priceBreakDownId);
        ApiRequestLog GetApiRequestLog(int requestLogId);
        AirCRSBookingResponse ApiBookingRequest(ApiRequestLog apiRequestlogData, AirBookRequestInput airBookDataFromRequest, int priceBreakDownId, string loggedInUserId);
        AirCRSExtraServiceResponse GetAirExtraService(ApiRequestLog apiRequestlogData, string loggedInUserId);
    }
}
