﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirCRS_API.SERVICE.AirSearchApiServices.ApiRequestDots
{
    public class AirCRSBookingResponse
    {
        public AirCRSBookingResponse()
        {
            AirTicketResponse = new AirModels.AirTicketResponse();
        }
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
        public AirModels.AirTicketResponse AirTicketResponse { get; set; }
    }
}
