﻿using AirModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirCRS_API.SERVICE.AirSearchApiServices.ApiRequestDots
{
    public class CRSTicketDataSyncResponse
    {
		public List<TicketInfo> TicketInfos { get; set; } = new List<TicketInfo>();
		public List<AirErrorResponse> Errors { get; set; } = new List<AirErrorResponse>();
		public string ReservationNumber { get; set; }
		public string SegmentCode { get; set; }
		public string AirItineraryCode { get; set; }
		public string IGXKey { get; set; }
	}
}
