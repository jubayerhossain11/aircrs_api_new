﻿using System.Collections.Generic;

namespace AirCRS_API.SERVICE.AirSearchApiServices.ApiRequestDots
{
    public class AirTicketRequestInput
    {
        public string AirPricingSolution_Key { get; set; }
        public List<string> ConfirmationNumbers;

        //pnr number
        public string ReservationNumber { get; set; }
    }
}