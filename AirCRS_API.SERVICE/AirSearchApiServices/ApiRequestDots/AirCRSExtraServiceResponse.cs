﻿using AirCRS_API.SERVICE.TripServices.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirCRS_API.SERVICE.AirSearchApiServices.ApiRequestDots
{
    public class AirCRSExtraServiceResponse
    {
        public List<ApiResponseMealModel> MealList { get; set; }
        public List<ApiResponseBaggageModel> BaggageList { get; set; }
        public ApiResponseWheelChairModel WheelChair { get; set; }
    }
}
