﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AirCRS_API.SERVICE.AirSearchApiServices.ApiRequestDots
{
    public class AirBookRequestInput
    {
        public List<PassengerInformation> PassengerInformations { get; set; }
        public string AirPricingSolution_Key { get; set; }

		public AgentInfo AgentInfo { get; set; }
	}

	public class AgentInfo
	{
		public bool IsAgent { get; set; }
		public string AgentName { get; set; }
		public string AgentAddress { get; set; }
		public string AgentEmail { get; set; }
		public string AgentCode { get; set; }
		public string AgentDomain { get; set; }
		public string Phone { get; set; }
		public string PostalCode { get; set; }
		public string CityName { get; set; }
		public string CountryName { get; set; }
		public string CountryCode { get; set; }

	}

	public class PassengerInformation
	{
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string Title { get; set; }
		public string Gender { get; set; }
		public string DOB { get; set; }
		public string Email { get; set; }
		public string PassengerContractNumber { get; set; }
		public string PassengerPassport { get; set; }
		public string PassengerPassportExpDate { get; set; }
		public string PassengerNationality { get; set; }
		public string PassengerFrequentFlyerNumber { get; set; }
		public string PassengerCode { get; set; }
		public string PassengerKey { get; set; }
		//public string AgencyAddress { get; set; }
		//public string AgencyPhone { get; set; }
		public string PhoneCountryCode { get; set; }
		//public string PostalCode { get; set; }
		public string CityName { get; set; }
		public string CountryName { get; set; }
		public string CountryCode { get; set; }
		public bool IsLeadPassenger { get; set; }
		public bool IsWheelchair { get; set; }
		public string DocumentType { get; set; }
		public SSRModel Meal { get; set; }
		public SSRModel WheelChair { get; set; }
		public SSRModel Seat { get; set; }

        public long? BaggageId { get; set; }
        public long? MealId { get; set; }
        public long? WheelChairId { get; set; }
    }

	public class SSRModel
	{
		public string Code { get; set; }
		public string Description { get; set; }
	}
}