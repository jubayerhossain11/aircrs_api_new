﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirCRS_API.SERVICE.AirSearchApiServices.ApiRequestDots
{
    public class AirExtraServicesRequestInput
    {
        public string AirPricingSolution_Key { get; set; }
    }
}
