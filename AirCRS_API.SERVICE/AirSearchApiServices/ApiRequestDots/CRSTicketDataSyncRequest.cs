﻿using AirModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirCRS_API.SERVICE.AirSearchApiServices.ApiRequestDots
{
    public class CRSTicketDataSyncRequest
    {
        public string ReservationNumber { get; set; }
        public List<ShortPassengerInfo> Passengers { get; set; } = new List<ShortPassengerInfo>();
        public IATA IATA { get; set; } = new IATA();
        public string SegmentCode { get; set; }
        public string AirItineraryCode { get; set; }
        public string IGXKey { get; set; }
    }
}
