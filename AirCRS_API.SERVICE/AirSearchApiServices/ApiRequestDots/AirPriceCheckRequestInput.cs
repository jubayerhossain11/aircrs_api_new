﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AirCRS_API.SERVICE.AirSearchApiServices.ApiRequestDots
{
    public class AirPriceCheckRequestInput
    {
        public string AirPricingSolution_Key { get; set; }
    }
}