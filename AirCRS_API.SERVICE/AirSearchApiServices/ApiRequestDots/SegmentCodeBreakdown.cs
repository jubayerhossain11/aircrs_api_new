﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirCRS_API.SERVICE.AirSearchApiServices.ApiRequestDots
{
    public class SegmentCodeBreakdown
    {
        public string JsonFileName { get; set; }
        public string AirPriceSolutionsKey { get; set; }
        public string ApiRef { get; set; }
    }
}
