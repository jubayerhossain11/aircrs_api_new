﻿using AirCRS_API.DAL.Entities;
using AirCRS_API.DAL.Enums;
using AirCRS_API.DAL.UnitOfWorks;
using AirCRS_API.SERVICE.AirPriceServices.Dtos;
using AirCRS_API.SERVICE.AirSearchApiServices.ApiRequestDots;
using AirCRS_API.SERVICE.AirSearchApiServices.Dtos;
using AirCRS_API.SERVICE.TripServices.Dtos;
using AirModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace AirCRS_API.SERVICE.AirSearchApiServices
{
    public class AirSearchApiService : IAirSearchApiService
    {
        private string EncryptionKey = "2@3G#kja$#kmm";
        private readonly IUoW _uow;
        public AirSearchApiService(IUoW uoW)
        {
            _uow = uoW;
        }


        public List<AirSearchResponse> GerAirSearchData(AirSearchRequestInput airSearchRequest, DateTime FormatedDepartureDate, string loggedinUserId, int totalRequestedTickets)
        {
            List<AirSearchResponse> airSearchResponse = new List<AirSearchResponse>();

            var getAllSegments = _uow.SegmentRepository.GetAll().Where(x => x.IsRemoved == false);
            var getAllAirCodes = _uow.AirportSearchCodeRepository.GetAll();

            var numberOfTrips = (from Trip in _uow.TripRepository.GetAll().Where(x => x.IsRemoved == false && x.NoOfTickets >= totalRequestedTickets && x.IsActive)
                                 join Air in _uow.AirlineRepository.GetAll()
                                 on Trip.AirLineId equals Air.Id
                                 join TO in _uow.AirportSearchCodeRepository.GetAll()
                                 on Trip.Origin equals TO.AirportCode
                                 join TD in _uow.AirportSearchCodeRepository.GetAll()
                                 on Trip.Destination equals TD.AirportCode
                                 join TS in _uow.SegmentRepository.GetAll().Where(x => x.IsRemoved == false)
                                 on Trip.TripId equals TS.TripId into segmentTemp
                                 join TPB in _uow.PriceBreakdownRepository.GetAll()
                                 on Trip.TripId equals TPB.TripId into priceBTemp
                                 join pnr in _uow.PnrRepository.GetAll()
                                 on Trip.TripId equals pnr.TripId
                                 join pnrLog in _uow.PnrLogRepository.GetAll()
                                 on pnr.PnrId equals pnrLog.PnrId into pnrLogTemp
                                 orderby Trip.TripId descending
                                 where Trip.JournayOnWardDate.Date == FormatedDepartureDate.Date &&
                                       Trip.JournayType == airSearchRequest.JourneyType &&
                                       Trip.Origin == airSearchRequest.Origin &&
                                       Trip.Destination == airSearchRequest.Destination &&
                                       (Trip.CreatedBy == loggedinUserId || Trip.IsPublic)
                                 select new TripVM()
                                 {
                                     TripId = Trip.TripId,
                                     AirLineId = Trip.AirLineId,
                                     AirLineCode = Air.Code,
                                     AirLineFullName = Air.AriLineName,
                                     ClassCode = Trip.ClassCode,
                                     classTypeName = Trip.ClassType == 1 ? "Economy" : "Business",
                                     Destination = Trip.Destination,
                                     DestinationFullName = TD.AirportCode,
                                     IsIndividual = Trip.IsIndividual,
                                     JournayOnWardDate = Trip.JournayOnWardDate,
                                     JournayReturnDate = Trip.JournayReturnDate,
                                     JournayType = Trip.JournayType,
                                     NoOfTickets = Trip.NoOfTickets,
                                     Origin = Trip.Origin,
                                     OriginFullName = TO.AirportCode,
                                     IsPublic = Trip.IsPublic,
                                     IsActive = Trip.IsActive,
                                     CreatorUserId = Trip.CreatedBy,
                                     PriceBreckdowns = priceBTemp.ToList(),
                                     PnrLog = pnrLogTemp.Where(x => x.UsesFlag != (int)PnrUsesType.Cancel).ToList()

                                     //PriceBreckdowns = priceBTemp.Select(x => new PriceBreckdownVM() { BaseFare = x.BaseFare, PaxType = x.PaxType, PriceBreckdownId = x.PriceBreckdownId, Tax = x.Tax, ServiceCharge = x.ServiceCharge, TripId = x.TripId }).ToList(),
                                 }).ToList();



            foreach (var trip in numberOfTrips)
            {
                List<FareBreakdown> fareBreakdown = new List<FareBreakdown>();
                List<FlightDescription> Onwards = new List<FlightDescription>();
                List<TotalTravelTime> totalTravelTimes = new List<TotalTravelTime>();

                //find lowest price 
                var priceBreakdown = new PriceBreakdown();

                foreach (var price in trip.PriceBreckdowns.OrderBy(X => X.SingleAdultPrice))
                {
                    var priceDetailsIds = price.PriceBreakdownDetails.Select(x => x.Id).ToArray();

                    if ((price.PassengerLimit - trip.PnrLog.Where(x=> priceDetailsIds.Contains(x.PriceBreakdownDetailsId) && x.UsesFlag != (int)PnrUsesType.Cancel).Count()) >= totalRequestedTickets)
                    {
                        if (airSearchRequest.NoOfChildren > 0 && (price.PriceBreakdownDetails.Where(x => x.PaxType == 2).FirstOrDefault().BaseFare <= 0 || price.PriceBreakdownDetails.Where(x => x.PaxType == 2).FirstOrDefault().BaseFare == null))
                        {
                            throw new Exception("Child Fare not found");
                        }
                        if (airSearchRequest.NoOfInfant > 0 && (price.PriceBreakdownDetails.Where(x => x.PaxType == 3).FirstOrDefault().BaseFare <= 0 || price.PriceBreakdownDetails.Where(x => x.PaxType == 3).FirstOrDefault().BaseFare == null))
                        {
                            throw new Exception("Infant Fare not found");
                        }
                        priceBreakdown = price;
                        break;
                    }
                }

                if (priceBreakdown == null || priceBreakdown.Id <= 0)
                {
                    throw new Exception("Fare not Available");
                }

                priceBreakdown.PriceBreakdownDetails = _uow.PriceBreakdownDetailsRepository.Get(x => x.PriceBreakdownId == priceBreakdown.Id).ToList();

                var totalBaseFare = (decimal)(priceBreakdown.PriceBreakdownDetails.Where(x => x.PaxType == 1).FirstOrDefault().BaseFare) * airSearchRequest.NoOfAdult +
                                  (decimal)(priceBreakdown.PriceBreakdownDetails.Where(x => x.PaxType == 2).FirstOrDefault().BaseFare) * airSearchRequest.NoOfChildren +
                                  (decimal)(priceBreakdown.PriceBreakdownDetails.Where(x => x.PaxType == 3).FirstOrDefault().BaseFare) * airSearchRequest.NoOfInfant;

                var totalTax = (decimal)(priceBreakdown.PriceBreakdownDetails.Where(x => x.PaxType == 1).FirstOrDefault().Tax) * airSearchRequest.NoOfAdult +
                              (decimal)(priceBreakdown.PriceBreakdownDetails.Where(x => x.PaxType == 2).FirstOrDefault().Tax) * airSearchRequest.NoOfChildren +
                              (decimal)(priceBreakdown.PriceBreakdownDetails.Where(x => x.PaxType == 3).FirstOrDefault().Tax == null ? 0 : priceBreakdown.PriceBreakdownDetails.Where(x => x.PaxType == 3).FirstOrDefault().Tax) * airSearchRequest.NoOfInfant;


                var tripInfo = new AirSearchResponse()
                {
                    Adults = airSearchRequest.NoOfAdult,
                    Childs = airSearchRequest.NoOfChildren,
                    Infants = airSearchRequest.NoOfInfant,
                    BasePrice = totalBaseFare,
                    TotalTax = totalTax,
                    TotalPrice = totalBaseFare + totalTax,
                    IsBookable = true,
                    IsRefundable = true,
                    IsTaxBreakdownAvailable = true,
                    HasOwnID = true,
                    PlatingCarrier = trip.AirLineCode,
                    PlatingCarrierName = trip.AirLineFullName,
                    TripType = trip.JournayType == 1 ? "One-Way" : "",
                };

                #region add PriceBreckdowns

                foreach (var price in priceBreakdown.PriceBreakdownDetails)
                {
                    var priceBreckdown = new FareBreakdown()
                    {

                        NoOfPassenger = price.PaxType == 1 ? airSearchRequest.NoOfAdult : price.PaxType == 2 ? airSearchRequest.NoOfChildren : airSearchRequest.NoOfInfant,
                        BaseFare = (decimal)price.BaseFare,
                        PassengerType = price.PaxType == 1 ? "ADT" : price.PaxType == 2 ? "CHD" : "INF",
                        TotalTax = (decimal)(price.Tax == null ? 0 : price.Tax),
                        TotalFare = (decimal)price.BaseFare + (decimal)(price.Tax == null ? 0 : price.Tax)
                    };

                    if (airSearchRequest.NoOfAdult > 0 && price.PaxType == 1)
                    {
                        fareBreakdown.Add(priceBreckdown);
                    }
                    else if (airSearchRequest.NoOfChildren > 0 && price.PaxType == 2)
                    {
                        fareBreakdown.Add(priceBreckdown);
                    }
                    else if (airSearchRequest.NoOfInfant > 0 && price.PaxType == 3)
                    {
                        fareBreakdown.Add(priceBreckdown);
                    }
                }

                #endregion

                #region add Segment

                var segments = (from s in getAllSegments
                                join ts in getAllAirCodes
                                on s.SegmentOrigin equals ts.AirportCode
                                where s.TripId == trip.TripId
                                orderby s.SegmentSerialNo ascending
                                select new SegmentOutputVM()
                                {
                                    TripId = s.TripId,
                                    ArrivalDate = s.ArrivalDate,
                                    DepartureDate = s.DepartureDate,
                                    Origin = ts.AirportCode,
                                    Duration = s.Duration,
                                    FlightNumber = s.FlightNumber,
                                    Baggage = s.Baggage,
                                    ClassType = s.ClassType,
                                    SegmentSerialNo = s.SegmentSerialNo,
                                    SegmentType = s.SegmentType,
                                    ClassTypeName = s.ClassType == 1 ? "Economy" : "Business",
                                }).ToList();

                var inatialSource = trip.Origin;
                var count = 0;
                var arivalAndDepartureDateCount = 0;

                DateTime? arivalFirst = null;
                DateTime? departureSecond = null;
                TimeSpan totalTravaltime = new TimeSpan(0, 0, 0, 0, 0);
                // var AirportTimes = UtilsGetAirLines.GetAllAirLinesData();
                string Time = "";

                foreach (var item in segments)
                {
                    Time = "";
                    TimeSpan segmentDuration = TimeSpan.Parse(item.Duration);
                    if (segmentDuration.Days > 0)
                        Time = segmentDuration.Days + "d " + segmentDuration.Hours + "h " + segmentDuration.Minutes + "m";
                    else if (segmentDuration.Hours > 0)
                        Time = segmentDuration.Hours + "h " + segmentDuration.Minutes + "m";
                    else
                        Time = segmentDuration.Minutes + "m";

                    var segmentItem = new FlightDescription()
                    {
                        Origin = inatialSource,
                        Destination = item.Origin,
                        AirBaggageAllowance = item.Baggage,
                        ArrivalTime = item.ArrivalDate.Value.ToString("dd-MMM-yyyy HH:mm tt"),
                        DepartureTime = item.DepartureDate.Value.ToString("dd-MMM-yyyy HH:mm tt"),
                        Carrier = trip.AirLineCode,
                        CarrierName = trip.AirLineFullName,
                        OperatingCarrier = trip.AirLineCode,
                        OperatingCarrierName = trip.AirLineFullName,
                        TravelDuration = Time,
                        CabinClass = item.ClassType == 1 ? "Economy" : "Business",
                        FlightNumber = item.FlightNumber,
                        OperatingFlightNumber = item.FlightNumber,
                        BookingCode = trip.ClassCode,
                    };

                    Onwards.Add(segmentItem);
                    //update source
                    inatialSource = segmentItem.Destination;
                    count++;

                    totalTravaltime = totalTravaltime + TimeSpan.Parse(item.Duration);

                    if (arivalAndDepartureDateCount == 0)
                        arivalFirst = item.ArrivalDate.Value;
                    if (arivalAndDepartureDateCount == 1)
                        departureSecond = item.DepartureDate.Value;

                    if (arivalFirst != null && departureSecond != null)
                    {
                        totalTravaltime = totalTravaltime + (departureSecond.Value - arivalFirst.Value);
                        arivalAndDepartureDateCount = 0;
                        arivalFirst = item.ArrivalDate;
                    }
                    if (segments.Count() == 1)
                    {
                        totalTravaltime = TimeSpan.Parse(item.Duration);
                    }

                    arivalAndDepartureDateCount++;
                }

                #endregion

                #region TotalTravelTime

                string Time2 = "";
                if (totalTravaltime.Days > 0)
                    Time2 = totalTravaltime.Days + "d " + totalTravaltime.Hours + "h " + totalTravaltime.Minutes + "m";
                else if (totalTravaltime.Hours > 0)
                    Time2 = totalTravaltime.Hours + "h " + totalTravaltime.Minutes + "m";
                else
                    Time2 = totalTravaltime.Minutes + "m";

                var travelTime = new TotalTravelTime()
                {
                    NoOfStop = segments.Count() > 1 ? segments.Count() - 1 : 0,
                    TotalTravelDuration = Time2,
                    TravelType = "onward"
                };

                totalTravelTimes.Add(travelTime);
                #endregion

                tripInfo.Onwards = Onwards;
                tripInfo.FareBreakdown = fareBreakdown;
                tripInfo.TotalTravelTimes = totalTravelTimes;

                airSearchResponse.Add(tripInfo);

                #region AddApiRequestLog

                var apiRequestLog = new ApiRequestLog()
                {
                    NumberOfAdult = airSearchRequest.NoOfAdult,
                    NumberOfChild = airSearchRequest.NoOfChildren,
                    NumberOfInfrant = airSearchRequest.NoOfInfant,
                    TotalPrice = tripInfo.TotalPrice,
                    TripId = trip.TripId.Value,
                    CreatedBy = loggedinUserId,
                    CreatedDate = DateTime.Now,
                    IsRemoved = false,
                    Status = (int)ApiRequestLogStatus.Search
                };

                _uow.ApiRequestLogRepository.Insert(apiRequestLog);
                _uow.ApiRequestLogRepository.Save();
                #endregion

                //add apiRequestId To the Api Response
                tripInfo.AirPricingSolution_Key = EncryptString($"{apiRequestLog.ApiRequestLogId}|{priceBreakdown.Id}", EncryptionKey);
            }

            return airSearchResponse;
        }

        public AirPriceCheckResponse GetAirPriceData(AirPriceCheckRequestInput airSearchRequest, ApiRequestLog getRequestLogDataByLogId, int priceBreakDownId)
        {
            AirPriceCheckResponse airPriceCheckResponse = new AirPriceCheckResponse();
            using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
            {
                #region Check Price

                var priceBreckdowns = _uow.PriceBreakdownRepository.Get(x => x.Id == priceBreakDownId).FirstOrDefault();
                var priceBrakDownDetailsIds = priceBreckdowns.PriceBreakdownDetails.Select(x => x.Id).ToArray();

                var getPnr = _uow.PnrRepository.Get(x => x.TripId == priceBreckdowns.TripId).FirstOrDefault();

                if((priceBreckdowns.PassengerLimit - _uow.PnrLogRepository.Get(x => priceBrakDownDetailsIds.Contains(x.PriceBreakdownDetailsId) && x.UsesFlag != (int)PnrUsesType.Cancel).Count()) < (getRequestLogDataByLogId.NumberOfAdult+ getRequestLogDataByLogId.NumberOfChild + getRequestLogDataByLogId.NumberOfInfrant))
                {
                    throw new Exception("Fare not Available");
                }

                var totalBaseFare = (decimal)(priceBreckdowns.PriceBreakdownDetails.Where(x => x.PaxType == 1).FirstOrDefault().BaseFare) * getRequestLogDataByLogId.NumberOfAdult +
                                       (decimal)(priceBreckdowns.PriceBreakdownDetails.Where(x => x.PaxType == 2).FirstOrDefault().BaseFare) * getRequestLogDataByLogId.NumberOfChild +
                                       (decimal)(priceBreckdowns.PriceBreakdownDetails.Where(x => x.PaxType == 3).FirstOrDefault().BaseFare) * getRequestLogDataByLogId.NumberOfInfrant;

                var totalTax = (decimal)(priceBreckdowns.PriceBreakdownDetails.Where(x => x.PaxType == 1).FirstOrDefault().Tax != null ?
                                     priceBreckdowns.PriceBreakdownDetails.Where(x => x.PaxType == 1).FirstOrDefault().Tax : 0) * getRequestLogDataByLogId.NumberOfAdult +
                              (decimal)(priceBreckdowns.PriceBreakdownDetails.Where(x => x.PaxType == 2).FirstOrDefault().Tax != null ?
                              priceBreckdowns.PriceBreakdownDetails.Where(x => x.PaxType == 2).FirstOrDefault().Tax : 0) * getRequestLogDataByLogId.NumberOfChild +
                              (decimal)(priceBreckdowns.PriceBreakdownDetails.Where(x => x.PaxType == 3).FirstOrDefault().Tax != null ?
                              priceBreckdowns.PriceBreakdownDetails.Where(x => x.PaxType == 3).FirstOrDefault().Tax : 0) * getRequestLogDataByLogId.NumberOfInfrant;


                if (getRequestLogDataByLogId.TotalPrice != (totalBaseFare + totalTax))
                {
                    airPriceCheckResponse.IsPriceChanged = true;
                }
                else
                {
                    airPriceCheckResponse.IsPriceChanged = false;
                }
                #endregion

                #region TripInfo

                var tripData = (from Trip in _uow.TripRepository.GetAll().Where(x => x.IsRemoved == false && x.IsActive && x.TripId == getRequestLogDataByLogId.TripId)
                                join Air in _uow.AirlineRepository.GetAll()
                                on Trip.AirLineId equals Air.Id
                                join TO in _uow.AirportSearchCodeRepository.GetAll()
                                on Trip.Origin equals TO.AirportCode
                                join TD in _uow.AirportSearchCodeRepository.GetAll()
                                on Trip.Destination equals TD.AirportCode
                                join TS in _uow.SegmentRepository.GetAll().Where(x => x.IsRemoved == false)
                                on Trip.TripId equals TS.TripId into segmentTemp
                                join TPB in _uow.PriceBreakdownRepository.GetAll()
                                 on Trip.TripId equals TPB.TripId into priceBTemp
                                orderby Trip.TripId descending
                                where Trip.TripId == getRequestLogDataByLogId.TripId
                                select new TripVM()
                                {
                                    TripId = Trip.TripId,
                                    AirLineId = Trip.AirLineId,
                                    AirLineCode = Air.Code,
                                    AirLineFullName = Air.AriLineName,
                                    ClassCode = Trip.ClassCode,
                                    classTypeName = Trip.ClassType == 1 ? "Economy" : "Business",
                                    Destination = Trip.Destination,
                                    DestinationFullName = TD.AirportCode,
                                    IsIndividual = Trip.IsIndividual,
                                    JournayOnWardDate = Trip.JournayOnWardDate,
                                    JournayReturnDate = Trip.JournayReturnDate,
                                    JournayType = Trip.JournayType,
                                    NoOfTickets = Trip.NoOfTickets,
                                    Origin = Trip.Origin,
                                    OriginFullName = TO.AirportCode,
                                    IsPublic = Trip.IsPublic,
                                    IsActive = Trip.IsActive,
                                    CreatorUserId = Trip.CreatedBy,
                                    PriceBreckdowns = priceBTemp.Where(p => p.Id == priceBreakDownId).ToList(),
                                    // PriceBreckdowns = priceBTemp.Select(x => new PriceBreckdownVM() { BaseFare = x.BaseFare, PaxType = x.PaxType, PriceBreckdownId = x.PriceBreckdownId, Tax = x.Tax, ServiceCharge = x.ServiceCharge, TripId = x.TripId }).ToList(),
                                }).FirstOrDefault();
                #endregion

                List<FareBreakdown> fareBreakdown = new List<FareBreakdown>();
                List<FlightDescription> Onwards = new List<FlightDescription>();
                List<TotalTravelTime> totalTravelTimes = new List<TotalTravelTime>();
                var getAllSegments = _uow.SegmentRepository.GetAll().Where(x => x.IsRemoved == false);
                var getAllAirCodes = _uow.AirportSearchCodeRepository.GetAll();

                airPriceCheckResponse.Adults = getRequestLogDataByLogId.NumberOfAdult.Value;
                airPriceCheckResponse.Childs = getRequestLogDataByLogId.NumberOfChild.Value;
                airPriceCheckResponse.Infants = getRequestLogDataByLogId.NumberOfInfrant.Value;
                airPriceCheckResponse.BasePrice = totalBaseFare.Value;
                airPriceCheckResponse.TotalTax = totalTax.Value;
                airPriceCheckResponse.TotalPrice = totalBaseFare.Value + totalTax.Value;
                airPriceCheckResponse.IsBookable = true;
                airPriceCheckResponse.IsRefundable = true;
                airPriceCheckResponse.IsTaxBreakdownAvailable = true;
                airPriceCheckResponse.HasOwnID = true;
                airPriceCheckResponse.PlatingCarrier = tripData.AirLineCode;
                airPriceCheckResponse.PlatingCarrierName = tripData.AirLineFullName;
                airPriceCheckResponse.TripType = tripData.JournayType == 1 ? "One-Way" : "";
                airPriceCheckResponse.AirPricingSolution_Key = airSearchRequest.AirPricingSolution_Key;

                #region add PriceBreckdowns

                foreach (var price in tripData.PriceBreckdowns.FirstOrDefault()?.PriceBreakdownDetails)
                {
                    var priceBreckdown = new FareBreakdown()
                    {

                        NoOfPassenger = price.PaxType == 1 ? getRequestLogDataByLogId.NumberOfAdult.Value : price.PaxType == 2 ? getRequestLogDataByLogId.NumberOfChild.Value : getRequestLogDataByLogId.NumberOfInfrant.Value,
                        BaseFare = (decimal)price.BaseFare,
                        PassengerType = price.PaxType == 1 ? "ADT" : price.PaxType == 2 ? "CHD" : "INF",
                        TotalTax = (decimal)(price.Tax == null ? 0 : price.Tax),
                        TotalFare = (decimal)price.BaseFare + (decimal)(price.Tax == null ? 0 : price.Tax)
                    };

                    if (getRequestLogDataByLogId.NumberOfAdult > 0 && price.PaxType == 1)
                    {
                        fareBreakdown.Add(priceBreckdown);
                    }
                    else if (getRequestLogDataByLogId.NumberOfChild > 0 && price.PaxType == 2)
                    {
                        fareBreakdown.Add(priceBreckdown);
                    }
                    else if (getRequestLogDataByLogId.NumberOfInfrant > 0 && price.PaxType == 3)
                    {
                        fareBreakdown.Add(priceBreckdown);
                    }
                }

                #endregion

                #region add Segment

                var segments = (from s in getAllSegments
                                join ts in getAllAirCodes
                                on s.SegmentOrigin equals ts.AirportCode
                                where s.TripId == getRequestLogDataByLogId.TripId
                                orderby s.SegmentSerialNo ascending
                                select new SegmentOutputVM()
                                {
                                    TripId = s.TripId,
                                    ArrivalDate = s.ArrivalDate,
                                    DepartureDate = s.DepartureDate,
                                    Origin = ts.AirportCode,
                                    Duration = s.Duration,
                                    FlightNumber = s.FlightNumber,
                                    Baggage = s.Baggage,
                                    ClassType = s.ClassType,
                                    SegmentSerialNo = s.SegmentSerialNo,
                                    SegmentType = s.SegmentType,
                                    ClassTypeName = s.ClassType == 1 ? "Economy" : "Business",
                                }).ToList();

                var inatialSource = tripData.Origin;
                var count = 0;
                var arivalAndDepartureDateCount = 0;

                DateTime? arivalFirst = null;
                DateTime? departureSecond = null;
                TimeSpan totalTravaltime = new TimeSpan(0, 0, 0, 0, 0);
                // var AirportTimes = UtilsGetAirLines.GetAllAirLinesData();
                string Time = "";

                foreach (var item in segments)
                {


                    Time = "";
                    TimeSpan segmentDuration = TimeSpan.Parse(item.Duration);
                    if (segmentDuration.Days > 0)
                        Time = segmentDuration.Days + "d " + segmentDuration.Hours + "h " + segmentDuration.Minutes + "m";
                    else if (segmentDuration.Hours > 0)
                        Time = segmentDuration.Hours + "h " + segmentDuration.Minutes + "m";
                    else
                        Time = segmentDuration.Minutes + "m";

                    var segmentItem = new FlightDescription()
                    {

                        Origin = inatialSource,
                        Destination = item.Origin,
                        AirBaggageAllowance = item.Baggage,
                        ArrivalTime = item.ArrivalDate.Value.ToString("dd-MMM-yyyy HH:mm tt"),
                        DepartureTime = item.DepartureDate.Value.ToString("dd-MMM-yyyy HH:mm tt"),
                        Carrier = tripData.AirLineCode,
                        CarrierName = tripData.AirLineFullName,
                        OperatingCarrier = tripData.AirLineCode,
                        OperatingCarrierName = tripData.AirLineFullName,
                        TravelDuration = Time,
                        CabinClass = item.ClassType == 1 ? "Economy" : "Business",
                        FlightNumber = item.FlightNumber,
                        OperatingFlightNumber = item.FlightNumber,
                        BookingCode = tripData.ClassCode,
                    };

                    Onwards.Add(segmentItem);
                    //update source
                    inatialSource = segmentItem.Destination;
                    count++;

                    totalTravaltime = totalTravaltime + TimeSpan.Parse(item.Duration);

                    if (arivalAndDepartureDateCount == 0)
                        arivalFirst = item.ArrivalDate.Value;
                    if (arivalAndDepartureDateCount == 1)
                        departureSecond = item.DepartureDate.Value;

                    if (arivalFirst != null && departureSecond != null)
                    {
                        totalTravaltime = totalTravaltime + (departureSecond.Value - arivalFirst.Value);
                        arivalAndDepartureDateCount = 0;
                        arivalFirst = item.ArrivalDate;
                    }
                    if (segments.Count() == 1)
                    {
                        totalTravaltime = TimeSpan.Parse(item.Duration);
                    }

                    arivalAndDepartureDateCount++;
                }

                #endregion

                #region TotalTravelTime

                var Time2 = "";
                if (totalTravaltime.Days > 0)
                    Time2 = totalTravaltime.Days + "d " + totalTravaltime.Hours + "h " + totalTravaltime.Minutes + "m";
                else if (totalTravaltime.Hours > 0)
                    Time2 = totalTravaltime.Hours + "h " + totalTravaltime.Minutes + "m";
                else
                    Time2 = totalTravaltime.Minutes + "m";

                var travelTime = new TotalTravelTime()
                {
                    NoOfStop = segments.Count() > 1 ? segments.Count() - 1 : 0,
                    TotalTravelDuration = Time2,
                    TravelType = "onward"
                };

                totalTravelTimes.Add(travelTime);
                #endregion

                airPriceCheckResponse.Onwards = Onwards;
                airPriceCheckResponse.FareBreakdown = fareBreakdown;
                airPriceCheckResponse.TotalTravelTimes = totalTravelTimes;


                //update Api Request log 

                getRequestLogDataByLogId.Status = (int)ApiRequestLogStatus.CheckingPrice;
                _uow.ApiRequestLogRepository.Update(getRequestLogDataByLogId);
                _uow.ApiRequestLogRepository.Save();

                scope.Complete();
            }

            return airPriceCheckResponse;
        }

        public AirCRSBookingResponse ApiBookingRequest(ApiRequestLog apiRequestlogData, AirBookRequestInput airBookDataFromRequest, int priceBreakDownId, string loggedInUserId)
        {
            AirCRSBookingResponse airCrsBookingResponse = new AirCRSBookingResponse();

            using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
            {
                var numberOfTicketsRequire = apiRequestlogData.NumberOfAdult + apiRequestlogData.NumberOfChild + apiRequestlogData.NumberOfInfrant;

                bool isAdultNotEqualToLocalData = airBookDataFromRequest.PassengerInformations.Where(x => x.PassengerCode == "ADT").Count() == apiRequestlogData.NumberOfAdult ? false : true;
                bool isChildNotEqualToLocalData = airBookDataFromRequest.PassengerInformations.Where(x => x.PassengerCode == "CNN" || x.PassengerCode == "CHD").Count() == apiRequestlogData.NumberOfChild ? false : true;
                bool isInfrantNotEqualToLocalData = airBookDataFromRequest.PassengerInformations.Where(x => x.PassengerCode == "INF").Count() == apiRequestlogData.NumberOfInfrant ? false : true;

                //request Passenger Validation
                if (airBookDataFromRequest.PassengerInformations.Count != numberOfTicketsRequire || isAdultNotEqualToLocalData || isChildNotEqualToLocalData || isInfrantNotEqualToLocalData)
                    throw new Exception("Invalid Combination of Adult Child And Infrant Please try with valid Information !!");


                var priceBreckdowns = _uow.PriceBreakdownRepository.Get(x => x.Id == priceBreakDownId).FirstOrDefault();
                var priceBrakDownDetailsIds = priceBreckdowns.PriceBreakdownDetails.Select(x => x.Id).ToArray();

                var getPnr = _uow.PnrRepository.Get(x => x.TripId == priceBreckdowns.TripId).FirstOrDefault();

                if ((priceBreckdowns.PassengerLimit - _uow.PnrLogRepository.Get(x => priceBrakDownDetailsIds.Contains(x.PriceBreakdownDetailsId) && x.UsesFlag != (int)PnrUsesType.Cancel).Count()) < numberOfTicketsRequire)
                {
                    throw new Exception("Fare not found");
                }


                List<string> confirmationNumbers = new List<string>();
                List<TicketInfo> ticketInfoes = new List<TicketInfo>();


                var tripData = _uow.TripRepository.Get(x => x.TripId == apiRequestlogData.TripId).FirstOrDefault();
                if (airBookDataFromRequest.PassengerInformations.Count != numberOfTicketsRequire || numberOfTicketsRequire > tripData.NoOfTickets)
                {
                    //update Api Request log 

                    apiRequestlogData.Status = (int)ApiRequestLogStatus.BookingCanceled;
                    _uow.ApiRequestLogRepository.Insert(apiRequestlogData);
                    _uow.ApiRequestLogRepository.Save();

                    throw new Exception("Passenger count not matched");
                }
                else
                {
                    var tripPnr = _uow.PnrRepository.Get(x => x.TripId == apiRequestlogData.TripId).FirstOrDefault();
                    var noOfFreeTicket = _uow.TicketRepository.Get(x => x.TripId == apiRequestlogData.TripId && x.TStatus == (int)TicketStatus.Free).ToList();



                    if (tripData.DirectTicketSell == true && airBookDataFromRequest.PassengerInformations.Count > noOfFreeTicket.Count)
                    {
                        throw new Exception("Insufficient ticket found !!");
                    }
                    var tripPriceBreckDown = _uow.PriceBreakdownRepository.Get(x => x.TripId == apiRequestlogData.TripId && x.Id == priceBreakDownId).FirstOrDefault();

                    int numberOfAdultRequest = apiRequestlogData.NumberOfAdult.Value;
                    int numberOfChildRequest = apiRequestlogData.NumberOfChild.Value;
                    int numberOfInfrantRequest = apiRequestlogData.NumberOfInfrant.Value;
                    int addultCount = 1;
                    int childsCount = 1;
                    int infrantsCount = 1;

                    foreach (var item in airBookDataFromRequest.PassengerInformations)
                    {

                        //save Passenger

                        var passenger = new Passenger()
                        {
                            ContractNumber = item.PassengerContractNumber,
                            CreatedBy = loggedInUserId,
                            CreatedDate = DateTime.Now,
                            Email = item.Email,
                            Gender = item.Gender,
                            IsRemoved = false,
                            //Name = item.FirstName + " " + item.LastName,
                            FirstName = item.FirstName,
                            LastName = item.LastName,
                            Nationality = item.PassengerNationality,
                            PassportNumber = item.PassengerPassport,
                            PassengerCode = item.PassengerCode,
                            BaggageId = item.BaggageId,
                            MealId = item.MealId,
                            WheelChairId = item.WheelChairId,
                            //DOB = !string.IsNullOrWhiteSpace(item.DOB) ? Convert.ToDateTime(item.DOB) : new DateTime(),
                            Title = item.Title,
                            PassengerType = item.PassengerCode,
                            FrequentFlyerNumber = item.PassengerFrequentFlyerNumber,
                            //PassportExpDate = !string.IsNullOrWhiteSpace(item.PassengerPassportExpDate) ? Convert.ToDateTime(item.PassengerPassportExpDate) : new DateTime(),
                        };

                        if (!string.IsNullOrEmpty(item.DOB))
                            passenger.DOB = DateTime.Parse(item.DOB);

                        if (!string.IsNullOrEmpty(item.PassengerPassportExpDate))
                            passenger.PassportExpDate = DateTime.Parse(item.PassengerPassportExpDate);


                        _uow.PassengerRepository.Insert(passenger);
                        _uow.PassengerRepository.Save();

                        var pnrLog = new PnrLog()
                        {
                            CreatedBy = loggedInUserId,
                            CreatedDate = DateTime.Now,
                            IsRemoved = false,
                            IsSeen = false,
                            PnrId = tripPnr.PnrId,
                            UsesFlag = (int)PnrUsesType.Booked
                        };

                        var tripMealForPassenger = _uow.MealRepository.Get(x => x.Id == item.MealId).FirstOrDefault();
                        var tripBaggageForPassenger = _uow.BaggageRepository.Get(x => x.Id == item.BaggageId).FirstOrDefault();
                        var tripWheelChairForPassenger = _uow.WheelChairRepository.Get(x => x.Id == item.WheelChairId).FirstOrDefault();



                        if (numberOfAdultRequest >= addultCount && item.PassengerCode == "ADT")
                        {
                            pnrLog.PriceBreakdownDetailsId = tripPriceBreckDown.PriceBreakdownDetails.Where(x => x.PaxType == 1).FirstOrDefault().Id;
                            pnrLog.TotalPrice = (decimal)((tripPriceBreckDown.PriceBreakdownDetails.Where(x => x.PaxType == 1).FirstOrDefault().BaseFare != null ?
                                tripPriceBreckDown.PriceBreakdownDetails.Where(x => x.PaxType == 1).FirstOrDefault().BaseFare : 0) +
                                (tripPriceBreckDown.PriceBreakdownDetails.Where(x => x.PaxType == 1).FirstOrDefault().Tax != null ?
                                tripPriceBreckDown.PriceBreakdownDetails.Where(x => x.PaxType == 1).FirstOrDefault().Tax : 0) +
                                (tripPriceBreckDown.PriceBreakdownDetails.Where(x => x.PaxType == 1).FirstOrDefault().ServiceCharge != null ?
                                tripPriceBreckDown.PriceBreakdownDetails.Where(x => x.PaxType == 1).FirstOrDefault().ServiceCharge : 0)) +
                                 (tripMealForPassenger != null ? tripMealForPassenger.Price : 0) + (tripBaggageForPassenger != null ? tripBaggageForPassenger.Price : 0) + (tripWheelChairForPassenger != null ? tripWheelChairForPassenger.Price : 0);
                            pnrLog.PassengerId = passenger.PassengerId;
                            addultCount++;
                        }
                        else if (numberOfChildRequest >= childsCount && (item.PassengerCode == "CHD" || item.PassengerCode == "CNN"))
                        {
                            pnrLog.PriceBreakdownDetailsId = tripPriceBreckDown.PriceBreakdownDetails.Where(x => x.PaxType == 2).FirstOrDefault().Id;
                            pnrLog.TotalPrice = (decimal)((tripPriceBreckDown.PriceBreakdownDetails.Where(x => x.PaxType == 2).FirstOrDefault().BaseFare != null ?
                                    tripPriceBreckDown.PriceBreakdownDetails.Where(x => x.PaxType == 2).FirstOrDefault().BaseFare : 0) +
                                    (tripPriceBreckDown.PriceBreakdownDetails.Where(x => x.PaxType == 2).FirstOrDefault().Tax != null ?
                                    tripPriceBreckDown.PriceBreakdownDetails.Where(x => x.PaxType == 2).FirstOrDefault().Tax : 0) +
                                    (tripPriceBreckDown.PriceBreakdownDetails.Where(x => x.PaxType == 2).FirstOrDefault().ServiceCharge != null ?
                                    tripPriceBreckDown.PriceBreakdownDetails.Where(x => x.PaxType == 2).FirstOrDefault().ServiceCharge : 0)) + (tripMealForPassenger != null ? tripMealForPassenger.Price : 0) + (tripBaggageForPassenger != null ? tripBaggageForPassenger.Price : 0) + (tripWheelChairForPassenger != null ? tripWheelChairForPassenger.Price : 0);
                            pnrLog.PassengerId = passenger.PassengerId;
                            childsCount++;
                        }
                        else if (numberOfInfrantRequest >= infrantsCount && item.PassengerCode == "INF")
                        {
                            pnrLog.PriceBreakdownDetailsId = tripPriceBreckDown.PriceBreakdownDetails.Where(x => x.PaxType == 3).FirstOrDefault().Id;
                            pnrLog.TotalPrice = (decimal)((tripPriceBreckDown.PriceBreakdownDetails.Where(x => x.PaxType == 3).FirstOrDefault().BaseFare != null ?
                                  tripPriceBreckDown.PriceBreakdownDetails.Where(x => x.PaxType == 3).FirstOrDefault().BaseFare : 0) +
                                  (tripPriceBreckDown.PriceBreakdownDetails.Where(x => x.PaxType == 3).FirstOrDefault().Tax != null ?
                                  tripPriceBreckDown.PriceBreakdownDetails.Where(x => x.PaxType == 3).FirstOrDefault().Tax : 0) +
                                  (tripPriceBreckDown.PriceBreakdownDetails.Where(x => x.PaxType == 3).FirstOrDefault().ServiceCharge != null ?
                                  tripPriceBreckDown.PriceBreakdownDetails.Where(x => x.PaxType == 3).FirstOrDefault().ServiceCharge : 0)) +
                                   (tripMealForPassenger != null ? tripMealForPassenger.Price : 0) + (tripBaggageForPassenger != null ? tripBaggageForPassenger.Price : 0) + (tripWheelChairForPassenger != null ? tripWheelChairForPassenger.Price : 0);
                            pnrLog.Passenger = passenger;
                            infrantsCount++;
                        }

                        _uow.PnrLogRepository.Insert(pnrLog);
                        _uow.PnrLogRepository.Save();


                        #region generate Ticket
                        if (tripData.DirectTicketSell == true)
                        {
                            var pnr = _uow.PnrRepository.Get(x => x.PnrId == pnrLog.PnrId).FirstOrDefault();
                            var freeTicket = _uow.TicketRepository.Get(x => x.TripId == pnr.TripId && x.TStatus == (int)TicketStatus.Free).FirstOrDefault();

                            List<string> ticketNumber = new List<string>();
                            if (freeTicket == null)
                            {
                                throw new Exception("No free ticket found !!");
                            }
                            else
                            {
                                ticketNumber.Add(freeTicket.TicketNumber);
                            }

                            TicketInfo ticket = new TicketInfo()
                            {
                                PassengerName = $"{passenger.Title} {passenger.FirstName} {passenger.FirstName}",
                                ReservationNumber = pnr.PnrValue,
                                TicketNumbers = ticketNumber
                            };

                            //update Ticket Status
                            freeTicket.TStatus = (int)TicketStatus.Used;
                            _uow.TicketRepository.Update(freeTicket);
                            _uow.TicketRepository.Save();


                            //update PnrLog
                            pnrLog.UsesFlag = (int)PnrUsesType.Booked;
                            pnrLog.TicketNumber = freeTicket.TicketNumber;
                            _uow.PnrLogRepository.Update(pnrLog);
                            _uow.PnrLogRepository.Save();

                            ticketInfoes.Add(ticket);
                            confirmationNumbers.Add(pnr.PnrValue);
                        }
                        else
                        {
                            var extraServices = new List<ExtraServiceItem>();
                            var getExtraBaggageInfo = _uow.BaggageRepository.Get(x => x.Id == item.BaggageId).FirstOrDefault();
                            if(getExtraBaggageInfo != null)
                            {
                                var baggageItem = new ExtraServiceItem() { 
                                ExtraServiceType = "Baggage",
                                ItemDescription = $"{getExtraBaggageInfo.Quantity} {getExtraBaggageInfo.BaggageType.ToString()}"
                                };

                                extraServices.Add(baggageItem);
                            }
                            
                            var getExtraMeal = _uow.MealRepository.Get(x => x.Id == item.MealId).FirstOrDefault();
                            if(getExtraMeal != null)
                            {
                                var mealItem = new ExtraServiceItem() { 
                                ExtraServiceType = "Meal",
                                ItemDescription = $"{getExtraMeal.Code} - {getExtraMeal.Name}"
                                };
                                extraServices.Add(mealItem);
                            }

                            var wheelChair = _uow.WheelChairRepository.Get(x => x.Id == item.WheelChairId).FirstOrDefault();
                            if (wheelChair != null)
                            {
                                var wheelChairItem = new ExtraServiceItem()
                                {
                                    ExtraServiceType = "WheelChair",
                                };
                                extraServices.Add(wheelChairItem);
                            }


                            var pnr = _uow.PnrRepository.Get(x => x.PnrId == pnrLog.PnrId).FirstOrDefault();
                            List<string> ticketNumber = new List<string>();
                            ticketNumber.Add(pnr.PnrValue);
                            TicketInfo ticket = new TicketInfo()
                            {
                                PassengerName = $"{passenger.Title} {passenger.FirstName} {passenger.LastName}",
                                ReservationNumber = pnr.PnrValue,
                                TicketNumbers = ticketNumber,
                                ExtraServiceItems = extraServices
                            };

                            ticketInfoes.Add(ticket);
                            confirmationNumbers.Add(pnr.PnrValue);
                        }

                        #endregion
                    }

                    //update Trip Ticket Inventory
                    //var tripInfo = unit.Trips.Where(x => x.TripId == apiRequestlogData.TripId).FirstOrDefault();
                    //tripInfo.NoOfTickets = tripInfo.NoOfTickets - (apiRequestlogData.NumberOfAdult.Value + apiRequestlogData.NumberOfChild.Value + apiRequestlogData.NumberOfInfrant.Value);
                    //unit.Entry(tripInfo).State = EntityState.Modified;
                    //unit.SaveChanges();

                    //update Api Request log 
                    apiRequestlogData.Status = tripData.DirectTicketSell == true ? (int)ApiRequestLogStatus.Ticketing : (int)ApiRequestLogStatus.Booking;
                    _uow.ApiRequestLogRepository.Update(apiRequestlogData);
                    _uow.ApiRequestLogRepository.Save();

                    scope.Complete();

                    airCrsBookingResponse.AirTicketResponse.ConfirmationNumbers = confirmationNumbers;
                    airCrsBookingResponse.AirTicketResponse.TicketInfoes = ticketInfoes;
                    airCrsBookingResponse.IsSuccess = true;
                    airCrsBookingResponse.Message = tripData.DirectTicketSell == true ? "Ticket Generated Successfully" : "Ticketed Successfully, Check back later for Ticket No.";

                    return airCrsBookingResponse;
                }
            }
        }

        public AirCRSExtraServiceResponse GetAirExtraService(ApiRequestLog apiRequestlogData, string loggedInUserId)
        {
            var output = (from trip in _uow.TripRepository.GetAll()
                          join meal in _uow.MealRepository.Get(x => x.IsActive)
                          on trip.TripId equals meal.TripId into MealTemp
                          join baggage in _uow.BaggageRepository.Get(x => x.IsActive)
                          on trip.TripId equals baggage.TripId into BaggageTemp
                          where trip.TripId == apiRequestlogData.TripId
                          join wheelChair in _uow.WheelChairRepository.GetAll()
                          on trip.TripId equals wheelChair.TripId into WheelChairTemp
                          select new AirCRSExtraServiceResponse()
                          {
                              BaggageList = BaggageTemp.Select(b => new ApiResponseBaggageModel()
                              {
                                  Id = b.Id,
                                  Price = b.Price,
                                  Quantity = b.Quantity,
                                  BaggageType = ((BaggageType)b.BaggageType).ToString()
                              }).ToList(),
                              MealList = MealTemp.Select(b => new ApiResponseMealModel()
                              {
                                  Id = b.Id,
                                  Price = b.Price,
                                  Code = b.Code,
                                  Name = b.Name
                              }).ToList(),
                              WheelChair = WheelChairTemp.Where(x => x.IsActive).Select(b => new ApiResponseWheelChairModel()
                              {
                                  Id = b.Id,
                                  IsAvailable = b.IsAvailable,
                                  Price = b.Price
                              }).FirstOrDefault(),
                          }).FirstOrDefault();

            return output;
        }


        public ApiRequestLog GetApiRequestLog(int requestLogId)
        {
            return _uow.ApiRequestLogRepository.Get(x => x.ApiRequestLogId == requestLogId && (x.Status == (int)ApiRequestLogStatus.Search || x.Status == (int)ApiRequestLogStatus.CheckingPrice)).FirstOrDefault();
        }


        #region Encrypt Or Decrypt

        private string EncryptString(string Message, string Passphrase)
        {
            byte[] Results;
            System.Text.UTF8Encoding UTF8 = new System.Text.UTF8Encoding();
            MD5CryptoServiceProvider HashProvider = new MD5CryptoServiceProvider();
            byte[] TDESKey = HashProvider.ComputeHash(UTF8.GetBytes(Passphrase));
            TripleDESCryptoServiceProvider TDESAlgorithm = new TripleDESCryptoServiceProvider();
            TDESAlgorithm.Key = TDESKey;
            TDESAlgorithm.Mode = CipherMode.ECB;
            TDESAlgorithm.Padding = PaddingMode.PKCS7;
            byte[] DataToEncrypt = UTF8.GetBytes(Message);
            try
            {
                ICryptoTransform Encryptor = TDESAlgorithm.CreateEncryptor();
                Results = Encryptor.TransformFinalBlock(DataToEncrypt, 0, DataToEncrypt.Length);
            }
            finally
            {
                TDESAlgorithm.Clear();
                HashProvider.Clear();
            }
            return Convert.ToBase64String(Results);
        }

        public static string DecryptString(string encrString)
        {
            byte[] b;
            string decrypted;
            try
            {
                b = Convert.FromBase64String(encrString);
                decrypted = System.Text.ASCIIEncoding.ASCII.GetString(b);
            }
            catch (FormatException fe)
            {
                decrypted = "";
            }
            return decrypted;
        }

        private string DecryptString(string Message, string Passphrase)
        {
            byte[] Results;
            System.Text.UTF8Encoding UTF8 = new System.Text.UTF8Encoding();
            MD5CryptoServiceProvider HashProvider = new MD5CryptoServiceProvider();
            byte[] TDESKey = HashProvider.ComputeHash(UTF8.GetBytes(Passphrase));
            TripleDESCryptoServiceProvider TDESAlgorithm = new TripleDESCryptoServiceProvider();
            TDESAlgorithm.Key = TDESKey;
            TDESAlgorithm.Mode = CipherMode.ECB;
            TDESAlgorithm.Padding = PaddingMode.PKCS7;
            byte[] DataToDecrypt = Convert.FromBase64String(Message);
            try
            {
                ICryptoTransform Decryptor = TDESAlgorithm.CreateDecryptor();
                Results = Decryptor.TransformFinalBlock(DataToDecrypt, 0, DataToDecrypt.Length);
            }
            finally
            {
                TDESAlgorithm.Clear();
                HashProvider.Clear();
            }
            return UTF8.GetString(Results);
        }

        private HttpResponseMessage JsonSerialize<T>(T obj)
        {
            string json = JsonConvert.SerializeObject(obj);

            var resp = new HttpResponseMessage()
            {
                Content = new StringContent(json)
            };
            resp.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            return resp;
        }

        #endregion

    }
}
