﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirCRS_API.SERVICE.AirSearchApiServices.Dtos
{
    public class AirSearchRequestInput
    {
        public int NoOfChildren { get; set; }
        public int NoOfInfant { get; set; }
        public string ClassType { get; set; }
        public string ReturnDate { get; set; }
        public string DepartureDate { get; set; }
        public string Destination { get; set; }
        public string Origin { get; set; }
        public int JourneyType { get; set; }
        public int NoOfAdult { get; set; }
        public bool IsDomestic { get; set; }
        public string UserCurrency { get; set; }
        public string SortingTypeCode { get; set; }
    }
}
