﻿using AirCRS_API.DAL.Entities;
using AirCRS_API.DAL.UnitOfWorks;
using AirCRS_API.SERVICE.SegmentServices.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirCRS_API.SERVICE.SegmentServices
{
    public class SegmentService: ISegmentService
    {
        private readonly IUoW _uow;
        public SegmentService(IUoW uow)
        {
            _uow = uow;
        }
        public List<SegmentOutputDto> GetSegmentByTrip(Segment input)
        {
            List<SegmentOutputDto> output = new List<SegmentOutputDto>();

            var tripInfo = _uow.TripRepository.GetByID(input.SegmentId);

            var setments = (from s in _uow.SegmentRepository.GetAll().Where(x => x.IsRemoved == false)
                            join ts in _uow.AirportSearchCodeRepository.GetAll()
                            on s.SegmentOrigin equals ts.AirportCode
                            where s.TripId == tripInfo.TripId
                            orderby s.SegmentSerialNo ascending
                            select new SegmentOutputDto()
                            {
                                TripId = s.TripId,
                                ArrivalDate = s.ArrivalDate,
                                DepartureDate = s.DepartureDate,
                                Origin = ts.AirportCode,
                                Duration = s.Duration,
                                FlightNumber = s.FlightNumber,
                                Baggage = s.Baggage,
                                ClassType = s.ClassType,
                                SegmentSerialNo = s.SegmentSerialNo,
                                SegmentType = s.SegmentType,
                                ClassTypeName = s.ClassType == 1 ? "Economy" : "Business",
                            }).ToList();

            var inatialSource = tripInfo.Origin;
            var count = 0;

            foreach (var item in setments)
            {
                var segmentItem = new SegmentOutputDto();

                segmentItem.Origin = inatialSource;
                segmentItem.Destination = item.Origin;
                segmentItem.ArrivalDate = item.ArrivalDate;
                segmentItem.DepartureDate = item.DepartureDate;

                output.Add(segmentItem);
                //update source
                inatialSource = segmentItem.Destination;
                count++;
            }

            return output;
        }
    }
}
