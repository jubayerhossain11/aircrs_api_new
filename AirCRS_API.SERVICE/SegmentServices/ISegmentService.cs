﻿using AirCRS_API.DAL.Entities;
using AirCRS_API.SERVICE.SegmentServices.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirCRS_API.SERVICE.SegmentServices
{
    public interface ISegmentService
    {
        List<SegmentOutputDto> GetSegmentByTrip(Segment input);
    }
}
