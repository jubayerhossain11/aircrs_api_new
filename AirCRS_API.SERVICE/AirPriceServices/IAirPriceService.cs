﻿using AirCRS_API.SERVICE.AirPriceServices.Dtos;
using AirCRS_API.SERVICE.AirSearchApiServices.ApiRequestDots;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirCRS_API.SERVICE.AirPriceServices
{
    public interface IAirPriceService
    {
        AirPriceCheckDto GetAirPriceRequest(AirPriceCheckRequestInput airSearchRequest);
    }
}
