﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirCRS_API.SERVICE.AirPriceServices.Dtos
{
    public class ApiOriginalPriceDto
    {
        public decimal TotalPrice { get; set; }
        public decimal BasePrice { get; set; }
        public decimal TotalTax { get; set; }
        public decimal AdditionalPrice { get; set; }
        public decimal ConvertionRate { get; set; }
    }
}
