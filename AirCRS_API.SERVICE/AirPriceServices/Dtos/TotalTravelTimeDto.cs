﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirCRS_API.SERVICE.AirPriceServices.Dtos
{
    public class TotalTravelTimeDto
    {
        public string TravelType { get; set; }
        public string TotalTravelDuration { get; set; }
        public int NoOfStop { get; set; }
    }
}
