﻿using AirCRS_API.DAL.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirCRS_API.SERVICE.AirPriceServices.Dtos
{
    public class AdditionalChargeDto
    {
        public string Source { get; set; }
        public DateTime? RoleEndDate { get; set; }
        public DateTime? RoleStartDate { get; set; }
        public TimeSpan? EndTime { get; set; }
        public TimeSpan? StartTime { get; set; }
        public string AirLineCode { get; set; }
        public string Destination { get; set; }
        public AmountType ServiceChargeType { get; set; }
        public decimal Commission { get; set; }
        public AmountType CommissionType { get; set; }
        public decimal Markup { get; set; }
        public AmountType MarkupType { get; set; }
        public string ApiCode { get; set; }
        public int UserId { get; set; }
        public decimal ServiceCharge { get; set; }
        public string ChargeType { get; set; }
    }
}
