﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirCRS_API.SERVICE.AirPriceServices.Dtos
{
    public class AirPriceCheckRequestDto
    {
        public int JourneyType { get; set; }
        public string Origin { get; set; }
        public string Destination { get; set; }
        public string DepartureDate { get; set; }
        public string ReturnDate { get; set; }
        public string ClassType { get; set; }
        public int NoOfInfant { get; set; }
        public int NoOfChildren { get; set; }
        public int NoOfAdult { get; set; }
        public string SegmentCode { get; set; }
        public List<AdditionalChargeDto> AdditionalCharges { get; set; }
        public IATADto IATA { get; set; }
        public List<ConvertionRateDto> ConvertionRates { get; set; }
        public string UserCurrency { get; set; }
        public string IGXKey { get; set; }
    }
}
