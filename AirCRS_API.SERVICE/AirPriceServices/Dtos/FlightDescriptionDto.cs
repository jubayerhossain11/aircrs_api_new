﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirCRS_API.SERVICE.AirPriceServices.Dtos
{
    public class FlightDescriptionDto
    {
        public string Currency { get; set; }
        public string FareBasis { get; set; }
        public string CabinClass { get; set; }
        public string BookingCount { get; set; }
        public string BookingCode { get; set; }
        public string DestinationTerminal { get; set; }
        public string OriginTerminal { get; set; }
        public string OperatingFlightNumber { get; set; }
        public string OperatingCarrierName { get; set; }
        public string OperatingCarrier { get; set; }
        public string AirBaggageAllowance { get; set; }
        public string AvailabilitySource { get; set; }
        public string TravelDuration { get; set; }
        public string ArrivalTime { get; set; }
        public string DepartureTime { get; set; }
        public string Destination { get; set; }
        public string Origin { get; set; }
        public string FlightNumber { get; set; }
        public string CarrierName { get; set; }
        public string Carrier { get; set; }
        public int Group { get; set; }
        public string AirSegment_Key { get; set; }
        public string Distance { get; set; }
        public string Equipment { get; set; }
    }
}
