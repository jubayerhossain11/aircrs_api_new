﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirCRS_API.SERVICE.AirPriceServices.Dtos
{
    public class FareBreakdownDto
    {
        public List<TaxDto> TaxesBreakdown;
        public string PassengerType { get; set; }
        public decimal TotalFare { get; set; }
        public int NoOfPassenger { get; set; }
        public decimal BaseFare { get; set; }
        public decimal TotalTax { get; set; }
    }
}
