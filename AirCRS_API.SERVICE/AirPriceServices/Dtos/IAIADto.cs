﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirCRS_API.SERVICE.AirPriceServices.Dtos
{
    public class IATADto
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string TargetBranch { get; set; }
        public string GDSCode { get; set; }
        public string URL { get; set; }
        public string ApiRef { get; set; }
        public string ClientId { get; set; }
        public string EndUserIp { get; set; }
        public string AdditionalInfoJson { get; set; }
    }
}
