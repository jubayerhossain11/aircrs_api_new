﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirCRS_API.SERVICE.AirPriceServices.Dtos
{
    public class AirPriceCheckDto : AirSearchDto
    {
        public ApiOriginalPriceDto ApiOriginalPrice;
        public bool IsPriceChanged { get; set; }
        public string AirItineraryCode { get; set; }
        public string IGXKey { get; set; }
    }
}
