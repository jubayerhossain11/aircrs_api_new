﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirCRS_API.SERVICE.AirPriceServices.Dtos
{
    public class AirSearchDto
    {
        public List<TotalTravelTimeDto> TotalTravelTimes;
        public List<FlightDescriptionDto> Onwards;
        public List<FlightDescriptionDto> Returns;
        public List<FareBreakdownDto> FareBreakdown;

        public bool HasOwnID { get; set; }
        public string SegmentCode { get; set; }
        public string TripType { get; set; }
        public decimal CancelPenalty { get; set; }
        public decimal ChangePenalty { get; set; }
        public string LatestTicketingTime { get; set; }
        public decimal TotalTax { get; set; }
        public bool IsTaxBreakdownAvailable { get; set; }
        public bool IsBookable { get; set; }
        public bool IsRefundable { get; set; }
        public string PlatingCarrier { get; set; }
        public string OwnIDRef { get; set; }
        public int Infants { get; set; }
        public int Childs { get; set; }
        public int Adults { get; set; }
        public string PassengerType { get; set; }
        public string APICurrencyType { get; set; }
        public decimal BasePrice { get; set; }
        public decimal TotalPrice { get; set; }
        public string AirPricingSolution_Key { get; set; }
        public string PlatingCarrierName { get; set; }
        public string IGXKey { get; set; }
    }
}
