﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirCRS_API.SERVICE.AirPriceServices.Dtos
{
    public class PriceBreckdownDto
    {
        public int? PriceBreckdownId { get; set; }
        public int? TripId { get; set; }
        public int PaxType { get; set; }
        public int? BaseFare { get; set; }
        public int? Tax { get; set; }
        public int? ServiceCharge { get; set; }
        public bool IsLive { get; set; }

        public string CreatorUserId { get; set; }
        public string LoggedinUserId { get; set; }
    }
}
