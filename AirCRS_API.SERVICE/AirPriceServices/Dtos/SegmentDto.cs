﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirCRS_API.SERVICE.AirPriceServices.Dtos
{
    public class SegmentDto
    {
        public int? SegmentId { get; set; }
        public int SegmentType { get; set; }
        public int? TripId { get; set; }
        public DateTime? DepartureDate { get; set; }
        public DateTime? ArrivalDate { get; set; }
        public int? ClassType { get; set; }
        public string SegmentOrigin { get; set; }
        public string Destination { get; set; }
        public DateTime? TravelTime { get; set; }

        public string Duration { get; set; }
        public int SegmentSerialNo { get; set; }
        public string FlightNumber { get; set; }
        public string AirLines { get; set; }
        public string Baggage { get; set; }

        public string ClassTypeName { get; set; }
        public string Origin { get; set; }
        public string AirportFullNameForOrigin { get; set; }
        public string AirportFullNameForDestination { get; set; }

    }
}
