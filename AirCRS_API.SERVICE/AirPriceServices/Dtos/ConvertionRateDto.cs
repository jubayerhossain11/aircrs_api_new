﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirCRS_API.SERVICE.AirPriceServices.Dtos
{
    public class ConvertionRateDto
    {
        public string ApiRef { get; set; }
        public decimal CurrencyRate { get; set; }
    }
}
