﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirCRS_API.SERVICE.AirPriceServices.Dtos
{
    public class TrimDto
    {
        public int? TripId { get; set; }
        public int? JournayType { get; set; }
        public System.DateTime JournayOnWardDate { get; set; }
        public DateTime? JournayReturnDate { get; set; }
        public int AirLineId { get; set; }
        public string AirLineCode { get; set; }
        public string AirLineFullName { get; set; }
        public string Origin { get; set; }
        public string OriginFullName { get; set; }
        public string Destination { get; set; }
        public string DestinationFullName { get; set; }
        public int NoOfTickets { get; set; }
        public bool IsIndividual { get; set; }
        public bool IsPublic { get; set; }
        public int TicketStatus { get; set; }
        public int ClassType { get; set; }
        public string classTypeName { get; set; }
        public string ClassCode { get; set; }
        public string CRSName { get; set; }
        public bool IsActive { get; set; }
        public string CreatorUserId { get; set; }
        public string LoggedinUserId { get; set; }
        public List<SegmentDto> Segments { get; set; }
        public List<PriceBreckdownDto> PriceBreckdowns { get; set; }
        public List<PnrVM> Pnrs { get; set; }
        public List<string> Tickets { get; set; }

        //Agency Information
        public string AgencyName { get; set; }
        public string AgencyAddress { get; set; }
        public string AgentAddress { get; set; }
        public string AgenctContactNo { get; set; }

    }
}
