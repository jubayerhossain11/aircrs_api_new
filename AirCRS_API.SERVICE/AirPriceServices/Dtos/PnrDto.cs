﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirCRS_API.SERVICE.AirPriceServices.Dtos
{
    public class PnrVM
    {
        public int? PnrId { get; set; }
        public int? TripId { get; set; }
        public string PnrValue { get; set; }
        public int? PnrCount { get; set; }
    }
}
