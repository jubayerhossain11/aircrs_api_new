﻿using AirCRS_API.DAL.Enums;
using AirCRS_API.DAL.UnitOfWorks;
using AirCRS_API.SERVICE.AirPriceServices.Dtos;
using AirCRS_API.SERVICE.AirSearchApiServices.ApiRequestDots;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace AirCRS_API.SERVICE.AirPriceServices
{
    public class AirPriceService : IAirPriceService
    {
        private string EncryptionKey = "2@3G#kja$#kmm";

        private readonly IUoW _uow;
        public AirPriceService(IUoW uoW)
        {
            _uow = uoW;
        }
        public AirPriceCheckDto GetAirPriceRequest(AirPriceCheckRequestInput airSearchRequest)
        {
            AirPriceCheckRequestDto AirPriceCheckRequest = new AirPriceCheckRequestDto();
            AirPriceCheckDto airPriceCheckResponse = new AirPriceCheckDto();

            // AirPricingSolution_Key validation
            var requestLogId = Convert.ToInt32(DecryptString(airSearchRequest.AirPricingSolution_Key, EncryptionKey));
            var getRequestLogDataByLogId = _uow.ApiRequestLogRepository.Get(x => x.ApiRequestLogId == requestLogId && (x.Status == (int)ApiRequestLogStatus.Search || x.Status == (int)ApiRequestLogStatus.CheckingPrice)).FirstOrDefault();

            if (getRequestLogDataByLogId == null)
            {
                return null;
            }
            else
            {
                #region Check Price

                var priceBreckdowns = _uow.PriceBreakdownRepository.GetAll().Where(x => x.TripId == getRequestLogDataByLogId.TripId).ToList();

                //var totalBaseFare = (decimal)(priceBreckdowns.Where(x => x.PaxType == 1).FirstOrDefault().BaseFare ?? 0) * getRequestLogDataByLogId.NumberOfAdult +
                //                       (decimal)(priceBreckdowns.Where(x => x.PaxType == 2).FirstOrDefault().BaseFare ?? 0) * getRequestLogDataByLogId.NumberOfChild +
                //                       (decimal)(priceBreckdowns.Where(x => x.PaxType == 3).FirstOrDefault().BaseFare ?? 0) * getRequestLogDataByLogId.NumberOfInfrant;

                //var totalTax = (decimal)(priceBreckdowns.Where(x => x.PaxType == 1).FirstOrDefault().Tax ?? 0) * getRequestLogDataByLogId.NumberOfAdult +
                //              (decimal)(priceBreckdowns.Where(x => x.PaxType == 2).FirstOrDefault().Tax ?? 0) * getRequestLogDataByLogId.NumberOfChild +
                //              (decimal)(priceBreckdowns.Where(x => x.PaxType == 3).FirstOrDefault().Tax ?? 0) * getRequestLogDataByLogId.NumberOfInfrant;


                //if (getRequestLogDataByLogId.TotalPrice != (totalBaseFare + totalTax))
                //{
                //    airPriceCheckResponse.IsPriceChanged = true;
                //}
                //else
                //{
                //    airPriceCheckResponse.IsPriceChanged = false;
                //}
                #endregion

                #region TripInfo

                var tripData = (from Trip in _uow.TripRepository.GetAll().Where(x => x.IsRemoved == false && x.NoOfTickets > 0 && x.IsActive && x.TripId == getRequestLogDataByLogId.TripId)
                                join Air in _uow.AirlineRepository.GetAll()
                                on Trip.AirLineId equals Air.Id
                                join TO in _uow.AirportSearchCodeRepository.GetAll()
                                on Trip.Origin equals TO.AirportCode
                                join TD in _uow.AirportSearchCodeRepository.GetAll()
                                on Trip.Destination equals TD.AirportCode
                                join TS in _uow.SegmentRepository.GetAll().Where(x => x.IsRemoved == false)
                                on Trip.TripId equals TS.TripId into segmentTemp
                                join TPB in _uow.PriceBreakdownRepository.GetAll()//.Where(x => x.IsRemoved == false)
                                on Trip.TripId equals TPB.TripId into priceBTemp
                                orderby Trip.TripId descending
                                where Trip.TripId == getRequestLogDataByLogId.TripId
                                select new TripDto()
                                {
                                    TripId = Trip.TripId,
                                    AirLineId = Trip.AirLineId,
                                    AirLineCode = Air.Code,
                                    AirLineFullName = Air.AriLineName,
                                    ClassCode = Trip.ClassCode,
                                    classTypeName = Trip.ClassType == 1 ? "Economy" : "Business",
                                    Destination = Trip.Destination,
                                    DestinationFullName = TD.AirportCode,
                                    IsIndividual = Trip.IsIndividual,
                                    JournayOnWardDate = Trip.JournayOnWardDate,
                                    JournayReturnDate = Trip.JournayReturnDate,
                                    JournayType = Trip.JournayType,
                                    NoOfTickets = Trip.NoOfTickets,
                                    Origin = Trip.Origin,
                                    OriginFullName = TO.AirportCode,
                                    IsPublic = Trip.IsPublic,
                                    IsActive = Trip.IsActive,
                                    CreatorUserId = Trip.CreatedBy,
                                    //PriceBreckdowns = priceBTemp.Select(x => new PriceBreckdownDto() { BaseFare = x.BaseFare, PaxType = x.PaxType, PriceBreckdownId = x.Id, Tax = x.Tax, ServiceCharge = x.ServiceCharge, TripId = x.TripId }).ToList(),
                                }).FirstOrDefault();
                #endregion

                List<FareBreakdownDto> fareBreakdown = new List<FareBreakdownDto>();
                List<FlightDescriptionDto> Onwards = new List<FlightDescriptionDto>();
                List<TotalTravelTimeDto> totalTravelTimes = new List<TotalTravelTimeDto>();
                var getAllSegments = _uow.SegmentRepository.GetAll().Where(x => x.IsRemoved == false);
                var getAllAirCodes = _uow.AirportSearchCodeRepository.GetAll();

                airPriceCheckResponse.Adults = getRequestLogDataByLogId.NumberOfAdult.Value;
                airPriceCheckResponse.Childs = getRequestLogDataByLogId.NumberOfChild.Value;
                airPriceCheckResponse.Infants = getRequestLogDataByLogId.NumberOfInfrant.Value;
                //airPriceCheckResponse.BasePrice = totalBaseFare.Value;
                //airPriceCheckResponse.TotalTax = totalTax.Value;
                //airPriceCheckResponse.TotalPrice = totalBaseFare.Value + totalTax.Value;
                airPriceCheckResponse.IsBookable = true;
                airPriceCheckResponse.IsRefundable = true;
                airPriceCheckResponse.IsTaxBreakdownAvailable = true;
                airPriceCheckResponse.HasOwnID = true;
                airPriceCheckResponse.PlatingCarrier = tripData.AirLineCode;
                airPriceCheckResponse.PlatingCarrierName = tripData.AirLineFullName;
                airPriceCheckResponse.TripType = tripData.JournayType == 1 ? "One-Way" : "";
                airPriceCheckResponse.AirPricingSolution_Key = airSearchRequest.AirPricingSolution_Key;

                #region add PriceBreckdowns

                foreach (var price in tripData.PriceBreckdowns)
                {
                    var priceBreckdown = new FareBreakdownDto()
                    {

                        NoOfPassenger = price.PaxType == 1 ? getRequestLogDataByLogId.NumberOfAdult.Value : price.PaxType == 2 ? getRequestLogDataByLogId.NumberOfChild.Value : getRequestLogDataByLogId.NumberOfInfrant.Value,
                        BaseFare = (decimal)price.BaseFare,
                        PassengerType = price.PaxType == 1 ? "ADT" : price.PaxType == 2 ? "CHD" : "INF",
                        TotalTax = (decimal)price.Tax,
                        TotalFare = (decimal)price.BaseFare + (decimal)price.Tax
                    };

                    if (getRequestLogDataByLogId.NumberOfAdult > 0 && price.PaxType == 1)
                    {
                        fareBreakdown.Add(priceBreckdown);
                    }
                    else if (getRequestLogDataByLogId.NumberOfChild > 0 && price.PaxType == 2)
                    {
                        fareBreakdown.Add(priceBreckdown);
                    }
                    else if (getRequestLogDataByLogId.NumberOfInfrant > 0 && price.PaxType == 3)
                    {
                        fareBreakdown.Add(priceBreckdown);
                    }
                }

                #endregion

                #region add Segment

                var segments = (from s in getAllSegments
                                join ts in getAllAirCodes
                                on s.SegmentOrigin equals ts.AirportCode
                                where s.TripId == getRequestLogDataByLogId.TripId
                                orderby s.SegmentSerialNo ascending
                                select new SegmentOutputDto()
                                {
                                    TripId = s.TripId,
                                    ArrivalDate = s.ArrivalDate,
                                    DepartureDate = s.DepartureDate,
                                    Origin = ts.AirportCode,
                                    Duration = s.Duration,
                                    FlightNumber = s.FlightNumber,
                                    Baggage = s.Baggage,
                                    ClassType = s.ClassType,
                                    SegmentSerialNo = s.SegmentSerialNo,
                                    SegmentType = s.SegmentType,
                                    ClassTypeName = s.ClassType == 1 ? "Economy" : "Business",
                                }).ToList();

                var inatialSource = tripData.Origin;
                var count = 0;
                var arivalAndDepartureDateCount = 0;

                DateTime? arivalFirst = null;
                DateTime? departureSecond = null;
                TimeSpan totalTravaltime = new TimeSpan(0, 0, 0, 0, 0);
                // var AirportTimes = UtilsGetAirLines.GetAllAirLinesData();
                string Time = "";

                foreach (var item in segments)
                {


                    Time = "";
                    TimeSpan segmentDuration = TimeSpan.Parse(item.Duration);
                    if (segmentDuration.Days > 0)
                        Time = segmentDuration.Days + "d " + segmentDuration.Hours + "h " + segmentDuration.Minutes + "m";
                    else if (segmentDuration.Hours > 0)
                        Time = segmentDuration.Hours + "h " + segmentDuration.Minutes + "m";
                    else
                        Time = segmentDuration.Minutes + "m";

                    var segmentItem = new FlightDescriptionDto()
                    {

                        Origin = inatialSource,
                        Destination = item.Origin,
                        AirBaggageAllowance = item.Baggage,
                        ArrivalTime = item.ArrivalDate.Value.ToString("dd-MMM-yyyy HH:mm tt"),
                        DepartureTime = item.DepartureDate.Value.ToString("dd-MMM-yyyy HH:mm tt"),
                        Carrier = tripData.AirLineCode,
                        CarrierName = tripData.AirLineFullName,
                        OperatingCarrier = tripData.AirLineCode,
                        OperatingCarrierName = tripData.AirLineFullName,
                        TravelDuration = Time,
                        CabinClass = item.ClassType == 1 ? "Economy" : "Business",
                        FlightNumber = item.FlightNumber,
                        OperatingFlightNumber = item.FlightNumber,
                        BookingCode = tripData.ClassCode,
                    };

                    Onwards.Add(segmentItem);
                    //update source
                    inatialSource = segmentItem.Destination;
                    count++;

                    totalTravaltime = totalTravaltime + TimeSpan.Parse(item.Duration);

                    if (arivalAndDepartureDateCount == 0)
                        arivalFirst = item.ArrivalDate.Value;
                    if (arivalAndDepartureDateCount == 1)
                        departureSecond = item.DepartureDate.Value;

                    if (arivalFirst != null && departureSecond != null)
                    {
                        totalTravaltime = totalTravaltime + (departureSecond.Value - arivalFirst.Value);
                        arivalAndDepartureDateCount = 0;
                        arivalFirst = item.ArrivalDate;
                    }
                    if (segments.Count() == 1)
                    {
                        totalTravaltime = TimeSpan.Parse(item.Duration);
                    }

                    arivalAndDepartureDateCount++;
                }

                #endregion

                #region TotalTravelTime

                var Time2 = "";
                if (totalTravaltime.Days > 0)
                    Time2 = totalTravaltime.Days + "d " + totalTravaltime.Hours + "h " + totalTravaltime.Minutes + "m";
                else if (totalTravaltime.Hours > 0)
                    Time2 = totalTravaltime.Hours + "h " + totalTravaltime.Minutes + "m";
                else
                    Time2 = totalTravaltime.Minutes + "m";

                var travelTime = new TotalTravelTimeDto()
                {
                    NoOfStop = segments.Count() > 1 ? segments.Count() - 1 : 0,
                    TotalTravelDuration = Time2,
                    TravelType = "onward"
                };

                totalTravelTimes.Add(travelTime);
                #endregion

                airPriceCheckResponse.Onwards = Onwards;
                airPriceCheckResponse.FareBreakdown = fareBreakdown;
                airPriceCheckResponse.TotalTravelTimes = totalTravelTimes;


                //update Api Request log 

                getRequestLogDataByLogId.Status = (int)ApiRequestLogStatus.CheckingPrice;
                _uow.ApiRequestLogRepository.Update(getRequestLogDataByLogId);
                _uow.ApiRequestLogRepository.Save();

            }
            airPriceCheckResponse.ApiOriginalPrice = new ApiOriginalPriceDto();
            airPriceCheckResponse.ApiOriginalPrice.TotalPrice = airPriceCheckResponse.TotalPrice;
            airPriceCheckResponse.ApiOriginalPrice.BasePrice = airPriceCheckResponse.BasePrice;
            airPriceCheckResponse.ApiOriginalPrice.TotalTax = airPriceCheckResponse.TotalTax;

            return airPriceCheckResponse;

        }

        private string DecryptString(string Message, string Passphrase)
        {
            byte[] Results;
            System.Text.UTF8Encoding UTF8 = new System.Text.UTF8Encoding();
            MD5CryptoServiceProvider HashProvider = new MD5CryptoServiceProvider();
            byte[] TDESKey = HashProvider.ComputeHash(UTF8.GetBytes(Passphrase));
            TripleDESCryptoServiceProvider TDESAlgorithm = new TripleDESCryptoServiceProvider();
            TDESAlgorithm.Key = TDESKey;
            TDESAlgorithm.Mode = CipherMode.ECB;
            TDESAlgorithm.Padding = PaddingMode.PKCS7;
            byte[] DataToDecrypt = Convert.FromBase64String(Message);
            try
            {
                ICryptoTransform Decryptor = TDESAlgorithm.CreateDecryptor();
                Results = Decryptor.TransformFinalBlock(DataToDecrypt, 0, DataToDecrypt.Length);
            }
            finally
            {
                TDESAlgorithm.Clear();
                HashProvider.Clear();
            }
            return UTF8.GetString(Results);
        }
    }
}
