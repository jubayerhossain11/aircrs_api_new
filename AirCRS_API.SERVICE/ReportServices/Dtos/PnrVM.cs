﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirCRS_API.SERVICE.ReportServices.Dtos
{
    public class PnrVM
    {
        public long PnrId { get; set; }
        public long TripId { get; set; }
        public string PnrValue { get; set; }
        public int? PnrCount { get; set; }
        public bool IsRemoved { get; set; }

    }
}
