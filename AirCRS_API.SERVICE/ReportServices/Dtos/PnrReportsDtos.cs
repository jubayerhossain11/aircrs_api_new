﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AirCRS_API.DAL.Enums;

namespace AirCRS_API.SERVICE.ReportServices.Dtos
{
    public class PnrReportsDtos
    {

        public string TripUniqueNo { get; set; }
        public string PassangerType { get; set; }
        public string firstName { get; set; }
        public string Title { get; set; }
        public string lastName { get; set; }
        public DateTime? dob { get; set; }
        public string gender { get; set; }
        public int? NumberOfTickets { get; set; }
        public string PnrLogStatustatus { get; set; }
        public string passportNo { get; set; }
        public DateTime? expireDate { get; set; }
        public string meal { get; set; }
        public string frequentFlyer { get; set; }

        public int? bags { get; set; }

        public string CustomBaggage { get; set; }
        public decimal? fare { get; set; }
        public string pnrNo { get; set; }
        public string phone { get; set; }
        public string phoneNo { get; set; }
        public string remarks { get; set; }


    }
}
