﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AirCRS_API.DAL.Entities;
using AirCRS_API.DAL.Enums;
using AirCRS_API.SERVICE.ReportServices.Dtos;

namespace AirCRS_API.SERVICE.ReportServices
{
    public class ReportServices : IReportServices
    {
        private readonly DAL.UnitOfWorks.IUoW _uow;
        public ReportServices(DAL.UnitOfWorks.IUoW uow)
        {
            _uow = uow;
        }
        public List<PnrVM> GetPNRList(string loggedinUserId)
        {

            var response = (from pnr in _uow.PnrRepository.GetAll().Where(x => x.IsRemoved == false && x.CreatedBy == loggedinUserId)
                            select new PnrVM
                            {
                                PnrId = pnr.PnrId,
                                PnrValue = pnr.PnrValue,
                                TripId = pnr.TripId,
                                PnrCount = pnr.PnrCount,
                                IsRemoved = pnr.IsRemove
                            }).ToList();


            return response;
        }
        public List<PnrReportsDtos> GetPnrReportList(int TripId, string loginId)
        {
            var response = (from trip in _uow.TripRepository.GetAll().Where(x => x.TripId == TripId)
                            join pnr in _uow.PnrRepository.GetAll()
                            on trip.TripId equals pnr.TripId
                            join pnrLog in _uow.PnrLogRepository.GetAll()
                            on pnr.PnrId equals pnrLog.PnrId into PnrlogTemp
                            where (trip.CreatedBy == loginId)
                            select new
                            {
                                tripInfo = trip,
                                pnrLog = PnrlogTemp,
                                pnrInfo = pnr
                            }).FirstOrDefault();

            var pnrLogOutput = new List<PnrReportsDtos>();

            if (response.pnrLog.Any())
            {
                var mainBaggage = "";
                var tripSegment = _uow.SegmentRepository.Get(x => x.TripId == response.tripInfo.TripId).ToList();

                foreach (var segment in tripSegment)
                {
                    mainBaggage += segment.Baggage + " +";
                }

                foreach (var item in response.pnrLog)
                {
                    PnrReportsDtos pnrReport = new PnrReportsDtos();
                    pnrReport.PassangerType = item?.Passenger?.PassengerCode;

                    pnrReport.PnrLogStatustatus =
                        item?.UsesFlag != null ? ((PnrUsesType)item.UsesFlag).ToString() != "Cancel" ? "Ticketed" : "Cancel" : "UnUsed";
                    pnrReport.TripUniqueNo = response.tripInfo.TripUniqueNo;
                    pnrReport.firstName = item?.Passenger?.FirstName ?? "";
                    pnrReport.Title = item?.Passenger?.Title ?? "";
                    pnrReport.lastName = item?.Passenger?.LastName ?? "";
                    pnrReport.dob = item?.Passenger?.DOB;
                    pnrReport.gender = item?.Passenger?.Gender ?? "";
                    pnrReport.passportNo = item?.Passenger?.PassportNumber ?? "";
                    pnrReport.expireDate = item?.Passenger?.PassportExpDate;
                    pnrReport.meal = item?.Passenger?.Meal?.Name ?? "";
                    pnrReport.frequentFlyer = item?.Passenger?.FrequentFlyerNumber ?? "";
                    pnrReport.bags = item?.Passenger?.Baggage != null ? item.Passenger.Baggage.Quantity : 0;
                    pnrReport.fare = item != null ? item.TotalPrice : 0;
                    pnrReport.phone = item?.Passenger?.ContractNumber;
                    pnrReport.pnrNo = item.Pnr.PnrValue;
                    pnrReport.remarks = "";
                    pnrReport.CustomBaggage = item?.Passenger?.Baggage != null ? (mainBaggage + item.Passenger.Baggage.Quantity + $" {((BaggageType)item.Passenger.Baggage.BaggageType).ToString()}") : mainBaggage;

                    pnrLogOutput.Add(pnrReport);
                }
            }

            for (int i = 0; i < response.tripInfo.NoOfTickets - response.pnrLog.Count(); i++)
            {
                PnrReportsDtos pnrReport = new PnrReportsDtos();
                pnrReport.PassangerType = "";

                pnrReport.TripUniqueNo = response.tripInfo.TripUniqueNo;
                pnrReport.PnrLogStatustatus = "Unused";
                pnrReport.firstName = "";
                pnrReport.lastName = "";
                pnrReport.dob = null;
                pnrReport.gender = "";
                pnrReport.passportNo = "";
                pnrReport.expireDate = null;
                pnrReport.meal = "";
                pnrReport.frequentFlyer = "";
                pnrReport.bags = null;
                pnrReport.fare = null;
                pnrReport.phone = "";
                pnrReport.pnrNo = response.pnrInfo.PnrValue;
                pnrReport.remarks = "";
                pnrLogOutput.Add(pnrReport);
            }

            

            //if(response[0].NumberOfTickets > response.Count)
            //{
            //    var rowadd = response[0].NumberOfTickets - response.Count;
            //    if (rowadd > 0)
            //    {
            //        for (int i = 0; i < rowadd; i++)
            //        {
            //            PnrReportsDtos pnrReport = new PnrReportsDtos();
            //            //pnrReport = null;
            //            pnrReport.PassangerType = "";
            //            pnrReport.NumberOfTickets = null;
            //            pnrReport.PnrLogStatustatus = "Unused";
            //            pnrReport.firstName = "";
            //            pnrReport.lastName = "";
            //            pnrReport.dob = null;
            //            pnrReport.gender = "";
            //            pnrReport.passportNo = "";
            //            pnrReport.expireDate = null;
            //            pnrReport.meal = "";
            //            pnrReport.frequentFlyer = "";
            //            pnrReport.bags = null;
            //            pnrReport.fare = null;
            //            pnrReport.phone = "";
            //            pnrReport.pnrNo = "";
            //            pnrReport.remarks = "";
            //            response.Add(pnrReport);
            //        }

            //    }
            //}

            return pnrLogOutput;

        }


    }
}
