﻿using AirCRS_API.DAL.Entities;
using AirCRS_API.SERVICE.ReportServices.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirCRS_API.SERVICE.ReportServices
{
    public interface IReportServices
    {
        List<PnrVM> GetPNRList(string loggedinUserId);
        List<PnrReportsDtos> GetPnrReportList(int TripId, string loginId);
    }
}
