﻿using AirCRS_API.SERVICE.BookingServices.Dtos;
using AirCRS_API.SERVICE.TripServices.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirCRS_API.SERVICE.BookingServices
{
    public interface IBookingService
    {
        List<PnrOutputDto> GetPNRWithAvaibleTickets(string loggedInUserId);
        List<PriceBreakDownOutput> GetPriceBreakDownByTripId(long tripId);
        bool CreatePassengerForBooking(List<CreateOrUpdatePassengerInfo> input, string createdBy);

        List<MealDto> GetMealListByTripId(long tripId);
        List<Dtos.BaggageDto> GetBaggageByTripId(long tripId);
        WheelChairDto GetWheelChairByTripId(long tripId);
    }
}
