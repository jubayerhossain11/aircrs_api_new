﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirCRS_API.SERVICE.BookingServices.Dtos
{
    public class PnrOutputDto
    {
        public string PNRNo { get; set; }
        public long TripId { get; set; }

        public long PNRId { get; set; }

        public int AvaiableTicket { get; set; }
    }
}
