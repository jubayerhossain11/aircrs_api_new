﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirCRS_API.SERVICE.BookingServices.Dtos
{
    public class PriceBreakDownOutput
    {
        public long PriceBreakDownId { get; set; }
        public string PriceBreakDownName { get; set; }
    }
}
