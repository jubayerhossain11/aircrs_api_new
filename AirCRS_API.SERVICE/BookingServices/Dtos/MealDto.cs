﻿using AirCRS_API.DAL.CommonEntities;
using AirCRS_API.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirCRS_API.SERVICE.BookingServices.Dtos
{
    public class MealDto: AuditEntity
    { 
        public long Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public bool IsActive { get; set; }

        public string NameForDropdown { get; set; }
    }
}
