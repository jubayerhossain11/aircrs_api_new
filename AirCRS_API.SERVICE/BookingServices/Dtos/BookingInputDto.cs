﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirCRS_API.SERVICE.BookingServices.Dtos
{
    public class BookingInputDto
    {
        public long TripId { get; set; }
    }
}
