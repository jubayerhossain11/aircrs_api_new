﻿using AirCRS_API.DAL.Entities;
using AirCRS_API.DAL.Enums;
using AirCRS_API.DAL.UnitOfWorks;
using AirCRS_API.SERVICE.BookingServices.Dtos;
using AirCRS_API.SERVICE.TripServices.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace AirCRS_API.SERVICE.BookingServices
{
    public class BookingService : IBookingService
    {
        private readonly IUoW _uow;
        public BookingService(IUoW uoW)
        {
            _uow = uoW;
        }

        public bool CreatePassengerForBooking(List<CreateOrUpdatePassengerInfo> input, string createdBy)
        {
            using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
            {
                foreach (var passenger in input)
                {
                    var passengerToCrate = new Passenger()
                    {
                        ContractNumber = passenger.ContractNumber,
                        CreatedBy = createdBy,
                        CreatedDate = DateTime.Now,
                        DOB = passenger.DOB,
                        Email = passenger.Email,
                        Gender = passenger.Gender,
                        IsRemoved = false,
                        Title = passenger.Title,
                        FirstName = passenger.FirstName,
                        LastName = passenger.LastName,
                        FrequentFlyerNumber = passenger.FrequentFlyerNumber,
                        Nationality = passenger.Nationality,
                        PassengerCode = passenger.PassengerType,
                        PassportNumber = passenger.PassportNumber,
                        PassportExpDate = passenger.PassportExpDate,
                        BaggageId = passenger.BaggageId,
                        MealId = passenger.MealId,
                        WheelChairId = passenger.WheelChairId
                    };
                    _uow.PassengerRepository.Insert(passengerToCrate);
                    _uow.PassengerRepository.Save();

                    var priceBreakdownDetailList = _uow.PriceBreakdownDetailsRepository.Get(x => x.PriceBreakdownId == passenger.PriceBreakdownId).ToList();
                    var getPnrByTripId = _uow.PnrRepository.Get(x => x.TripId == passenger.TripId).FirstOrDefault();

                    var PriceBreaDetailskDownInfo = priceBreakdownDetailList.Where(x => passenger.PassengerType == "ADULT" ? x.PaxType == 1 : passenger.PassengerType == "CHILD" ? x.PaxType == 2 : x.PaxType == 3).FirstOrDefault();
                    
                    var tripMealForPassenger = _uow.MealRepository.Get(x=>x.Id == passenger.MealId).FirstOrDefault();
                    var tripBaggageForPassenger = _uow.BaggageRepository.Get(x => x.Id == passenger.BaggageId).FirstOrDefault();
                    var tripWheelChairForPassenger = _uow.WheelChairRepository.Get(x => x.Id == passenger.WheelChairId).FirstOrDefault();

                    var pnrlogs = new PnrLog()
                    {
                        CreatedBy = createdBy,
                        IsRemoved = false,
                        IsSeen = false,
                        CreatedDate = DateTime.Now,
                        PassengerId = passengerToCrate.PassengerId,
                        PnrId = passenger.PnrId,
                        PriceBreakdownDetailsId = PriceBreaDetailskDownInfo.Id,
                        UsesFlag = (int)PnrUsesType.Booked,
                        Pnr = getPnrByTripId,
                        TicketNumber = "",
                        BookingSourceType = BookingSourceType.CRS_Portal,
                        TotalPrice = (PriceBreaDetailskDownInfo.BaseFare != null ? PriceBreaDetailskDownInfo.BaseFare.Value : 0) + (PriceBreaDetailskDownInfo.Tax != null ? PriceBreaDetailskDownInfo.Tax.Value : 0) + (PriceBreaDetailskDownInfo.ServiceCharge != null ? PriceBreaDetailskDownInfo.ServiceCharge.Value : 0) + 
                        (tripMealForPassenger != null ? tripMealForPassenger.Price: 0) + (tripBaggageForPassenger != null ? tripBaggageForPassenger.Price : 0) + (tripWheelChairForPassenger != null ? tripWheelChairForPassenger.Price : 0)
                    };

                    _uow.PnrLogRepository.Insert(pnrlogs);
                    _uow.PnrLogRepository.Save();
                }

                scope.Complete();
                return true;
            }
           
        }

        public List<PnrOutputDto> GetPNRWithAvaibleTickets(string loggedInUserId)
        {

            var output = (from trip in _uow.TripRepository.GetAll()
                          join pnr in _uow.PnrRepository.GetAll()
                          on trip.TripId equals pnr.TripId
                          join pnrLog in _uow.PnrLogRepository.GetAll()
                          on pnr.PnrId equals pnrLog.PnrId into pnarLogTemp
                          where trip.CreatedBy == loggedInUserId
                          select new PnrOutputDto()
                          {
                              PNRId = pnr.PnrId,
                              PNRNo = $"{pnr.PnrValue} ({(trip.NoOfTickets - pnarLogTemp.Where(x => x.UsesFlag != (int)PnrUsesType.Cancel).Count())})",
                              TripId = trip.TripId
                          }).ToList();

            return output;
        }

        public List<PriceBreakDownOutput> GetPriceBreakDownByTripId(long tripId)
        {

            var output = (from trip in _uow.TripRepository.GetAll()
                          join priceBreakDown in _uow.PriceBreakdownRepository.GetAll()
                          on trip.TripId equals priceBreakDown.TripId
                          where trip.TripId == tripId
                          select new PriceBreakDownOutput()
                          {
                              PriceBreakDownId = priceBreakDown.Id,
                              PriceBreakDownName = priceBreakDown.PriceSlotName
                          }).ToList();

            return output;
        }

        public WheelChairDto GetWheelChairByTripId(long tripId)
        {

            var output = (from trip in _uow.TripRepository.GetAll()
                          join wheelChair in _uow.WheelChairRepository.GetAll()
                          on trip.TripId equals wheelChair.TripId
                          where trip.TripId == tripId && wheelChair.IsActive
                          select new WheelChairDto()
                          {
                              IsActive = wheelChair.IsActive,
                              Price = wheelChair.Price,
                              Id = wheelChair.Id,
                              IsAvailable = wheelChair.IsAvailable
                          }).FirstOrDefault();

            return output;
        }
        public List<MealDto> GetMealListByTripId(long tripId)
        {

            var output = (from trip in _uow.TripRepository.GetAll()
                          join meal in _uow.MealRepository.GetAll()
                          on trip.TripId equals meal.TripId
                          where trip.TripId == tripId && meal.IsActive && !meal.IsRemove
                          select new MealDto()
                          {
                              Id = meal.Id,
                              Code = meal.Code,
                              IsActive = meal.IsActive,
                              Name = meal.Name,
                              Price = meal.Price,
                              NameForDropdown = $"{meal.Code} > {meal.Name} ({meal.Price})"
                          }).ToList();

            return output;
        }

        public List<Dtos.BaggageDto> GetBaggageByTripId(long tripId)
        {

            var output = (from trip in _uow.TripRepository.GetAll()
                          join baggage in _uow.BaggageRepository.GetAll()
                          on trip.TripId equals baggage.TripId
                          where trip.TripId == tripId && baggage.IsActive && !baggage.IsRemove
                          select new Dtos.BaggageDto()
                          {
                              Id = baggage.Id,
                              Price = baggage.Price,
                              Quantity = baggage.Quantity,
                              BaggageType = baggage.BaggageType,
                              NameForDropdown = $"{baggage.BaggageType}_{baggage.Quantity} ({baggage.Price})"
                          }).ToList();

            return output;
        }
    }
}
