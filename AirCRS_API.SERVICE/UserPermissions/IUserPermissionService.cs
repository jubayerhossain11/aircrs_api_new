﻿using AirCRS_API.SERVICE.UserPermissions.Dtos;

namespace BackOffice_API.Services.UserPermissions
{
    public interface IUserPermissionService
    {
        object ValidateUrlManu(long roleId, string controllerName, string actionName);
    }
}
